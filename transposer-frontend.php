<?php

/**
 * Wrappers for trp_replace_post_field()
 */
function trp_replace_post_title( $content, $post_id ) {
	return trp_replace_post_field( $content, 'post_title', $post_id );
}

function trp_replace_post_excerpt( $content ) {
	return trp_replace_post_field( $content, 'post_excerpt' );
}

function trp_replace_post_content( $content ) {
	return trp_replace_post_field( $content, 'post_content' );
}


/**
 * If ?trpl=en is given, it replaces the post title, content, excerpt with the corresponding translation in the frontend
 *
 * @param    string    The content of the corresponding text field
 * @param    string    Name of the field to replace: 'post_title', 'post_excerpt' or 'post_content'
 * @param    int       A post ID passed by the_title filter
 * @return   string    The replaced text of the given field
 */
function trp_replace_post_field( $content, $field, $post_id = 0 ) {

	// Don't apply for Eurozine as it will break the theme
	if( TRP_Eurozine::is_eurozine() )
		return $content;

	if( ! isset( $_REQUEST['trpl'] ) || strlen( $_REQUEST['trpl'] ) != 2 )
		return $content;

	if( ! in_array( $_REQUEST['trpl'], array_keys( TRP_Helper::get_all_languages() ) ) )
		return $content;

	// Only for single post views and pages
	// Replace only the main post object's content
	if ( ! is_singular() || ! is_main_query() )
		return $content;

	// Only replace if the current filter call in single view really corresponds to the main post ID
	// In this way we avoid that other site elements that also call e.g. 'the_title'-filter don't get replaced with the same value
	// This condition only applies for the_title filter, since the_excerpt and the_content filters don't pass a post ID as a parameter but it should cover most cases
	if( $post_id > 0 && $post_id != get_queried_object_id() )
		return $content;

	global $post;

	if( $post->ID != get_queried_object_id() )
		return $content;

	if( ! in_array( get_post_type( $post->ID ), get_option( 'transposer_default_ui_post_types', array( 'post' ) ) ) )
		return $content;

	$TRP = new TRP;
	$translation = $TRP->get_post_translation( $post->ID, $_REQUEST['trpl'] );



	if( isset( $_GET['dev'] ) && $field == 'post_content' ) {
		echo "<pre>";
		echo $field . ":<br>";
		print_r( $translation );
		echo "</pre>";
	}


	if( ! is_null( $translation ) && ! empty( $translation->$field ) ) {

		// Render shortcodes in the content field
		if( $field == 'post_content' ) {

			$return = stripslashes( $translation->$field );

			if( get_option( 'transposer_do_frontend_shortcodes', '0' ) == '0' || isset( $_GET['stripshortcodes'] ) )
				$return = strip_shortcodes( $return );
			else
				$return = do_shortcode( $return );

	 		$return = do_blocks( $return );

			if( isset( $_GET['noembed'] ) )
				remove_filter( 'the_content', [ $GLOBALS['wp_embed'], 'autoembed' ], 8 );

			if( ! isset( $_GET['noautop'] ) )
				$return = wpautop( $return );

			// Remove plain oembed URLs but keep links intact
			$return = TRP_Helper::remove_plain_urls( $return );

			// DeepL tends to create paragraphs containing only a single dot or semicolon. Remove them.
			$return = str_replace( '<p>.</p>', '', $return );
			$return = str_replace( '<p>&nbsp ;</p>', '', $return );
			$return = str_replace( '<p>.<br />', '<p>', $return );

		} else {
			$return = stripslashes( $translation->$field );
		}

		// Add a disclaimer at the end of the post_content if it's an auto-translation,
		if( $translation->type == 'auto' && $field == 'post_content' ) {

			// Add the EU Logo at the bottom of each article
			$return .='
			<div class="trp-row">
				<div class="trp-column" style="width:70px">Translated by<br><a href="https://displayeurope.eu"><img src="' . esc_url( plugins_url( 'css/images/logo_displayeurope.png', __FILE__ ) ) . '" alt="Display Europe" style="height:40px; width:auto;"></a></div>
				<div class="trp-column">Co-funded by the European Union<br><img src="' . esc_url( plugins_url( 'css/images/logo_eu.png', __FILE__ ) ) . '" alt="European Union" style="height:40px; width:auto;"></div>
			</div>';

			// Add the translation engine
			// For Arabic and Bosnian/Croation/Serbian we use Google, for everything else DeepL
			$engine = in_array( $_REQUEST['trpl'], array( 'ar', 'hr', 'sr', 'bs' ) ) ? '(Google Translate)' : '(DeepL)';

			$return .= '<div class="trp-translation-disclaimer"><span class="trp-translation-disclaimer-text">' . sprintf( __( 'Translation is done via AI technology %s. The quality is limited by the used language model.', 'trp' ), $engine ) . '</span></div>';

			$return = '<p class="trp-translation-disclaimer-text">' . sprintf( __( 'This text has been auto-translated from %s.', 'trp' ), (string) TRP_Helper::get_all_languages()[ TRP_Helper::get_original_language_codes( $translation->post_id )[0]] ) . '</p>' . $return;

		}

		return $return;

	}

	return $content;

}

add_filter( 'the_title', 'trp_replace_post_title', 99, 2 );
add_filter( 'the_excerpt', 'trp_replace_post_excerpt', 99, 1 );
add_filter( 'the_content', 'trp_replace_post_content', 99, 1 );

function trp_switch_locale() {
	if( ! isset( $_REQUEST['trpl'] ) || strlen( $_REQUEST['trpl'] ) != 2 )
		return;
}
//add_action( 'locale', 'trp_switch_locale' );


/**
 * Replaces the <title> tag with a Transposer translation for single post views and pages
 *
 * @param     array    $titleparts     Array containing the title parts
 * @return    array
 */
function trp_filter_wp_title( $titleparts) {

	// Only replace title for single views and pages
	if( ! is_singular() || is_feed() )
		return $titleparts;

	if( ! isset( $_REQUEST['trpl'] ) || strlen( $_REQUEST['trpl'] ) != 2 )
		return $titleparts;

	if( ! in_array( $_REQUEST['trpl'], array_keys( TRP_Helper::get_all_languages() ) ) )
		return $titleparts;

	$TRP = new TRP;
	$translation = $TRP->get_post_translation( get_queried_object_id(), $_REQUEST['trpl'] );

	if( ! empty( $translation->post_title ) )
		$titleparts['title'] = $translation->post_title;

	return $titleparts;
}
add_filter( 'document_title_parts', 'trp_filter_wp_title' );

?>