<?php

//https://www.wpexplorer.com/wordpress-theme-options/


class TRP_Settings {

	public function __construct() {

		/**
		* Register options page to the admin_menu action hook
		*/
		add_action( 'admin_menu', array( $this, 'register_options_page' ) );

		add_action( 'admin_init', array( $this, 'register_settings' ) );

	}


	/**
	* Add a new options page named "My Options".
	*/
	public function register_options_page() {
		add_submenu_page(
			'transposer',
			esc_html__( 'Settings' ),
			esc_html__( 'Settings' ),
			'manage_options',
			'transposer-options',
			array( $this, 'options_page_html' )
		);
	}


	/**
	* The Options page
	*/
	public function options_page_html() {
		if ( ! current_user_can( 'manage_options' ) ) {
			return;
		}

		if ( isset( $_GET['settings-updated'] ) ) {
			add_settings_error(
				'options_messages',
				'options_message',
				esc_html__( 'Settings Saved', 'trp' ),
					'updated'
			);
		}

		settings_errors( 'options_messages' );

		?>

		<script language="javascript">
		// Switches option sections
		jQuery(document).ready(function($) {

			$('form#transposer-options table.form-table').each( function(i) {
				$(this).attr('id', i + 1);
			});
			$('form#transposer-options h2').each( function(i) {
				$(this).attr('class', i + 1);
			});

			// Hide all by default
			$('table.form-table').hide();
			$('h2').hide();

			// Display active group
			var activetab = '';
			if (typeof(localStorage) != 'undefined') {
				activetab = localStorage.getItem("activetab");
			}

			if (activetab != '' && $(activetab).length) {
				$('table' + activetab).fadeIn();
				$('h2.' + activetab.replace('#','')).fadeIn();
			} else {
				$('table.form-table:first').fadeIn();
				$('h2:first').fadeIn();
			}

			if (activetab != '' && $(activetab + '-tab').length) {
				$(activetab + '-tab').addClass('nav-tab-active');
			} else {
				$('.nav-tab-wrapper a:first').addClass('nav-tab-active');
			}

			$('.nav-tab-wrapper a').click(function(evt) {
				$('.nav-tab-wrapper a').removeClass('nav-tab-active');
				$(this).addClass('nav-tab-active').blur();
				var clicked_group = $(this).attr('href');
				if (typeof(localStorage) != 'undefined') {
					localStorage.setItem("activetab", $(this).attr('href'));
				}
				$('table.form-table').hide();
				$('h2').hide();
				$(clicked_group).fadeIn();
				$('h2.' + clicked_group.replace('#','')).fadeIn();
				evt.preventDefault();
			});
		});
		</script>

		<style type="text/css">
		.nav-tab-wrapper {
			border-bottom:1px solid #ccc;
		}
		span.wp-default {
			color:gray;
			padding-left:10px;
		}
		span.error {
			color:red;
			padding-left:10px;
		}
		</style>


		<div class="wrap">

			<?php screen_icon("options-general"); ?>
			<h1>Transposer <?php _e("Settings"); ?></h1>

			<h4 class="nav-tab-wrapper">
				<a id="1-tab" class="nav-tab" href="#1"><?php _e( 'General', 'trp' ); ?></a>
				<a id="2-tab" class="nav-tab" href="#2"><?php _e( 'Job queue', 'trp' ); ?></a>
				<a id="3-tab" class="nav-tab" href="#3"><?php _e( 'Repco API', 'trp' ); ?></a>
				<!--<a id="4-tab" class="nav-tab" href="#4"><?php _e( 'Frontend', 'trp' ); ?></a>-->
			</h4>

			<form action="options.php" method="post" id="transposer-options">
			<?php

				settings_fields( 'trp_options_ui' );
				do_settings_sections( 'trp_options_ui' );

				settings_fields( 'trp_options_jobs' );
				do_settings_sections( 'trp_options_jobs' );

				settings_fields( 'trp_options_repco' );
				do_settings_sections( 'trp_options_repco' );

				//settings_fields( 'trp_options_frontend' );
				//do_settings_sections( 'trp_options_frontend' );


				submit_button( 'Save Settings' );

			?>
			</form>
		</div>
		<?php

	}

	/**
	* Register settings
	*/
	public function register_settings() {


		register_setting( 'trp_options_repco', 'transposer_fallback_language', array( 'type' => 'string', 'sanitize_callback' => array( $this, 'sanitize_value' ) ) );
		register_setting( 'trp_options_repco', 'transposer_default_ui_post_types', array( 'type' => 'array', 'sanitize_callback' => array( $this, 'sanitize_array' ) ) );

		register_setting( 'trp_options_repco', 'transposer_enable_cron', array( 'type' => 'string', 'sanitize_callback' => array( $this, 'sanitize_value' ) ) );
		//register_setting( 'trp_options_repco', 'transposer_max_processes', array( 'type' => 'integer', 'sanitize_callback' => array( $this, 'sanitize_value' ) ) );
		register_setting( 'trp_options_repco', 'transposer_queue_order', array( 'type' => 'string', 'sanitize_callback' => array( $this, 'sanitize_value' ) ) );

		register_setting( 'trp_options_repco', 'transposer_prefer_translation_plugin', array( 'type' => 'string', 'sanitize_callback' => array( $this, 'sanitize_value' ) ) );
		register_setting( 'trp_options_repco', 'transposer_use_tags', array( 'type' => 'string', 'sanitize_callback' => array( $this, 'sanitize_tags' ) ) );
		register_setting( 'trp_options_repco', 'transposer_default_post_types', array( 'type' => 'array', 'sanitize_callback' => array( $this, 'sanitize_array' ) ) );
		register_setting( 'trp_options_repco', 'transposer_exclude_taxonomies', array( 'type' => 'array', 'sanitize_callback' => array( $this, 'sanitize_array' ) ) );
		register_setting( 'trp_options_repco', 'transposer_contributor_taxonomies', array( 'type' => 'array', 'sanitize_callback' => array( $this, 'sanitize_array' ) ) );
		register_setting( 'trp_options_repco', 'transposer_add_post_author', array( 'type' => 'string', 'sanitize_callback' => array( $this, 'sanitize_value' ) ) );
		register_setting( 'trp_options_repco', 'transposer_content_grouping_taxonomies', array( 'type' => 'array', 'sanitize_callback' => array( $this, 'sanitize_array' ) ) );
		register_setting( 'trp_options_repco', 'transposer_add_post_parent', array( 'type' => 'string', 'sanitize_callback' => array( $this, 'sanitize_value' ) ) );
		register_setting( 'trp_options_repco', 'transposer_do_api_shortcodes', array( 'type' => 'string', 'sanitize_callback' => array( $this, 'sanitize_value' ) ) );

		register_setting( 'trp_options_repco', 'transposer_do_frontend_shortcodes', array( 'type' => 'string', 'sanitize_callback' => array( $this, 'sanitize_value' ) ) );


		add_settings_section(
			'general_section',
			__( 'General', 'trp' ),
			false,
			'trp_options_ui'
		);


		add_settings_field(
			'transposer_fallback_language',
			esc_html__( 'Default language', 'trp' ),
			array( $this, 'transposer_fallback_language' ),
			'trp_options_ui',
			'general_section',
			array(
				'option_name' => 'transposer_fallback_language',
				'label_for' => 'transposer-fallback-language'
			)
		);


		add_settings_field(
			'transposer_default_ui_post_types',
			esc_html__( 'Make Transposer available for certain post types', 'trp' ),
			array( $this, 'transposer_default_ui_post_types' ),
			'trp_options_ui',
			'general_section',
			array(
				'option_name' => 'transposer_default_ui_post_types',
				'label_for' => 'transposer-default-ui-post_types'
			)
		);



		add_settings_section(
			'general_section',
			__( 'Job queue', 'trp' ),
			false,
			'trp_options_jobs'
		);


		/*
		add_settings_field(
			'transposer_api_endpoint',
			esc_html__( 'API endpoint', 'trp' ),
			array( $this, 'transposer_api_endpoint' ),
			'trp_options_jobs',
			'general_section',
			array(
				'option_name' => 'transposer_api_endpoint',
				'label_for' => 'transposer-api-endpoint'
			)
		);
		*/

		add_settings_field(
			'transposer_enable_cron',
			esc_html__( 'Process job queue automatically', 'trp' ),
			array( $this, 'transposer_enable_cron' ),
			'trp_options_jobs',
			'general_section',
			array(
				'option_name' => 'transposer_enable_cron',
				'label_for' => 'transposer-enable-cron'
			)
		);

/*
		add_settings_field(
			'transposer_max_processes',
			esc_html__( 'Maximum transposer jobs', 'trp' ),
			array( $this, 'transposer_max_processes' ),
			'trp_options_jobs',
			'general_section',
			array(
				'option_name' => 'transposer_max_processes',
				'label_for' => 'transposer-max-processes'
			)
		);
*/

		add_settings_field(
			'transposer_queue_order',
			esc_html__( 'Queue order', 'trp' ),
			array( $this, 'transposer_queue_order' ),
			'trp_options_jobs',
			'general_section',
			array(
				'option_name' => 'transposer_queue_order',
				'label_for' => 'transposer-queue-order'
			)
		);




		add_settings_section(
			'general_section',
			__( 'Repco API', 'trp' ),
			false,
			'trp_options_repco'
		);


		add_settings_field(
			'transposer_prefer_translation_plugin',
			esc_html__( 'Prefered translation plugin', 'trp' ),
			array( $this, 'transposer_prefer_translation_plugin' ),
			'trp_options_repco',
			'general_section',
			array(
				'option_name' => 'transposer_prefer_translation_plugin',
				'label_for' => 'transposer-prefer-translation-plugin'
			)
		);


		add_settings_field(
			'transposer_use_tags',
			esc_html__( 'Only expose posts with certain tags', 'trp' ),
			array( $this, 'transposer_use_tags' ),
			'trp_options_repco',
			'general_section',
			array(
				'option_name' => 'transposer_use_tags',
				'label_for' => 'transposer-use-tags'
			)
		);


		add_settings_field(
			'transposer_default_post_types',
			esc_html__( 'Default Post Types', 'trp' ),
			array( $this, 'transposer_default_post_types' ),
			'trp_options_repco',
			'general_section',
			array(
				'option_name' => 'transposer_default_post_types',
				'label_for' => 'transposer-default-post-types'
			)
		);

		add_settings_field(
			'transposer_exclude_taxonomies',
			esc_html__( 'Exclude taxonomies from tags', 'trp' ),
			array( $this, 'transposer_exclude_taxonomies' ),
			'trp_options_repco',
			'general_section',
			array(
				'option_name' => 'transposer_exclude_taxonomies',
				'label_for' => 'transposer-exclude-taxonomies'
			)
		);

		add_settings_field(
			'transposer_contributor_taxonomies',
			esc_html__( 'Contributor Taxonomies', 'trp' ),
			array( $this, 'transposer_contributor_taxonomies' ),
			'trp_options_repco',
			'general_section',
			array(
				'option_name' => 'transposer_contributor_taxonomies',
				'label_for' => 'transposer-contributor-taxonomies'
			)
		);

		add_settings_field(
			'transposer_add_post_author',
			esc_html__( "Add the post author to the Contributors?", 'trp' ),
			array( $this, 'transposer_add_post_author' ),
			'trp_options_repco',
			'general_section',
			array(
				'option_name' => 'transposer_add_post_author',
				'label_for' => 'transposer-add-post-author'
			)
		);

		add_settings_field(
			'transposer_content_grouping_taxonomies',
			esc_html__( "ContentGrouping Taxonomies", 'trp' ),
			array( $this, 'transposer_content_grouping_taxonomies' ),
			'trp_options_repco',
			'general_section',
			array(
				'option_name' => 'transposer_content_grouping_taxonomies',
				'label_for' => 'transposer-add-post-author'
			)
		);

		add_settings_field(
			'transposer_add_post_parent',
			esc_html__( "Use the contentItem's parent post as the ContentGrouping", 'trp' ),
			array( $this, 'transposer_add_post_parent' ),
			'trp_options_repco',
			'general_section',
			array(
				'option_name' => 'transposer_add_post_parent',
				'label_for' => 'transposer-add-post-parent'
			)
		);

		add_settings_field(
			'transposer_do_api_shortcodes',
			esc_html__( "Choose whether to execute or to remove shortcodes from the post content", 'trp' ),
			array( $this, 'transposer_do_api_shortcodes' ),
			'trp_options_repco',
			'general_section',
			array(
				'option_name' => 'transposer_do_api_shortcodes',
				'label_for' => 'transposer-do-api-shortcodes'
			)
		);


/*
		add_settings_section(
			'general_section',
			__( 'Frontend', 'trp' ),
			false,
			'trp_options_frontend'
		);
*/

		add_settings_field(
			'transposer_do_frontend_shortcodes',
			esc_html__( "Choose whether to execute or to remove shortcodes in the frontend", 'trp' ),
			array( $this, 'transposer_do_frontend_shortcodes' ),
			'trp_options_repco',
			'general_section',
			array(
				'option_name' => 'transposer_do_frontend_shortcodes',
				'label_for' => 'transposer-do-frontend-shortcodes'
			)
		);

	}


	/**
	* Fallback language
	*/
	public function transposer_fallback_language( $args ) {
		$value = get_option( 'transposer_fallback_language', 'en' );
		?>
		<select id="transposer-fallback-language" name="transposer_fallback_language">
		<?php foreach( TRP_Helper::get_all_languages() as $code => $lang ) :
			$selected = $value == $code ? 'selected="selected"' : ''; ?>
			<option value="<?php echo esc_attr( $code ); ?>" <?php echo $selected; ?>><?php echo $lang; ?></option>
		<?php endforeach; ?>
		</select>

		<p class="description"><?php esc_html_e( "If there's no language information, this language is used as a default, e.g. for presets in menus. Use the mostly used language of your content.", 'trp' ); ?></p>
		<?php
	}



	/**
	* Default UI post types: Make Transposer available for certain post types
	*/
	public function transposer_default_ui_post_types( $args ) {
		$value = get_option( 'transposer_default_ui_post_types', array( 'post' ) );
		$post_types = get_post_types();

		// Exclude certain post types generally that don't come into consideration
		$exclude = TRP_Helper::get_exclude_post_types();
		$post_types = array_diff( $post_types, $exclude );

		$i = 0;

		foreach( $value as $key => $val ) {

			?>
			<select id="transposer-default-ui-post-types-<?php echo $i; ?>" name="transposer_default_ui_post_types[]">
			<option value="">-<?php _e( 'Add new', 'trp' ); ?>-</option>
			<?php foreach( $post_types as $post_type ) :
				$selected = $val == $post_type ? 'selected="selected"' : ''; ?>
				<option value="<?php echo $post_type; ?>" <?php echo $selected; ?>><?php echo $post_type; ?></option>
			<?php endforeach; ?>
			</select>
			<?php

			$i++;

		}

		?>

		<select id="transposer-default-ui-post-types-<?php echo $i; ?>" name="transposer_default_ui_post_types[]">
			<option value="">-<?php _e( 'None', 'trp' ); ?>-</option>
			<?php foreach( $post_types as $post_type ) : ?>
				<option value="<?php echo $post_type; ?>"><?php echo $post_type; ?></option>
			<?php endforeach; ?>
		</select>

		<p class="description"><?php esc_html_e( 'Make Transposer available to these post types, e.g. auto-translations.', 'trp' ); ?></p>
		<?php

	}


	/**
	* API Endpoint
	*/
	public function transposer_api_endpoint( $args ) {
		$value = get_option( 'transposer_api_endpoint' );
		?>
		<input
			type="url"
			id="transposer-api-endpoint"
			name="transposer_api_endpoint"
			value="<?php echo esc_attr( $value ); ?>">
		<p class="description"><?php esc_html_e( 'URL to the Transposer API.', 'trp' ); ?></p>
		<?php
	}



	/**
	* Whether to enable WP cron jobs and thus API calls to Transposer
	*/
	public function transposer_enable_cron( $args ) {
		$value = get_option( 'transposer_enable_cron', '1' );

		$options = array( 1 => __( 'Yes', 'trp' ), 0 => __( 'No', 'trp' ) );

		?>

		<select id="transposer-enable-cron" name="transposer_enable_cron">
		<?php foreach( $options as $option => $option_caption ) :
			$selected = $option == $value ? 'selected="selected"' : ''; ?>
			<option value="<?php echo $option; ?>" <?php echo $selected; ?>><?php echo $option_caption; ?></option>
		<?php endforeach; ?>
		</select>

		<p class="description"><?php esc_html_e( "Whether to enable Transposer API calls. If turned off, the Transposer jobs won't work. However they are saved and will be processed as soon as this option is turned on again. Turn this off for debugging reasons.", 'trp' ); ?></p>
		<?php
	}


	/**
	* Maximum number of processes field
	*/
	public function transposer_max_processes( $args ) {
		$value = get_option( 'transposer_max_processes' );
		?>
		<input
			type="number"
			id="transposer-max-processes"
			name="transposer_max_processes"
			value="<?php echo esc_attr( $value ); ?>"
			min="1"
			max="5">
		<p class="description"><?php esc_html_e( 'Maximum number of simultaneous Transposer jobs.', 'trp' ); ?></p>
		<?php
	}


	/**
	* Queue order
	*/
	public function transposer_queue_order( $args ) {

		$value = strtoupper( get_option( 'transposer_queue_order', 'ASC' ) );
		?>
		<select id="transposer-queue-order" name="transposer_queue_order">
			<option value="ASC" <?php if( $value == 'ASC' ) echo 'selected="selected"'; ?>><?php _e( 'First come, first served', 'trp' ); ?></option>
			<option value="DESC" <?php if( $value == 'DESC' ) echo 'selected="selected"'; ?>><?php _e( 'Most recent first', 'trp' ); ?></option>
		</select>

		<p class="description"><?php esc_html_e( 'The order in which the job queue is processed.', 'trp' ); ?></p>
		<?php
	}






	/**
	* Prefered translation plugin
	* In case there are different translations in the same language available for the same post this option
	* lets you choose which translation done by which plugin to prefer
	*
	* TODO: Prevent this from being changed afterwards by a config constant or a filter or make it a part of an installation routine/wizard
	*       Otherwise we'd need to update all the modified dates of the concerned posts so that Repco gets to know about the change (or find another simple mechanism to trigger updates)
	*/
	public function transposer_prefer_translation_plugin( $args ) {
		$value = get_option( 'transposer_prefer_translation_plugin', 'transposer' );

		$options = array( 'transposer' => 'Transposer' );

		if( TRP_WPML::is_wpml() )
			$options['wpml'] = 'WPML';

		if( TRP_Eurozine::is_eurozine() )
			$options['eurozine'] = 'Eurozine';

		?>
		<select id="transposer-prefer-translation-plugin" name="transposer_prefer_translation_plugin">
		<?php foreach( $options as $option => $option_caption ) :
			$selected = $option == $value ? 'selected="selected"' : ''; ?>
			<option value="<?php echo $option; ?>" <?php echo $selected; ?>><?php echo $option_caption; ?></option>
		<?php endforeach; ?>
		</select>

		<p class="description"><?php esc_html_e( "In case there are different translations available in the same language, choose which translation of which plugin to prefer. The corresponding version will be delivered to Repco and displayed in your frontend.", 'trp' ); ?></p>
		<?php
	}


	/**
	* Default post types
	*/
	public function transposer_default_post_types( $args ) {
		$value = get_option( 'transposer_default_post_types', array( 'post' ) );
		$post_types = get_post_types();

		// Exclude certain post types generally that don't come into consideration
		$exclude = TRP_Helper::get_exclude_post_types();
		$post_types = array_diff( $post_types, $exclude );

		$i = 0;

		foreach( $value as $key => $val ) {

			?>
			<select id="transposer-default-post-types-<?php echo $i; ?>" name="transposer_default_post_types[]">
			<?php foreach( $post_types as $post_type ) :
				$selected = $val == $post_type ? 'selected="selected"' : ''; ?>
				<option value="<?php echo $post_type; ?>" <?php echo $selected; ?>><?php echo $post_type; ?></option>
			<?php endforeach; ?>
			</select>
			<?php

			$i++;

		}

		?>

		<select id="transposer-default-post-types-<?php echo $i; ?>" name="transposer_default_post_types[]">
			<option value="">-<?php _e( 'Add new', 'trp' ); ?>-</option>
			<?php foreach( $post_types as $post_type ) : ?>
				<option value="<?php echo $post_type; ?>"><?php echo $post_type; ?></option>
			<?php endforeach; ?>
		</select>

		<p class="description"><?php esc_html_e( 'The Post Types that represent Repco ContentItems. Choose those that you want Repco to ingest & syndicate as your main content items.', 'trp' ); ?></p>
		<?php

	}


	/**
	* Include tags
	*/
	public function transposer_use_tags( $args ) {
		$value = get_option( 'transposer_use_tags', array() );

		$value =  empty( $value )  ? '' : implode( ',', $value );

		?>
		<label for="transposer-use-tags"><input type="text" name="transposer_use_tags" value="<?php echo esc_attr( $value ); ?>"></label>

		<p class="description"><?php esc_html_e( 'Constrain the posts delivered to Repco to certain tags. Leave empty to share all published.', 'trp' ); ?> <?php _e( 'Separate tags by commas' ); ?></p>
		<?php

	}




	/**
	* Exclude taxonomies
	*/
	public function transposer_exclude_taxonomies( $args ) {
		$value = get_option( 'transposer_exclude_taxonomies', array() );
		$taxonomies = get_taxonomies();

		// Exclude certain taxonomies in general we know about and that don't come into consideration
		$exclude = TRP_Helper::get_exclude_taxonomies();
		$taxonomies = array_diff( $taxonomies, $exclude );

		$i = 0;

		foreach( $value as $key => $val ) {

			?>
			<select id="transposer-exclude-taxonomies-<?php echo $i; ?>" name="transposer_exclude_taxonomies[]">
			<option value="">-<?php _e( 'None', 'trp' ); ?>-</option>
			<?php foreach( $taxonomies as $taxonomy ) :
				$selected = $val == $taxonomy ? 'selected="selected"' : ''; ?>
				<option value="<?php echo $taxonomy; ?>" <?php echo $selected; ?>><?php echo $taxonomy; ?></option>
			<?php endforeach; ?>
			</select>
			<?php

			$i++;

		}

		?>

		<select id="transposer-exclude-taxonomies-<?php echo $i; ?>" name="transposer_exclude_taxonomies[]">
			<option value="">-<?php _e( 'Add new', 'trp' ); ?>-</option>
			<?php foreach( $taxonomies as $taxonomy ) : ?>
				<option value="<?php echo $taxonomy; ?>"><?php echo $taxonomy; ?></option>
			<?php endforeach; ?>
		</select>


		<p class="description"><?php esc_html_e( "Choose taxonomies that do not serve as a tag to describe a ContentItem or MediaAsset (like internally used taxonomies or information, that don't belong in the Concepts, like authors).", 'trp' ); ?></p><?php

	}


	/**
	* Contributor taxonomies
	*/
	public function transposer_contributor_taxonomies( $args ) {
		$value = get_option( 'transposer_contributor_taxonomies', array() );
		$taxonomies = get_taxonomies();

		// Exclude certain taxonomies in general we know about and that don't come into consideration
		$exclude = TRP_Helper::get_exclude_taxonomies();
		$taxonomies = array_diff( $taxonomies, $exclude );

		$i = 0;

		foreach( $value as $key => $val ) {

			?>
			<select id="transposer-contributor-taxonomies-<?php echo $i; ?>" name="transposer_contributor_taxonomies[]">
			<option value="">-<?php _e( 'None', 'trp' ); ?>-</option>
			<?php foreach( $taxonomies as $taxonomy ) :
				$selected = $val == $taxonomy ? 'selected="selected"' : ''; ?>
				<option value="<?php echo $taxonomy; ?>" <?php echo $selected; ?>><?php echo $taxonomy; ?></option>
			<?php endforeach; ?>
			</select>
			<?php

			$i++;

		}

		?>

		<select id="transposer-contributor-taxonomies-<?php echo $i; ?>" name="transposer_contributor_taxonomies[]">
			<option value="">-<?php _e( 'Add new', 'trp' ); ?>-</option>
			<?php foreach( $taxonomies as $taxonomy ) : ?>
				<option value="<?php echo $taxonomy; ?>"><?php echo $taxonomy; ?></option>
			<?php endforeach; ?>
		</select>

		<p class="description"><?php esc_html_e( 'Taxonomies that represent Repco contributors. The can be e.g. authors, moderators, guests, reviewers, translators, ...', 'trp' ); ?></p>
		<?php

	}


	/**
	* Whether to add the post_author to the contributors in the Repco API
	*/
	public function transposer_add_post_author( $args ) {
		$value = get_option( 'transposer_add_post_author', '1' );

		$options = array( 1 => __( 'Yes', 'trp' ), 0 => __( 'No', 'trp' ) );

		?>

		<select id="transposer-add-post-author" name="transposer_add_post_author">
		<?php foreach( $options as $option => $option_caption ) :
			$selected = $option == $value ? 'selected="selected"' : ''; ?>
			<option value="<?php echo $option; ?>" <?php echo $selected; ?>><?php echo $option_caption; ?></option>
		<?php endforeach; ?>
		</select>

		<p class="description"><?php esc_html_e( "Whether to add the user who created the post to the contributors.", 'trp' ); ?></p>
		<?php
	}


	/**
	* ContentGrouping taxonomies
	*/
	public function transposer_content_grouping_taxonomies( $args ) {
		$value = get_option( 'transposer_content_grouping_taxonomies', array() );
		$taxonomies = get_taxonomies();

		// Exclude certain taxonomies in general we know about and that don't come into consideration
		$exclude = TRP_Helper::get_exclude_taxonomies();
		$taxonomies = array_diff( $taxonomies, $exclude );

		$i = 0;

		foreach( $value as $key => $val ) {

			?>
			<select id="transposer-content-grouping-taxonomies-<?php echo $i; ?>" name="transposer_content_grouping_taxonomies[]">
			<option value="">-<?php _e( 'None', 'trp' ); ?>-</option>
			<?php foreach( $taxonomies as $taxonomy ) :
				$selected = $val == $taxonomy ? 'selected="selected"' : ''; ?>
				<option value="<?php echo $taxonomy; ?>" <?php echo $selected; ?>><?php echo $taxonomy; ?></option>
			<?php endforeach; ?>
			</select>
			<?php

			$i++;

		}

		?>

		<select id="transposer-contributor-taxonomies<?php echo $i; ?>" name="transposer_content_grouping_taxonomies[]">
			<option value="">-<?php _e( 'Add new', 'trp' ); ?>-</option>
			<?php foreach( $taxonomies as $taxonomy ) : ?>
				<option value="<?php echo $taxonomy; ?>"><?php echo $taxonomy; ?></option>
			<?php endforeach; ?>
		</select>

		<p class="description"><?php esc_html_e( 'Taxonomies that represent Repco ContentGroupings. It can be an episodic information like a TV or podcasting series as well as a name of a collection of contentItems.', 'trp' ); ?></p>
		<?php

	}


	/**
	* Whether to add the contentItems' parent posts as the contentGrouping
	*/
	public function transposer_add_post_parent( $args ) {
		$value = get_option( 'transposer_add_post_parent', '1' );

		$options = array( 1 => __( 'Yes', 'trp' ), 0 => __( 'No', 'trp' ) );

		?>

		<select id="transposer-add-post-parent" name="transposer_add_post_parent">
		<?php foreach( $options as $option => $option_caption ) :
			$selected = $option == $value ? 'selected="selected"' : ''; ?>
			<option value="<?php echo $option; ?>" <?php echo $selected; ?>><?php echo $option_caption; ?></option>
		<?php endforeach; ?>
		</select>

		<p class="description"><?php esc_html_e( "Whether to add the contentItems' parent posts as the ContentGrouping.", 'trp' ); ?></p>
		<?php
	}


	/**
	* Whether to render or remove shortcodes in the Repco API
	*/
	public function transposer_do_api_shortcodes( $args ) {
		$value = get_option( 'transposer_do_api_shortcodes', '0' );

		$options = array( 1 => __( 'Render shortcodes', 'trp' ), 0 => __( 'Remove shortcodes', 'trp' ) );

		?>

		<select id="transposer-do-api-shortcodes" name="transposer_do_api_shortcodes">
		<?php foreach( $options as $option => $option_caption ) :
			$selected = $option == $value ? 'selected="selected"' : ''; ?>
			<option value="<?php echo $option; ?>" <?php echo $selected; ?>><?php echo $option_caption; ?></option>
		<?php endforeach; ?>
		</select>

		<p class="description"><?php esc_html_e( "Whether to render or remove shortcodes in the Repco API.", 'trp' ); ?></p>
		<?php
	}


	/**
	* Whether to render or remove shortcodes in translations when displaying them in the frontend
	*/
	public function transposer_do_frontend_shortcodes( $args ) {
		$value = get_option( 'transposer_do_frontend_shortcodes', '0' );

		$options = array( 1 => __( 'Render shortcodes', 'trp' ), 0 => __( 'Remove shortcodes', 'trp' ) );

		?>

		<select id="transposer-do-frontend-shortcodes" name="transposer_do_frontend_shortcodes">
		<?php foreach( $options as $option => $option_caption ) :
			$selected = $option == $value ? 'selected="selected"' : ''; ?>
			<option value="<?php echo $option; ?>" <?php echo $selected; ?>><?php echo $option_caption; ?></option>
		<?php endforeach; ?>
		</select>

		<p class="description"><?php esc_html_e( "Whether to render or remove shortcodes in translations when displaying them in the frontend.", 'trp' ); ?></p>
		<?php
	}



	/**
	 * TODO: Sanitize scalar field values
	 */
	public function sanitize_value( $value ) {
		return $value;
	}


	/**
	 * Sanitize comma separated tags
	 */
	public function sanitize_tags( $value ) {
		$input_tags = explode( ',', $value ); // Put tags into array
		$output_tags = array();

		foreach( $input_tags as $tag ) {
			$output_tags[] = sanitize_title( trim( $tag ) );
		}

		return $output_tags;
	}


	/**
	 * Sanitize arrays
	 */
	public function sanitize_array( $array ) {

		if( ! is_array( $array ) )
			return array();

		// Remove empty values and duplicates
		return array_unique( array_filter( $array, function( $value ) { return ! is_null( $value ) && ! empty( $value ); } ) );
	}

}

$TRP_Settings = new TRP_Settings;

?>