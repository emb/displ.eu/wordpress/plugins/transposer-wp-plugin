<?php

/**
 * TODO
 * - Licenses
 */

class TRP_Repco {

	private $per_page = 50;

	private $page = 1;

	private $offset = 0;

	private $orderby = 'post_modified';

	private $order = 'ASC';


	public function __construct() {

		/**
		 * Register REST endpoint
		 * /wp-json/transposer/v1/repco
		 *
		 * Displays all reviewed posts for all media types
		 */
		add_action( 'rest_api_init', function () {
		    register_rest_route( 'transposer/v1/', '/repco/', array(
					'methods' => 'GET',
					'callback' => array( $this, 'rest_repco' ),
					'permission_callback' => '__return_true'
		    ) );
		});


	}


	/**
	 * The endpoint /wp-json/transposer/v1/repco
	 *
	 * Provides:
	 *   contentItems:       The whole post object and its translations of title, text and summary
	 *	                      ContentUrls to each language version
	 *                       ContentGroupings (taxonomies)
	 *                       Concepts (tags & categories)
	 *                       Contributors (uploaders & authors)
	 *
	 *   mediaAssets:        Media files (attachments & featured image including thumbnail sizes)
	 *                       Transcript (as plain text)
	 *                       Subtitles (WebVTT/SRT URL)
	 *                       Translations
	 *                       Chapters
    *
	 *   publicationService: The platform's publisher's information
	 *
	 *
	 * Pagination:
	 *
	 *    Use ?page=&per_page=
	 *    Default limit is 100
	 *    Max limit is 200
	 *
	 */
	public function rest_repco() {

		$posts = $this->get_posts();

		$total = $posts->found_posts;
		$pages = ceil( $total / $this->per_page );

	   // Add headers for pagination
	   $response = new WP_REST_Response( $this->filter_posts( $posts ), 200 );
	   $response->header( 'X-WP-Total', $total ); // Total number of found posts
	   $response->header( 'X-WP-TotalPages', $pages ); // Maximum number of pages

	   return $response;

	}


	/**
	 * Build the Post Query
	 *
	 * Takes care of
	 *  - Pagination
	 *  - Sorting
	 *  - Search by search term (?s=my+search+term)
	 *  - Search by category (?cat=1)
	 *  - Search by language (?lang=de)
	 *  - Filter by date (?date=2023-01-01)
	 *
	 * Returns the WP_Query object
	 */
	private function get_posts() {

		$init_limit = 50; // Standard limit if not set
		$max_limit = 200; // Maximum allowed limit

		$this->page = ( isset( $_GET['page'] ) && is_numeric( $_GET['page'] ) && $_GET['page'] > 0 ) ? $_GET['page'] : 1;
		$this->per_page = ( isset( $_GET['per_page'] ) && is_numeric( $_GET['per_page'] ) && $_GET['per_page'] > 0 ) ? $_GET['per_page'] : $init_limit;
		$this->offset = ( $this->page - 1 ) * $this->per_page;

		// Limit the limit number to $max_limit
		if( $this->per_page > $max_limit )
			$this->per_page = $max_limit;

		$post_ids = array();

		// Get original post IDs (means it is not a translation)
		// We only expose the originals of a certain post type in the API (which is usually 'post'), including their translations already in their own API entry

		$post_types = get_option( 'transposer_default_post_types', array( 'post' ) );

		// If the options is corrupt for some reason, set post_type 'post' as default, we need that information in any way
		if( ! is_array( $post_types ) || empty( $post_types ) )
			$post_types = array( 'post' );

		if( TRP_WPML::is_wpml() ) {

			/**
			 * WPML
			 */

			$post_ids = TRP_WPML::get_all_originals( $post_types );

		} elseif( TRP_Eurozine::is_eurozine() ) {

			/**
			 * EUROZINE
			 */

			$post_ids = TRP_Eurozine::get_all_originals( $post_types );

		} else {

			/**
			 * TRANSPOSER
			 * If none of the above, we assume there is no translation utility and just return all the post IDs of the post types
			 */

			$post_ids = get_posts( array( 'fields' => 'ids', 'post_type' => $post_types, 'numberposts' => -1, 'post_status' => 'publish' ) );

		}

		$posts = array();
		$return = array();

		// Set default orderby and order values, otherwise take the possible GET parameters if available
		$this->orderby = ( isset( $_GET["orderby"] ) && ( $_GET["orderby"] == "date" ) ) ? 'post_date' : $this->orderby; // Either 'post_date' or 'post_modified'
		$this->order = ( isset( $_GET["order"] ) && ( strtolower( $_GET["order"] ) == "asc" || strtolower( $_GET["order"] ) == "desc" ) ) ? strtoupper( $_GET["order"] ) : $this->order;


		/**
		 * Filter by Post Tags
		 */

		$expose_none = false;

		// Retrieve only posts by given tags if set in the options
		// Otherwise retrieve all
		$use_tags = get_option( 'transposer_use_tags', array() );

		// Remove empty array elements
		$use_tags = array_filter( $use_tags, fn( $value ) => ! is_null( $value ) && str_replace( ' ', '', $value ) !== '' );

		// Get the tag's term IDs
		if( is_array( $use_tags ) && ! empty( $use_tags ) ) {

			$term_ids = get_terms (
				array(
					'slug'     => $use_tags,
					'taxonomy' => 'post_tag',
					'fields'   => 'ids',
					'suppress_filter' => true
				)
			);

			// If tags were given but the terms weren't found, don't expose any of the posts in the API
			// It means someone wanted to filter by tags but the tag doesn't exist. It for sure doesn't mean to expose all
			if( ! is_array( $term_ids ) || empty( $term_ids ) )
				$expose_none = true;

		}

		/**
		 * Build the query
		 * We don't use WP_Query here on purpose since WPML changes the query even when suppress_filters=1 which changes the post__in array
		 * Otherwise we don't get the correct output
		 */
		global $wpdb;

		$sql_from = "SELECT p.ID, p.post_author, p.post_date, p.post_date_gmt, p.post_content, p.post_title, p.post_excerpt, p.post_status, p.comment_status, p.post_modified, p.post_modified_gmt, p.post_parent FROM " . $wpdb->posts . " p";
		$sql_total_from = "SELECT COUNT(DISTINCT(p.ID)) FROM ". $wpdb->posts . " p";

		$sql = '';

		// Add FROM for term filtering
		if( is_array( $term_ids ) && ! empty( $term_ids ) ) {
			$sql = ", " . $wpdb->term_taxonomy . " tt, " . $wpdb->term_relationships . " tr ";
		}

		// Add the default WHERE
		$sql .= " WHERE p.post_status='publish' AND p.post_type IN('" . implode( "','", $post_types ) . "') AND p.ID IN(" . implode( ',', $post_ids ) . ") ";

		// Add WHERE for term filtering
		if( is_array( $term_ids ) && ! empty( $term_ids ) )
			$sql .= " AND tt.term_id IN(" . implode( ',', $term_ids ) . ") AND tt.term_taxonomy_id=tr.term_taxonomy_id AND tr.object_id=p.ID ";

		// Make sure to select no posts at all
		if( $expose_none )
			$sql .= " AND 1=2 ";

		$sql_total = $sql_total_from . $sql;

		// Add GROUP and ORDER BY
		$sql .= " GROUP BY p.ID ORDER BY p.". $this->orderby . " " . $this->order;

		// Add pagination
		$sql .= " LIMIT " . $this->offset . ", " . $this->per_page;

		$sql = $sql_from . $sql;

		if( isset( $_GET['dev'] ) ) {
			echo "
				SQL:
				$sql;


				SQL Total:
				$sql_total;


				POST TYPES:
				";

				var_dump( $post_types );

				echo "


				TERM IDS:
				";

				var_dump( $term_ids );

				echo "


				USE TAGS:
				";

				var_dump( $use_tags );

				$return = $wpdb->get_results( $wpdb->prepare( $sql ) );
				echo "


				LAST ERROR:
				" . $wpdb->last_error;

				exit;

		}

		// Get total amount of posts
		$found_posts = $wpdb->get_var( $wpdb->prepare( $sql_total ) );

		// ... and only get the posts for the current page
		$return = new StdClass;
		$return->posts = $wpdb->get_results( $wpdb->prepare( $sql ) );
		$return->found_posts = $found_posts;

		return $return;

	}


	/**
	 * Object keys exposed in the API represent the Repco DB specification and are therefore different
	 *
	 * Takes the return of WP_Query and returns an array in the form of:
	 *
	 * [0] => Array
    *     (
    *         [contentItems] => stdClass Object
    *
    *         [mediaAssets] => Array
    *                       (
	 *                           [0] => stdClass Object
	 *                       )
    *     )
    *
	 * @param   object    A WP_Query object
	 * @return  array     An array containing all necessary infos for ingesting a Repco contentItem. Each element contains a post and its attachments, including concepts, files, licenses, authors, chapters, transcripts, etc.
	 */
	private function filter_posts( $posts ) {

		$return = array(); // The return array containing contentItems and mediaAssets

		$TRP = new TRP;

		$fallback_lang = get_option( 'transposer_fallback_language', 'en' );


		/**
		 * ContentItems
		 */

		// Each original post
		foreach( $posts->posts as $key => $post ) {

			$filtered_post = new StdClass; // The output object representing the contentItem including all the necessary entities
			$filtered_attachments = array(); // Collector array for mediaAsset objects

			// Add permalink to the original post object to have it available later
			$post->permalink = get_post_permalink( $post->ID );

			/* Create and populate the ContentItem object */
			$RepcoPostTranslations = new TRP_RepcoPostTranslations( $post ); 	// Get post translations

			$filtered_post->ID = $post->ID;
			$filtered_post->title = $RepcoPostTranslations->get_field_translations( 'post_title' ); //get_the_title( $post );
			$filtered_post->subtitle = $RepcoPostTranslations->get_field_translations( 'subtitle' );
			$filtered_post->summary = $RepcoPostTranslations->get_field_translations( 'post_excerpt' );
			$filtered_post->content = $RepcoPostTranslations->get_field_translations( 'post_content' ); // Get the rendered post content
			$filtered_post->pubDate = $post->post_date;
			$filtered_post->modifiedDate = $post->post_modified;
			$filtered_post->commentStatus = $post->comment_status; // TODO: Integrate in repco?
			$filtered_post->contentUrl = $RepcoPostTranslations->get_field_translations( 'permalink' ); //get_post_permalink( $post->ID );
			$filtered_post->originalLanguages['language_codes'] = TRP_Helper::get_original_language_codes( $post->ID );
			$filtered_post->teaserImageUid = get_post_thumbnail_id( $post->ID ); // Relation the mediaAsset's ID that is used as the teaserImage (= featured image/thumbnail)


			/**
			 * ContentGroupings
			 *
			 * Can either derive from term taxonomies or the given post's parent
			 */
			$filtered_post->contentGrouping = array();
			$content_grouping_taxonomies = get_option( 'transposer_content_grouping_taxonomies', array() );

			/* Add Groupings derived by taxonomies defined in the settings */
			if( is_array( $content_grouping_taxonomies ) && count( $content_grouping_taxonomies ) > 0 ) {
				$cg_terms = wp_get_post_terms( $post->ID, $content_grouping_taxonomies );

				foreach( $cg_terms as $cgterm ) {
					$filtered_post->contentGrouping[] = TRP_RepcoContentGrouping::get_instance( $cgterm, 'term' );
				}
			}

			/* Add the post_parent as the Grouping if so defined in the settings */
			if( get_option( 'transposer_add_post_parent' ) == '1' ) {
				$filtered_post->contentGrouping[] = TRP_RepcoContentGrouping::get_instance( $post->post_parent, 'post' );
			}


			/**
			 * Concepts
			 */
			$RepcoConcepts = new TRP_RepcoConcepts( $post );
			$filtered_post->concepts = $RepcoConcepts->get_concepts();


			/**
			 * Contributors
			 */
			$filtered_post->contributors = array();
			$filtered_contributors = array();

			/* Always add the post_author as the role "uploader" to get an email address for contacting */
			$contributor = new StdClass;
			$contributor->id = get_the_author_meta( 'ID', $post->post_author ); // The id will be the post_author which is the ID of the user who created the post
			$contributor->name = get_the_author_meta( 'display_name', $post->post_author );
			$contributor->description = get_the_author_meta( 'description', $post->post_author );
			$contributor->role = 'uploader';
			$contributor->contactInformation = get_the_author_meta( 'user_email', $post->post_author );

			$filtered_contributors[] = TRP_RepcoContributor::get_instance( $contributor );

			/* Add the post_author as a contributor, if defined in the settings */
			if( get_option( 'transposer_add_post_author' ) == '1' ) {
				$contributor = new StdClass;
				$contributor->id = get_the_author_meta( 'ID', $post->post_author ); // The id will be the post_author which is the ID of the user who created the post
				$contributor->name = get_the_author_meta( 'display_name', $post->post_author );
				$contributor->description = get_the_author_meta( 'description', $post->post_author );
				$contributor->role = 'author';
				$contributor->contactInformation = get_the_author_meta( 'user_email', $post->post_author );

				$filtered_contributors[] = TRP_RepcoContributor::get_instance( $contributor );
			}

			/* Get all the contributors as defined in the settings */
			$contributor_taxonomies = get_option( 'transposer_contributor_taxonomies', array() );

			if( is_array( $contributor_taxonomies ) && count( $contributor_taxonomies ) > 0 ) {

				// Get all the terms of the defined contributor taxonomies
				$contributors = wp_get_post_terms( $post->ID, $contributor_taxonomies );

				foreach( $contributors as $contributor ) {
					$contributor = TRP_RepcoContributor::get_instance( $contributor );

					// TODO: Add term meta to the $contributor WP_Term object (like description/biography or similar)
					$filtered_contributors[] = $contributor;
				}

			}


			// Eurozine has its contributors in postmeta keys 'copyright', 'first_published' (publisher) and 'contributor'
			if( TRP_Eurozine::is_eurozine() ) {
				$ez_author_val = get_post_meta( $post->ID, 'copyright', true ); // Contains the original author's information, add as role "author"
				//$ez_publisher_val = get_post_meta( $post->ID, 'first_published', true ); // Contains the publisher information, add as role "publisher"
				$ez_contributor_val = get_post_meta( $post->ID, 'contributor', true ); // Contains contributor information, add as role "contributor"

				$ez_contributor = new StdClass;
				$ez_contributor->id = 0;
				$ez_contributor->description = '';
				$ez_contributor->url = '';

				if( ! empty( $ez_author_val ) ) {
					$ez_contributor->name = $ez_author_val;
					$ez_contributor->role = 'author';
					$filtered_contributors[] = TRP_RepcoContributor::get_instance( $ez_contributor );
				}

				/*
				if( ! empty( $ez_publisher_val ) ) {
					$ez_contributor->name = $ez_publisher_val;
					$ez_contributor->role = 'publisher';
					$filtered_contributors[] = TRP_RepcoContributor::get_instance( $ez_contributor );
				}
				*/

				if( ! empty( $ez_contributor_val ) ) {
					$ez_contributor->name = $ez_contributor_val;
					$ez_contributor->role = 'contributor';
					$filtered_contributors[] = TRP_RepcoContributor::get_instance( $ez_contributor );
				}

			}

			$filtered_post->contributors = $filtered_contributors;


			/**
			 * TODO: License
			 */


			/**
			 * Media Assets
			 */
			$args = array();
			$args['post_type'] = 'attachment';
			$args['post_status'] = 'inherit';
			$args['post_parent'] = $post->ID;
			$args['suppress_filters'] = 1;

			$attachments = new WP_Query( $args );
			$attachments = $attachments->posts;


			/**
			 * Add the featured image (thumbnail) to mediaAssets if not already there
			 */

			$attachment_ids = array_column( (array) $attachments, 'ID' ); // 1. Extract all the attachment IDs
			$thumbnail_id = get_post_thumbnail_id( $post->ID ); // 2. Get the thumbnail's attachment ID (if there)

			// 3. If the thumbnail isn't part of the attachments, add it to them as the first mediaAsset item
			if( $thumbnail_id > 0 && ! in_array( $thumbnail_id, $attachment_ids ) ) {

				$thumbnail = get_post( $thumbnail_id ); // Get the thumbnail's post object

				// Add all the files to the mediaAsset object
				$thumbnail->files = TRP_RepcoFiles::get_files( $thumbnail );

				array_unshift( $attachments, $thumbnail ); // Prepend the object to the front of the full attachments array

			}


			/* Add all the other attachments as MediaAssets */
			foreach( $attachments as $att_key => $attachment ) {

				$filtered_attachments[$att_key] = new StdClass;
				$filtered_attachments[$att_key]->ID = $attachment->ID;

				/* Attachment translations */
				$RepcoPostTranslations = new TRP_RepcoPostTranslations( $attachment );
				$filtered_attachments[$att_key]->title = $RepcoPostTranslations->get_field_translations( 'post_title' ); // get_the_title( $attachment );
				$filtered_attachments[$att_key]->summary = $RepcoPostTranslations->get_field_translations( 'post_excerpt' );
				$filtered_attachments[$att_key]->content = $RepcoPostTranslations->get_field_translations( 'post_content' );

				// Eurozine has the thumbnail's image caption in a post meta related to the parent post (not the attachment)
				if( TRP_Eurozine::is_eurozine() ) {
				   $filtered_attachments[$att_key]->content = new StdClass;
				   $image_caption = get_post_meta( $post->ID, 'image_caption', true );

					if( ! empty( $image_caption ) )
						$filtered_attachments[$att_key]->content->$fallback_lang = array( 'value' => $image_caption );
				}

				$filtered_attachments[$att_key]->mediaType = str_replace( 'application', 'document', stristr( $attachment->post_mime_type, '/', true ) ); // audio, video, image, document. ''application will be converted to 'document'

				/* TODO: License */
				//$filtered_attachments[$att_key]->license = get_post_meta( $attachment->ID, 'license', true ); // TODO: Care for it, otherwise it won't be there
				//$filtered_attachments[$att_key]->contains_copyrighted_material = $attachment['meta']['copyright'][0]; // TODO: What to do with it?

				/* Concepts for MediaAssets */
				$tags = wp_get_post_tags( $attachment->ID ); // Add post_tags
				$category = get_the_category( $attachment->ID ); // Add the category TODO: Add parent

				$concepts = array_merge( $tags, $category );
				$filtered_attachments[$att_key]->concepts = $concepts;


				/**
				 * Transcripts & Chapters
				 *
				 * If the file is an audio or video, add transcripts and chapters if available
				 */
				if( stristr( $attachment->post_mime_type, '/', true ) == 'audio' || stristr( $attachment->post_mime_type, '/', true ) == 'video' ) {

					/**
					 * Transcripts & Subtitle URLs
					 */
					$filtered_attachments[$att_key]->transcripts = $TRP->get_transcripts( $attachment->ID );

					/**
					 * Chapters (in Transposer called 'regions')
					 */
					$regions = new TRP_Regions( $attachment->ID );
					$filtered_attachments[$att_key]->chapters = $regions->get_regions();

					// Remove some fields we don't need to share
					foreach( $filtered_attachments[$att_key]->transcripts as $tr_key => $transcript ) {
						$filtered_attachments[$att_key]->transcripts[$tr_key] = (object) array_diff( (array) $filtered_attachments[$att_key]->transcripts[$tr_key], array( 'job_id', 'media_id' ) );
					}

				}


				/**
				 * Files
				 */
				$filtered_attachments[$att_key]->files = TRP_RepcoFiles::get_files( $attachment );

			}


			// Put together the return array
			$return[$key]['contentItem'] = $filtered_post;
			$return[$key]['mediaAssets'] = $filtered_attachments;


			/**
			 * Publication Service
			 */
			$return[$key]['publicationService'] = new StdClass;
			$return[$key]['publicationService']->medium = '';
			$return[$key]['publicationService']->address = get_bloginfo('url');

			// Name is translatable, so it needs to be in an array
			// e.g. in JSON { 'en': { 'value': 'Eurozine' } }
			//$filtered_post->originalLanguages['language_codes'][0]
			$return[$key]['publicationService']->name[$fallback_lang] = array( 'value' => get_bloginfo( 'name' ) );



		}

		return $return;

	}


}


// RepcoPostTranslations
require( TRP_ABSPATH . '/classes/RepcoPostTranslations.php' );

// RepcoFiles
require( TRP_ABSPATH . '/classes/RepcoFiles.php' );

// RepcoConcepts
require( TRP_ABSPATH . '/classes/RepcoConcepts.php' );

// RepcoConcept
require( TRP_ABSPATH . '/classes/RepcoConcept.php' );

// RepcoContentGrouping
require( TRP_ABSPATH . '/classes/RepcoContentGrouping.php' );

// RepcoContributor
require( TRP_ABSPATH . '/classes/RepcoContributor.php' );

$TRP_Repco = new TRP_Repco;

?>