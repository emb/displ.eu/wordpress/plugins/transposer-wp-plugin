jQuery(document).ready( function() {


	jQuery(document).on( 'click', '.trp-edit-transcript', function() {
		let transcript_id = jQuery(this).attr('transcript_id');

		console.log( transcript_id );

		jQuery('body').prepend('<div id="body-overlay"></div>');
		jQuery('body').prepend('<iframe id="trp-editor" frameborder="0" scrolling="no" src="' + trp_transcript_editor_path + '&transcript_id=' + transcript_id + '"></iframe>');
		//jQuery('body').prepend('<div id="trp-editor"></div>');
		//jQuery('#trp-editor').load('/cba-core/wp-content/plugins/transposer/js/editor/build/?audio_id=18402');

	});


	jQuery(document).on( 'click', '.trp-edit-regions', function() {
		let post_id = jQuery(this).attr('post_id');

		console.log( post_id );

		jQuery('body').prepend('<div id="body-overlay"></div>');
		jQuery('body').prepend('<iframe id="trp-editor" frameborder="0" scrolling="no" src="' + trp_region_editor_path + '&post_id=' + post_id + '"></iframe>');

	});




	/**
	 * When clicking on an trp-edit-translation icon open a modal to directly edit the post translation
	 */
	jQuery(document).on( 'click', '.trp-edit-translation', function() {

		let translation_id = jQuery(this).attr('translation_id');
		let nonce = jQuery(this).attr('nonce');

		jQuery('body').prepend('<div id="body-overlay"></div>');
		jQuery('#body-overlay').html( trp_loader );

		jQuery.ajax({
			type: "GET",
			url: ajaxurl,
			data: {
				'action': 'trp_get_translation',
				'translation_id': translation_id,
				'_ajax_nonce' : nonce
			},
			dataType: 'json',

			// Display the translation editor in a modal
			success: function(response) {

				jQuery('#body-overlay').remove();

				// Populate the textarea with the post content and scroll to top
				jQuery("#trp-edit-translation #trp-post-title").val(response.post_title);
				jQuery("#trp-edit-translation #trp-post-subtitle").val(response.subtitle);
				jQuery("#trp-edit-translation #trp-post-excerpt").val(response.post_excerpt);
				//jQuery("#trp-edit-translation #trp-post-content").val(response.post_content);

				console.log('response: ' + response );
				jQuery('#trp-edit-translation').removeClass('hidden');

				// Initialize TinyMCE for post_content field
				jQuery("#trp-post-content").val(response.post_content);
				//wp.editor.initialize("trp-post-content", { tinymce: { wpautop: true, plugins : 'charmap colorpicker compat3x directionality fullscreen hr image lists media paste tabfocus textcolor wordpress wpautoresize wpdialogs wpeditimage wpemoji wpgallery wplink wptextpattern wpview', toolbar1: 'bold italic underline strikethrough | bullist numlist | blockquote hr | alignleft aligncenter alignright | link unlink | fullscreen | wp_adv', toolbar2: 'formatselect alignjustify forecolor | pastetext removeformat charmap | outdent indent | undo redo | wp_help' } });

				jQuery( "#trp-edit-translation" ).dialog({
					resizable: true,
					height: "auto",
					width: "75%",
					modal: true,
					closeText:'',
					buttons: {
						"Save": function(event) {

							event.preventDefault();

							let post_title = jQuery("#trp-post-title").val();
							let subtitle = jQuery("#trp-post-subtitle").val();
							let post_excerpt = jQuery("#trp-post-excerpt").val();
							let post_content = jQuery("#trp-post-content").val();

							// When the save button was clicked
							// Post the AJAX request to save the content to the Web VTT file
							jQuery.ajax({
								type: "POST",
								url: ajaxurl,
								data: {
									'action': 'trp_save_translation',
									'translation_id': translation_id,
									'post_title': post_title,
									'subtitle': subtitle,
									'post_content': post_content,
									'post_excerpt': post_excerpt,
									'_ajax_nonce': nonce
								},
								dataType: 'json',

								// Display a status message after saving
								success: function(response) {

									jQuery('#trp-edit-translation .trp-message').html( response.message );
									console.log(response);

								},

								error: function( response ) {
									TRPNotify(response);
								}

							});

							//jQuery( this ).dialog( "close" );
						},

						Cancel: function() {
							jQuery( this ).dialog( "close" );
						}
					}
				});


			}, // success

			error: function(response) {
				console.log(response.responseText);
				TRPNotify(response.responseText);
			}

		});

	});




	// Close the modal when clicking outside of it
	jQuery(document).on( 'click', '#body-overlay, .ui-widget-overlay', function() {
		jQuery('#body-overlay').remove();
		jQuery('#trp-editor').remove();
		jQuery('.trp-selector-container').html('');
	});





	// Create translations from a post
	jQuery(document).on( 'click', '.trp-create-translations', function() {

		let post_id = jQuery(this).parents('.trp-menu').attr('post_id');
		let nonce = jQuery(this).parents('.trp-menu').attr('nonce');

		let container = jQuery(this).parents('#trp-container-' + post_id );
		//container.html( trp_loader );

		// We always translate from the original post
		// so we display the original post title to the user to indicate that
		jQuery('#trp-original-post-caption').html( trp_loader ); // Show the loading icon until the request is ready

		jQuery.ajax({
			type: "GET",
			url: ajaxurl,
			data: {
				'action': 'trp_get_original_post',
				'post_id': post_id
			},
			dataType: 'json',

			// Re-Initialize the progress and try to retrieve a new job ID
			success: function(response) {
				jQuery('#trp-original-post-caption').html( response.post_title );
			}

		});



		// Re-enable all the checkboxes
		jQuery('#trp-language-selector-form input[type="checkbox"]').removeAttr('disabled');
		jQuery('#trp-language-selector-form input[type="checkbox"]').prop('checked', true);

		// Pre-select the source language that comes from the post taxonomy 'language'
		jQuery('select#trp-source-language').val( container.attr('language' ) );

		// Get the selected source language from the select
		let source_language = jQuery("select#trp-source-language option:checked").val();
		console.log('source language: ' + source_language);
		jQuery('input#trp-' + source_language).attr('disabled','disabled');
		jQuery('input#trp-' + source_language).prop('checked', false);

		// Disable the currently selected language as target language (disable checkbox) when changing the source language
		jQuery(document).on( 'change keyup', 'select#trp-source-language', function() {
			// Re-enable all the checkboxes
			jQuery('#trp-language-selector-form input[type="checkbox"]').removeAttr('disabled');
			jQuery('#trp-language-selector-form input[type="checkbox"]').prop('checked', true);

			// Disable and uncheck the checkbox that matches the chosen source language
			let source_language = jQuery("select#trp-source-language option:checked").val();
			jQuery('input#trp-' + source_language).attr('disabled','disabled');
			jQuery('input#trp-' + source_language).prop('checked', false);
		});

		jQuery( "#trp-language-selector" ).dialog({
			resizable: false,
			height: "auto",
			width: 400,
			closeText:'',
			modal: false,
			buttons: {
				"Translate": function(event) {

					event.preventDefault();

					container.attr('status', 'queued');
					container.html( trp_loader + ' working...');

					// Get source language again since it could have changed by the user in the meantime
					source_language = jQuery("select#trp-source-language option:checked").val();

					// Get the selected target languages from the checkboxes
					let selected_languages = jQuery("#trp-language-selector-form input:checkbox:checked").map( function() {
						return jQuery(this).val();
					}).get();

					// When the translate button was clicked
					// Post the AJAX request to create the job
					jQuery.ajax({
						type: "POST",
						url: ajaxurl,
						data: {
							'action': 'trp_create_job',
							'service_id': 'translate',
							'context': 'post_translation',
							'post_id': post_id,
							'parameters': {
							  'language': source_language,
							  'languages': selected_languages
							},
							'_ajax_nonce': nonce
						},
						dataType: 'json',

						// Re-Initialize the progress and try to retrieve a new job ID
						success: function(response) {

							displayTRPJobStatus(post_id, true );
							console.log(response );

						}

					});

					jQuery( this ).dialog( "close" );
				},

				Cancel: function() {
					jQuery( this ).dialog( "close" );
				}
			}
		});



	});





	// Create translations from a transcript
	jQuery(document).on( 'click', '.trp-create-transcript-translations', function() {

		let language = jQuery(this).attr('language');
		let post_id = jQuery(this).parents('.trp-menu').attr('post_id');
		let nonce = jQuery(this).parents('.trp-menu').attr('nonce');

		//let status = jQuery('#trp-service-selector-container-' + post_id); // not our status

		//let status = jQuery('#transcript-status-container-' + post_id );
		let status = jQuery('#trp-container-' + post_id ); //.parent('.trp-wrap');

		// Pre-select the source language that comes from the transcript
		jQuery('select#trp-source-language').val(language);

		jQuery( "#trp-language-selector" ).dialog({
			resizable: false,
			height: "auto",
			width: 400,
			closeText:'',
			modal: false,
			buttons: {
				"Translate": function(event) {

					event.preventDefault();

					// Update the outer container's status to 'queued', because we just did
					status.attr('status', 'queued');
					status.html( trp_loader + ' working...');

					// Get the selected source language from the select
					let source_language = jQuery("#trp-language-selector-form select#trp-source-language option:checked").val();

					// Get the selected target languages from the checkboxes
					let selected_languages = jQuery("#trp-language-selector-form input:checkbox:checked").map( function() {
						return jQuery(this).val();
					}).get();

					// When the translate button was clicked
					// Post the AJAX request to create the job
					jQuery.ajax({
						type: "POST",
						url: ajaxurl,
						data: {
							'action': 'trp_create_job',
							'service_id': 'translate',
							'context': 'transcript_translation',
							'post_id': post_id,
							'parameters': {
							  'language': source_language,
							  'languages': selected_languages
							},
							'_ajax_nonce': nonce
						},
						dataType: 'json',

						// Re-Initialize the progress and try to retrieve a new job ID
						success: function(response) {

							displayTRPJobStatus(post_id, true );
							console.log(response );

						},


					});

					jQuery( this ).dialog( "close" );
				},

				Cancel: function() {
					jQuery( this ).dialog( "close" );
				}
			}
		});



	});


	/**
	 * If 'transcribe' was chosen as a service, add another select field for selecting the language to transcribe for
	 * Remove it again if something else was chosen
	 */
	jQuery(document).on( 'change keyup', 'select[class*="trp-service-selector"]', function() {

		let id = jQuery(this);
		let post_id = id.attr('post_id');

		if( id.val() == 'transcribe' ) {

			let lang_sel = '<select class="trp-language">';

			jQuery.each( trp_transcription_languages, function( k, v ) {
				lang_sel += '<option value="' + k + '">' + v + '</option>';
			});

			lang_sel += '</select>';

			id.parent('div').find('.trp-language-select-container').html( lang_sel );

		} else {
			id.parent('div').find('select.trp-language').remove();
		}

	});



	// When the start-service button was clicked
	jQuery(document).on( 'click', '.trp-start-service', function() {

		let post_id = jQuery(this).attr('post_id');
		let nonce = jQuery(this).attr('nonce');

		let service_id = jQuery( '#trp-service-selector-' + post_id + ' option:checked' ).val();
		let language = '';
		let parameters = {};

		let status = jQuery('#trp-service-selector-container-' + post_id);

		status.attr('status', 'running');
		//status.find('select').attr('disabled', 'disabled');
		status.html(trp_loader + ' working...');

		if( service_id == 'transcribe' ) {
			language = jQuery(this).parent('.trp-service-selector').find( 'select.trp-language option:checked').val();

			parameters['language'] = language;
			parameters['model'] = 'medium';
		}

		console.log( 'start service: ' + service_id );

		jQuery.ajax({
			type: "POST",
			url: ajaxurl,
			data: {
				'action': 'trp_create_job',
				'service_id': service_id,
				'post_id': post_id,
				//'language': language,
				'parameters': parameters,
				'_ajax_nonce': nonce
			},
			dataType: 'json',

			// Re-Initialize the progress and try to retrieve a new job ID
			success: function(response) {
				console.log(response.message );

				status.next('.trp-last-msg').text(response.message);
				displayTRPJobStatus( post_id, true ); // Immediately reload job status

			}

		});

	});




	/**
	 * When clicking on an trp-edit-translation icon open a modal to directly edit the post translation
	 */
	jQuery(document).on( 'click', '.trp-retry-job', function() {

		let job_id = jQuery(this).attr('job_id');
		let nonce = jQuery(this).attr('nonce');

		jQuery.ajax({
			type: "POST",
			url: ajaxurl,
			data: {
				'action': 'trp_retry_job',
				'job_id': job_id,
				'_ajax_nonce' : nonce
			},
			dataType: 'json',

			// Display the translation editor in a modal
			success: function(response) {
				TRPNotify('Put job ' + job_id + ' to queue.');
			},

			error: function(response) {
				TRPNotify(response);
			}
		});

	});




	// Create translations from a transcript
	jQuery(document).on( 'click', '.trp-delete-transcript', function() {

		let post_id = jQuery(this).attr('transcript_id');
		let nonce = jQuery(this).attr('nonce');

		jQuery.ajax({
			type: "POST",
			url: ajaxurl,
			data: {
				'action': 'trp_delete_transcript',
				'job_id': job_id,
				'_ajax_nonce' : nonce
			},
			dataType: 'json',

			// Display the translation editor in a modal
			success: function(response) {
				TRPNotify('Put job ' + job_id + ' to queue.');
			},

			error: function(response) {
				TRPNotify(response);
			}
		});


	});





	/**
	 * Load the service selector menu
	 */
	jQuery(document).on( 'click', '.trp-load-service-selector', function() {

		let post_id = jQuery(this).attr('post_id');
		let nonce = jQuery(this).attr('nonce');
		let container = jQuery('#trp-selector-container-' + post_id);

		//jQuery('body').prepend('<div id="body-overlay"></div>');
		container.html( trp_loader );

		jQuery.ajax({
			type: "GET",
			url: ajaxurl,
			data: {
				'action': 'trp_get_service_selector',
				'post_id': post_id,
				'_ajax_nonce' : nonce
			},
			dataType: 'html',

			success: function(selector) {
				console.log(selector);
				container.html(selector);
			},

			error: function(response) {
				TRPNotify(response);
			}
		});

	});


	jQuery(document).on( 'mouseleave', '.trp-menu', function() {
		jQuery(this).remove();
	});



	/**
	 * Create a transcript from a post
	 */
	jQuery(document).on( 'click', '.trp-create-transcript', function() {

		let container = jQuery(this).parents('.trp-container').parent('.trp-wrap');
		container.html( trp_loader );
		container.attr('status', 'running' );

		let post_id =  jQuery(this).parents('.trp-menu').attr('post_id');
		let language = jQuery(this).attr('language');
		let nonce = jQuery(this).parents('.trp-menu').attr('nonce');

		if( language == '' ) {
			console.log( language );
			console.log( 'language was empty');
			return;
		}

		let parameters = {};
		parameters['language'] = language;
		parameters['model'] = 'medium';

		console.log( 'create transcription job' );

		jQuery.ajax({
			type: "POST",
			url: ajaxurl,
			data: {
				'action': 'trp_create_job',
				'service_id': 'transcribe',
				'context': 'transcript',
				'post_id': post_id,
				'parameters': parameters,
				'_ajax_nonce': nonce
			},
			dataType: 'json',

			// Re-Initialize the progress and try to retrieve a new job ID
			success: function(response) {

				// Display the message if there is one
				if( typeof response.message != 'undefined' ) {
					console.log(response.message );
					TRPNotify( response.message );
				}

				displayTRPJobStatus( post_id, true ); // Immediately reload job status
			}

		});

	});

});

/* TODO:  */
function displayTRPJobStatus(post_id, immediate = false) {
  console.log("Hi, it's me, displayTRPJobStatus()!");

  let interval = setInterval(reload_status, 10000);

  if (immediate) reload_status();

  /**
   * Reload the status by displaying its returned HTML element directly
   */
  function reload_status() {
    // Only reload if window is in focus
    //if( ! document.hasFocus() )
    //	return;

    let container = jQuery("#trp-container-" + post_id);
    let nonce = container.attr("nonce");
    let wrap = container.parent(".trp-wrap");
    let status = container.attr("status");

    console.log(status);

    // If nothing is running anymore, we reached a final state and don't reload the status anymore
    if (status != "running" && status != "queued" && status != "api-finished" && typeof status !== 'undefined' ) {
      clearInterval(interval);
      console.log("Cleared interval: " + interval);
      return;
    }

    jQuery.ajax({
      type: "GET",
      url: ajaxurl,
      data: {
        action: "trp_display_job_status",
        post_id: post_id,
        _ajax_nonce: nonce,
      },
      dataType: "html",
      success: function (job_status_html) {
        console.log("Reload status for post " + post_id);
        console.log(job_status_html);

        wrap.html(job_status_html);
      },
    });
  }
}

/* Displays a message to the user after receiving an AJAX response */
function TRPNotify(msg) {
  let notify = jQuery("#trp-notification");
  notify.html(msg);
  notify.show();
  notify.fadeOut(2000);
}