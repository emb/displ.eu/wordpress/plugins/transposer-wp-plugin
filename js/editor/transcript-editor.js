import { __ } from "@wordpress/i18n";
import { createRoot } from "react-dom";
import { BrowserRouter, Route, Routes } from "react-router-dom";

// Importing necessary files
import "./src/scss/index.scss";
import AuthRoute from "./src/routes/AuthRoute";
import HomePageTranscriptEditor from "./src/pages/HomePageTranscriptEditor";

const TranscriptEditor = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route element={<AuthRoute />}>
          <Route path="*" element={<HomePageTranscriptEditor />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
};

const rootElement = document.getElementById("transposer-app");
if (rootElement) {
  createRoot(rootElement).render(<TranscriptEditor />);
}
