const defaultConfig = require("@wordpress/scripts/config/webpack.config");

module.exports = {
  ...defaultConfig,
  entry: {
    ...defaultConfig.entry(),
    cutEditor: "./cut-editor.js",
    transcriptEditor: "./transcript-editor.js",
  },
};
