export const LANGUAGE = {
  en: { value: "en", name: "English" },
  de: { value: "de", name: "German" },
  sr: { value: "sr", name: "Serbian" },
  hr: { value: "hr", name: "Croatian" },
  bs: { value: "bs", name: "Bosnian" },
  cs: { value: "cs", name: "Czech" },
  sk: { value: "sk", name: "Slovakian" },
  fr: { value: "fr", name: "French" },
  hu: { value: "hu", name: "Hungarian" },
  it: { value: "it", name: "Italian" },
  pl: { value: "pl", name: "Polish" },
  es: { value: "es", name: "Spanish" },
  nl: { value: "nl", name: "Dutch" },
  pt: { value: "pt", name: "Portuguese" },
  uk: { value: "uk", name: "Ukrainian" },
  ru: { value: "ru", name: "Russian" },
  tr: { value: "tr", name: "Turkish" },
};

export const BASE_API = "admin-ajax.php";
//"https://cba.media/wp-admin/admin-ajax.php";
// "http://localhost/cba/wp-admin/admin-ajax.php";
