import { Outlet } from "react-router-dom";

const AuthRoute = () => {
  return (
    <>
      <div
        style={{
          width: "100%",
          height: "100%",
          overflow: "auto",
        }}
      >
        <Outlet />
      </div>
    </>
  );
};

export default AuthRoute;
