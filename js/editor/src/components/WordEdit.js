import * as React from "react";
import _ from "lodash";

const WordEdit = ({ word, segementIndex, wordIndex }) => {
  const [content, setContent] = React.useState(word.text);
  const handleChange = (e) => {
    setContent(e.target.value);
    let text = "";
    window.exportTranscriptContent.segments[segementIndex].words[
      wordIndex
    ].text = e.target.value;

    window.exportTranscriptContent.segments[segementIndex].text = text;
  };
  return <input onChange={handleChange} value={content} />;
};

export default WordEdit;
