import { TextField } from "@mui/material";
import * as React from "react";
import { useState, useEffect, useRef } from "react";
import { appStore } from "../store/app.store";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { formatToMinuteSecond } from "../functions/GeneralFunc";
import CreateIcon from "@mui/icons-material/Create";
import IconButton from "@mui/material/IconButton";
import PlayArrow from "@mui/icons-material/PlayArrow";
const theme = createTheme({
  palette: {
    lightPurple: {
      main: "#EADDFF",
      contrastText: "#E6E0E9",
    },
  },
});

const EditTranscript = ({ item, text, type, isTwoCol }) => {
  const {
    currentTime,
    setCurrentTranscriptSelected,
    currentTranscriptSelected,
    waveform,
    setIsEditting,
    videoRef,
  } = appStore();
  const [content, setContent] = useState(text);
  const [isCurrent, setIsCurrent] = useState();
  const [startTime, setStartTime] = useState(null);
  const [endTime, setEndTime] = useState(null);
  const autoScrollRef = useRef(null);

  useEffect(() => {
    setContent(text);
    setStartTime(formatToMinuteSecond(item.start));
    setEndTime(formatToMinuteSecond(item.end));
  }, [item]);
  React.useEffect(() => {
    setIsCurrent(false);
    if (item.start <= currentTime) {
      if (item.end > currentTime) {
        setIsCurrent(true);
      }
    }
  }, [currentTime]);
  const handleDoubleClick = (e) => {
    e.preventDefault();
    e.stopPropagation();
    if (item.id === undefined) item.id = "0";
    if (currentTranscriptSelected?.id === item.id) {
      setCurrentTranscriptSelected();
      setIsEditting(false);
    } else {
      setCurrentTranscriptSelected({ id: item.id, type });
      setIsEditting(true);
    }
    e.currentTarget.blur();
  };

  const handleChangeContent = (event) => {
    setContent(event.target.value);
  };
  const handleStopEdit = (e) => {
    e.preventDefault();
    e.stopPropagation();
    setCurrentTranscriptSelected();
  };
  const handleCorrespondingToWaveform = (e) => {
    e.preventDefault();
    e.stopPropagation();
    waveform.setTime(item.start);
    videoRef.currentTime = item.start;
  };

  useEffect(() => {
    if (isCurrent) {
      autoScrollRef.current?.scrollIntoView({
        // behavior: 'auto',
        // block: 'center',
        // inline: 'center'
        behavior: "smooth",
        block: "end",
        inline: "nearest",
      });
    }
  }, [isCurrent]);

  return (
    <ThemeProvider theme={theme}>
      <div
        className={`${type}-container`}
        style={{
          display: "flex",
          paddingLeft: 5,
          gap: 20,
        }}
        // onClick={handleCorrespondingToWaveform}
        ref={autoScrollRef}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            width: 100,
            cursor: "pointer",
            justifyContent: "center",
          }}
          // onDoubleClick={handleStopEdit}
        >
          <div
            style={{
              display: "flex",
              borderLeft: isCurrent && "4px solid #EADDFF",
              gap: 5,
              paddingLeft: 5,
              alignItems: "center",
            }}
            onClick={handleCorrespondingToWaveform}
            className="edit-transcript-timespan"
          >
            <div className="icon">
              <PlayArrow style={{ fontSize: 12 }} className="arrow-icon"/>
            </div>

            <div style={{ display: "flex", flexDirection: "column" }}>
              <span data-time={item.start}>{startTime}</span>
              <span data-time={item.end}>{endTime}</span>
            </div>
          </div>
        </div>

        {currentTranscriptSelected?.id === item.id &&
        currentTranscriptSelected?.type === type ? (
          <TextField
            className="transcript-edit"
            multiline
            maxRows={4}
            value={content}
            style={{ width: isTwoCol ? "100%" : "100vh" }}
            onChange={handleChangeContent}
            color="lightPurple"
            // focused
            sx={{
              "& .MuiInputBase-root": {
                color: "lightPurple.contrastText",
              },
            }}
            onFocus={(e) => {
              const end = e.target.value.length;
              e.target?.setSelectionRange(end, end);
            }}
            autoFocus={true}
          />
        ) : (
          <div
            // onDoubleClick={handleDoubleClick}
            style={{
              width: isTwoCol ? "100%" : "100vh",
              cursor: "pointer",
            }}
            className="transcript-edit"
          >
            {content}
          </div>
        )}
        {/* <button onClick={handleDoubleClick}>C</button> */}
        <IconButton aria-label="edit" onClick={handleDoubleClick}>
          <CreateIcon sx={{ color: "lightPurple.contrastText" }} />
        </IconButton>
        {/* <div
     
          id="testing"
          style={{ position: "relative", top: 50 }}
        /> */}
      </div>
    </ThemeProvider>
  );
};

export default EditTranscript;
