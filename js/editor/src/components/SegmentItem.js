import * as React from "react";
import _ from "lodash";
import WordItem from "./WordItem";

const SegmentItem = ({ item, segementIndex }) => {
  return (
    <>
      <div
        className="transcript-container"
        style={{
          display: "flex",
          borderLeft: "4px solid #808080",
          paddingLeft: 5,
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            width: 100,
            cursor: "pointer",
          }}
        >
          <span>{item.start}</span>
          <span>{item.end}</span>
        </div>
        <div style={{ width: "100%" }} className="transcript-edit">
          <WordItem words={item.words} segementIndex={segementIndex} />
        </div>
      </div>
    </>
  );
};

export default SegmentItem;
