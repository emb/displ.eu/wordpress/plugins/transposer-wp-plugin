import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import { appStore } from "../../store/app.store";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import { WebVTT } from "vtt.js";
import _ from "lodash";
import Checkbox from "@mui/material/Checkbox";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import { BASE_API } from "../../constant";

const label = { inputProps: { "aria-label": "Checkbox demo" } };

const languageOptions = [
  { label: "English", value: "en" },
  { label: "German", value: "de" },
  { label: "Serbian", value: "sr" },
  { label: "Croatian", value: "hr" },
  { label: "Bosnian", value: "bs" },
  { label: "Czech", value: "cz" },
  { label: "Slovakian", value: "sk" },
  { label: "French", value: "fr" },
  { label: "Hungarian", value: "hu" },
  { label: "Italian", value: "it" },
  { label: "Polish", value: "pl" },
  { label: "Spanish", value: "es" },
  { label: "Dutch", value: "nl" },
  { label: "Portuguese", value: "pt" },
  { label: "Ukrainian", value: "uk" },
  { label: "Russian", value: "ru" },
  { label: "Turkish", value: "tr" },
];

export default function ModalImportVTT() {
  const {
    setIsOpenImportModal,
    setImportFiles,
    importFiles,
    setIsLoading,
    transcriptFile,
  } = appStore();
  const [main, setMain] = React.useState(0);

  const handleClose = () => {
    setIsOpenImportModal(false);
    setImportFiles([]);
  };
  const handleChange = (file, event) => {
    file.language = event.target.value;
    setImportFiles([...importFiles]);
  };

  const handleOk = async () => {
    setIsLoading(true);
    const transcripts = [];
    for (const index in importFiles) {
      try {
        const file = importFiles[index].file;
        const { text, segments } = await parseVTT(file);
        // const transcript = processData(json, importFiles[index].language);
        const data = {
          language: importFiles[index].language,
          transcript: text,
          segments,
        };
        if (main.toString() === index) {
          data.is_parent = 1;
        }
        transcripts.push(data);
      } catch (ex) {
        alert(ex.message);
      }
    }
    const data = new URLSearchParams();
    data.append("action", "trp_editor_import_webvtt");
    // Pass post_id & nonce of the transcript
    data.append("post_id", transcriptFile.transcript.post_id);
    data.append("_ajax_nonce", transcriptFile.nonce);
    data.append("transcripts", JSON.stringify(transcripts));
    fetch(BASE_API, {
      method: "POST",
      body: data,
      headers: {
        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
      },
    });
    setIsLoading(false);
  };
  const processData = ({ text, segments }, language) => {
    const data = new URLSearchParams();
    data.append("action", "trp_editor_save_transcript");
    data.append("transcript_id", null);
    data.append("language", language);
    data.append("transcript", JSON.stringify(text));
    data.append("segments", JSON.stringify(segments));
    data.append("_ajax_nonce", null);
    data.append("license", null);
    data.append("author", null);
    return data;
  };
  const parseVTT = (file) => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.onload = function (event) {
        const vttData = event.target.result;

        const parser = new WebVTT.Parser(window, WebVTT.StringDecoder());
        const segments = [];
        let text = "";
        let count = 0;
        parser.oncue = (cue, d) => {
          segments.push({
            id: count.toString(),
            start: cue.startTime,
            end: cue.endTime,
            text: cue.text,
          });
          text = text + ` ${cue.text}`;
          count++;
        };

        parser.onflush = () => {
          resolve({ segments, text });
        };

        parser.onerror = (err) => {
          reject(err);
        };

        parser.parse(vttData);
        parser.flush();
      };
      reader.readAsText(file);
    });
  };
  const handleChangeMain = (k) => {
    setMain(k);
  };
  return (
    <React.Fragment>
      <Dialog open={true} onClose={handleClose}>
        <DialogTitle>Import WebVTT files</DialogTitle>
        <DialogContent>
          <div style={{ display: "flex", gap: 10, flexDirection: "column" }}>
            {importFiles.map((file, k) => (
              <div style={{ display: "flex", gap: 5, alignItems: "center" }}>
                {/* <input
                  type="checkbox"
                  checked={k === main}
                  onChange={handleChangeMain.bind(this, k)}
                /> */}
                <Checkbox
                  {...label}
                  checked={k === main}
                  onChange={handleChangeMain.bind(this, k)}
                />
                <div
                  style={{
                    width: 200,
                    display: "flex",
                    alignItems: "center",
                    fontSize: 18,
                  }}
                >
                  {" "}
                  <span>{file.file.name}</span>{" "}
                </div>
                {/* <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  label="Type"
                  onChange={handleChange.bind(this, file)}
                  value={file.language}
                  style={{ width: 100 }}
                >
                  <MenuItem value={"en"}>English</MenuItem>
                  <MenuItem value={"de"}>German</MenuItem>
                  <MenuItem value={"sr"}>Serbian</MenuItem>
                  <MenuItem value={"hr"}>Croatian</MenuItem>
                  <MenuItem value={"bs"}>Bosnian</MenuItem>
                  <MenuItem value={"cz"}>Czech</MenuItem>
                  <MenuItem value={"sk"}>Slovakian</MenuItem>
                  <MenuItem value={"fr"}>French</MenuItem>
                  <MenuItem value={"hu"}>Hungarian</MenuItem>
                  <MenuItem value={"it"}>Italian</MenuItem>
                  <MenuItem value={"pl"}>Polish</MenuItem>
                  <MenuItem value={"es"}>Spanish</MenuItem>
                  <MenuItem value={"nl"}>Dutch</MenuItem>
                  <MenuItem value={"pt"}>Portuguese</MenuItem>
                  <MenuItem value={"uk"}>Ukrainian</MenuItem>
                  <MenuItem value={"ru"}>Russian</MenuItem>
                  <MenuItem value={"tr"}>Turkish</MenuItem>
                </Select> */}
                <Autocomplete
                  disablePortal
                  id="combo-box-demo"
                  options={languageOptions}
                  // size="small"
                  sx={{ width: 120 }}
                  onChange={handleChange.bind(this, file)}
                  value={languageOptions.value}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      label="Language"
                      placeholder="Favorites"
                    />
                  )}
                />
              </div>
            ))}
          </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={handleOk}>OK</Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
}
