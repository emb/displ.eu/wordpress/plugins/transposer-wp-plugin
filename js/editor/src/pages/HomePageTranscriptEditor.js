import ContainerProperties from "../layout/homepage/ContainerProperties";
import WaveFormTranscriptAudio from "../layout/homepage/WaveFormTranscriptAudio";
import ActionBar from "../layout/homepage/ActionBar";
import TranscriptSection from "../layout/homepage/TranscriptSection";
import { appStore } from "../store/app.store";
import { Backdrop, CircularProgress } from "@mui/material";
import { useSearchParams } from "react-router-dom";
import _ from "lodash";
import axios from "axios";
import { useEffect } from "react";
import ModalImportVTT from "../components/modal/ModalImportVTT";
import { BASE_API } from "../constant";

const HomePage = () => {
  const {
    fileUrl,
    isLoading,
    setIsLoading,
    setTranscriptContent,
    setFileUrl,
    setTranscriptFile,
    setTranscripts,
    setTranslations,
    isPlay,
    setIsPlay,
    isEditting,
    setIsEditting,
    setCurrentTranscriptSelected,
    currentTranscriptSelected,
    combineTranscriptData,
    isOpenImportModal,
  } = appStore();
  const [search] = useSearchParams();
  const transcript_id = search.get("transcript_id");
  useEffect(() => {
    return () => {
      setIsEditting(false);
    };
  }, []);
  useEffect(() => {
    window.addEventListener("keydown", onWaveformPress);
    return () => {
      window.removeEventListener("keydown", onWaveformPress);
    };
  }, [isPlay, isEditting, currentTranscriptSelected, combineTranscriptData]);

  const onWaveformPress = (e) => {
    if (e.code === "Space") {
      if (!isEditting) {
        setIsPlay(!isPlay);
      }
    } else if (e.code === "Tab") {
      e.preventDefault();
      e.stopPropagation();
      if (currentTranscriptSelected) {
        const index = combineTranscriptData.findIndex(
          (v) => v.id === currentTranscriptSelected.id
        );
        if (index >= 0 && index < combineTranscriptData.length) {
          setCurrentTranscriptSelected({
            id: combineTranscriptData[index + 1].id,
            type: currentTranscriptSelected.type,
          });
          setIsEditting(true);
        }
      }
    }
  };
  useEffect(() => {
    if (!transcript_id) {
      alert("Missing id ");
      return;
    }
    setIsLoading(true);
    axios
      .get(
        `${BASE_API}?transcript_id=${transcript_id}&action=trp_editor_load_transcript`
      )
      .then((res) => {
        console.log(res.data);
        let index = _.findIndex(res.data.transcripts, (v) => {
          return v.id === res.data.transcript.id;
        });
        let result = res.data.transcripts.splice(index, 1);

        setTranscripts(result);

        setTranslations(res.data.transcripts);
        setFileUrl(res.data.file.url);
        // setFileUrl(
        //   "https://cba.media/wp-content/uploads/7/1/0000661117/01-4a-mag-dr-klampfer-29-04-4a-mag-dr-klampfer-29-04_preview.mp3"
        // );
        // setFileUrl(
        //   "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4"
        // );
        setTranscriptContent(res.data.transcript);
        setTranscriptFile({
          file: { id: res.data.file.id, title: res.data.file?.title },
          transcript: {
            id: res.data.transcript.id,
            post_id: res.data.transcript.post_id,
            language: res.data.transcript.language,
            type: "manual",
            segments: [],
            author: res.data.transcript.author,
            license: res.data.transcript.license,
            text: "",
          },
          nonce: res.data.nonce,
        });
      })
      .catch((ex) => {
        alert(ex.message);
        setIsLoading(false);
      });
  }, [transcript_id]);

  return (
    <div
      style={{
        display: "flex",
        width: "100%",
        height: "100%",
        backgroundColor: "#1D1B20",
      }}
    >
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={isLoading}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            gap: 5,
            alignItems: "center",
          }}
        >
          <CircularProgress color="inherit" />
          <span>{"Please wait. The file may take some time to load... "}</span>
        </div>
      </Backdrop>
      <ContainerProperties />
      <div style={{ width: "calc(100% - 500px)", height: "100%" }}>
        {fileUrl && <WaveFormTranscriptAudio />}
        <ActionBar />
        <TranscriptSection />
      </div>
      {isOpenImportModal && <ModalImportVTT />}
    </div>
  );
};

export default HomePage;
