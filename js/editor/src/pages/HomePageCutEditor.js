import CutEditorContainerProperties from "../layout/homepage/CutEditorContainerProperties";
import WaveFormCutAudio from "../layout/homepage/WaveFormCutAudio";
import CutEditorActionBar from "../layout/homepage/CutEditorActionBar";
import MusicSpeechSection from "../layout/homepage/MusicSpeechSection";
import { appStore } from "../store/app.store";
import ModalAddSpeechGrid from "../components/modal/ModalAddSpeechGrid";
import { Backdrop, CircularProgress } from "@mui/material";
import ModalFillSpeechGrid from "../components/modal/ModalFillSpeechGrid";
import { useSearchParams } from "react-router-dom";
import axios from "axios";
import _ from "lodash";
import { useEffect } from "react";
import { BASE_API } from "../constant";

const HomePage = () => {
  const {
    fileUrlCutEditor,
    listCutEditor,
    isFillNewSpeechGrid,
    isLoading,
    isAddNewSpeechGrid,
    newSpeechGrid,
    setIsLoading,
    setListCutEditor,
    setFileUrlCutEditor,
    setCutEditorFile,
    setIsPlay,
    isPlay,
    isEditting,
    setIsEditting,
  } = appStore();

  const [search] = useSearchParams();
  const post_id = search.get("post_id");
  useEffect(() => {
    return () => {
      setIsEditting(false);
    };
  }, []);
  useEffect(() => {
    window.addEventListener("keydown", onWaveformPress);
    return () => {
      window.removeEventListener("keydown", onWaveformPress);
    };
  }, [isPlay, isEditting]);

  const onWaveformPress = (e) => {
    if (e.code === "Space") {
      if (!isEditting) {
        setIsPlay(!isPlay);
      }
    }
  };
  useEffect(() => {
    if (!post_id) {
      alert("Missing ID");
      return;
    }
    setIsLoading(true);
    axios
      .get(BASE_API, {
        params: {
          post_id,
          action: "trp_editor_load_regions",
        },
      })
      .then((res) => {
        console.log(res.data);

        setCutEditorFile(res.data.file);
        setListCutEditor(res.data.regions);
        setFileUrlCutEditor(res.data.file.url);
        // setFileUrlCutEditor(
        //   "https://cba.media/wp-content/uploads/7/1/0000661117/01-4a-mag-dr-klampfer-29-04-4a-mag-dr-klampfer-29-04_preview.mp3"
        // );
        // setFileUrlCutEditor(
        //   "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4"
        // );
      })
      .catch((ex) => {
        alert(ex.message);
        setIsLoading(false);
      });
  }, [post_id]);

  return (
    <div
      style={{
        display: "flex",
        width: "100%",
        height: "100%",
        backgroundColor: "#1D1B20",
      }}
    >
      <Backdrop
        sx={{
          color: "#fff",
          zIndex: 1000,
          backgroundColor: "rgba(9, 9, 9, 0.75)",
        }}
        open={isLoading}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            gap: 5,
            alignItems: "center",
          }}
        >
          <CircularProgress color="inherit" />
          <span>{"Please wait. The file may take some time to load... "}</span>
        </div>
      </Backdrop>
      <CutEditorContainerProperties />
      <div
        style={{
          width: "calc(100% - 500px)",
          height: "100%",
          overflow: "auto",
        }}
      >
        {fileUrlCutEditor && listCutEditor && <WaveFormCutAudio />}
        <CutEditorActionBar />
        <MusicSpeechSection />
      </div>
      {isAddNewSpeechGrid && newSpeechGrid && <ModalAddSpeechGrid />}
      {isFillNewSpeechGrid && <ModalFillSpeechGrid />}
    </div>
  );
};

export default HomePage;
