import * as React from "react";
import SpeechGrid from "../../components/SpeechGrid";
import { appStore } from "../../store/app.store";
import _ from "lodash";

function MusicSpeechSection() {
  const { listCutEditor} = appStore();

  return (
    <div
      style={{
        display: "flex",
        height: "calc(100% - 415px)",
        flexDirection: "column",
        gap: 30,
        padding: 30,
        color: "white",
        overflow: "auto",
        
      }}
    >
      {listCutEditor.map((item, k) => (
        <>
          <SpeechGrid item={item} index={k} />
          {listCutEditor.length - 1 !== k && <div style={{ width: '100', height: 1, padding: '5px 10px 5px 50px' }} >
            <div style={{ width: '100', height: 1, backgroundColor: '#36343B' }} />

          </div>}
        </>

      ))}
    </div>
  );
}

export default MusicSpeechSection;
