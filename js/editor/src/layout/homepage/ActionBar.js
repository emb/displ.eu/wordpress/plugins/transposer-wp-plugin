import * as React from "react";
import { useEffect } from "react";
import Slider from "@mui/material/Slider";
import ZoomOutIcon from "@mui/icons-material/ZoomOut";
import ZoomInIcon from "@mui/icons-material/ZoomIn";
import ButtonGroup from "@mui/material/ButtonGroup";
import IconButton from "@mui/material/IconButton";
import FastRewindRoundedIcon from "@mui/icons-material/FastRewindRounded";
import FastForwardRoundedIcon from "@mui/icons-material/FastForwardRounded";
import Button from "@mui/material/Button";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { appStore } from "../../store/app.store";
import {
  KeyboardArrowDown,
  PauseRounded,
  PlayArrowRounded,
} from "@mui/icons-material";
import { saveAs } from "file-saver";
import _ from "lodash";
import { useSearchParams } from "react-router-dom";
import { formatToMinuteSecond, sleep } from "../../functions/GeneralFunc";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import axios from "axios";
import { styled } from "@mui/material/styles";
import { BASE_API } from "../../constant";
let theme = createTheme({});
theme = createTheme(theme, {
  palette: {
    violet: theme.palette.augmentColor({
      color: {
        main: "#D0BCFF",
        contrastText: "#381E72",
      },
      name: "violet",
    }),
  },
});
const VisuallyHiddenInput = styled("input")({
  clip: "rect(0 0 0 0)",
  clipPath: "inset(50%)",
  height: 1,
  overflow: "hidden",
  position: "absolute",
  bottom: 0,
  left: 0,
  whiteSpace: "nowrap",
  width: 1,
});

const ActionBar = () => {
  const {
    currentTime,
    waveform,
    isPlay,
    setIsPlay,
    duration,
    currentTranscriptSelected,
    transcriptFile,
    setIsLoading,
    setCurrentTranscriptSelected,
    setImportFiles,
    setIsOpenImportModal,
    languageChanged,
    translationContent,
    videoRef
  } = appStore();
  const [value, setValue] = React.useState(0);
  const [formattedCurrentTime, setFormattedCurrentTime] = React.useState(null);
  const [formattedDuration, setFormattedDuration] = React.useState(null);
  const [search] = useSearchParams();
  const transcript_id = search.get("transcript_id");
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  React.useEffect(() => {
    if (waveform) {
      if (isPlay) {
        waveform.play();
        if (videoRef) {
          videoRef.play();
        }
      } else {
        waveform.pause();
        if (videoRef) {
          videoRef.pause();
        }
      }
    }
  }, [isPlay, waveform, videoRef]);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const handlePlay = () => {
    setIsPlay(!isPlay);
  };
  React.useEffect(() => {
    if (duration > 0) {
      if (waveform) {
        waveform.zoom(value);
      }
    }
  }, [value, waveform, duration]);

  const handleForwardSkip = () => {
    waveform.skip(10);
  };
  const handleBackwardSkip = () => {
    waveform.skip(-10);
  };

  useEffect(() => {
    setFormattedCurrentTime(formatToMinuteSecond(currentTime));
  }, [currentTime]);
  useEffect(() => {
    setFormattedDuration(formatToMinuteSecond(duration));
  }, [duration]);
  const exportTranscript = async (type) => {
    if (currentTranscriptSelected) {
      setCurrentTranscriptSelected();
      await sleep(1500);
    }
    if (!transcriptFile) {
      return;
    }
    let elements = document.getElementsByClassName(`${type}-container`);

    let contents = [];
    let text = "";
    _.forEach(elements, (node, k) => {
      let start = parseFloat(
        node.childNodes[0].childNodes[0].childNodes[0].dataset.time
      );
      let end = parseFloat(
        node.childNodes[0].childNodes[0].childNodes[1].dataset.time
      );
      let content = node.childNodes[1].innerText;
      contents.push({ id: k.toString(), start, end, text: content });
      text = text + ` ${content}`;
    });

    return { contents, text };
  };
  const handleExport = async () => {
    return new Promise(async (resolve) => {
      setIsLoading(true);
      try {
        if (languageChanged !== null) {
          let contents = "";
          let text = "";
          var data = new URLSearchParams();
          data.append("action", "trp_editor_save_transcript");
          if (translationContent) {
            data.append("transcript_id", parseInt(translationContent.id));
            let translationData = await exportTranscript("translated");
            contents = translationData.contents;
            text = translationData.text;
          } else {
            data.append("transcript_id", parseInt(transcript_id));
            let transcriptData = await exportTranscript("transcript");
            contents = transcriptData.contents;
            text = transcriptData.text;
          }
          data.append("transcript", JSON.stringify(text));
          data.append("segments", JSON.stringify(contents));
          data.append("_ajax_nonce", transcriptFile.nonce);
          data.append("license", transcriptFile.license);
          data.append("author", transcriptFile.author);
          console.log(contents);
          data.append("language", languageChanged);
          fetch(BASE_API, {
            method: "POST",
            body: data,
            headers: {
              "Content-type":
                "application/x-www-form-urlencoded; charset=UTF-8",
            },
          });
        }
      } catch (ex) {
        alert(ex.message);
      }
      setIsLoading(false);
      resolve();
    });
  };
  // console.log(translationContent)
  const handleWebVTT = async () => {
    await handleExport();
    setIsLoading(true);
    const res = await axios.get(
      `${BASE_API}?transcript_id=${transcript_id}&action=trp_editor_load_transcript`
    );
    window.open(res.data.file.url); //TODO
    setIsLoading(false);
  };
  const handleSaveAsText = async () => {
    let { contents, text } = await exportTranscript();

    // let clone = { ...transcriptFile };
    // clone.transcript.segments = contents;
    // clone.transcript.text = text;

    var blob = new Blob([JSON.stringify(text)], {
      type: "text/plain;charset=utf-8",
    });
    saveAs(blob, "transcript.txt");
  };
  const handleParseVTT = (e) => {
    const tmp = [];
    _.forEach(e.target.files, (file) => {
      tmp.push({ file, language: null });
    });
    console.log(tmp);
    setImportFiles(tmp);
    setIsOpenImportModal(true);
    //e.target.files
  };
  return (
    <ThemeProvider theme={theme}>
      {" "}
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          height: 80,
          backgroundColor: "#202027",
          color: "#E6E0E9",
          padding: "0px 15px",
          border: "1px solid #322F35",
        }}
      >
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            height: "100%",
          }}
        >
          <div
            style={{
              display: "flex",
              minWidth: 200,
              gap: 5,
              alignItems: "center",
            }}
          >
            <ZoomOutIcon />
            <Slider
              aria-label="Volume"
              value={value}
              onChange={handleChange}
              color="violet"
            />
            <ZoomInIcon style={{ paddingLeft: 9 }} />
          </div>
          <div style={{ display: "flex", alignItems: "center", gap: 10 }}>
            <div>
              <div>{formattedCurrentTime}</div>
            </div>
            <div>
              <ButtonGroup size="large">
                <IconButton
                  aria-label="rewind"
                  sx={{ color: "#D0BCFF" }}
                  onClick={handleBackwardSkip}
                  onMouseDown={(e) => e.preventDefault()}
                >
                  <FastRewindRoundedIcon style={{ fontSize: 35 }} />
                </IconButton>
                <IconButton
                  aria-label="skip"
                  sx={{ color: "#D0BCFF" }}
                  onClick={handlePlay}
                  onMouseDown={(e) => e.preventDefault()}
                >
                  {!isPlay ? (
                    <PlayArrowRounded style={{ fontSize: 35 }} />
                  ) : (
                    <PauseRounded style={{ fontSize: 35 }} />
                  )}
                </IconButton>
                <IconButton
                  aria-label="forward"
                  sx={{ color: "#D0BCFF" }}
                  onClick={handleForwardSkip}
                  onMouseDown={(e) => e.preventDefault()}
                >
                  <FastForwardRoundedIcon style={{ fontSize: 35 }} />
                </IconButton>
              </ButtonGroup>
            </div>
            <div>{formattedDuration}</div>
          </div>
          <div style={{ display: "flex", gap: 10 }}>
            <Button
              variant="contained"
              color="violet"
              style={{ borderRadius: 100 }}
              // onClick={handleExport}
              // onMouseDown={handleParseVTT}
              component="label"
              role={undefined}
              tabIndex={-1}
            >
              Import
              <VisuallyHiddenInput
                type="file"
                accept=".vtt"
                onChange={handleParseVTT}
                multiple="multiple"
              />
            </Button>
            <Button
              variant="contained"
              color="violet"
              style={{ borderRadius: 100 }}
              onClick={handleExport}
              onMouseDown={(e) => e.preventDefault()}
            >
              Save
            </Button>
            <div>
              <Button
                id="basic-button"
                variant="contained"
                color="violet"
                style={{ borderRadius: 100 }}
                aria-controls={open ? "basic-menu" : undefined}
                aria-haspopup="true"
                aria-expanded={open ? "true" : undefined}
                onClick={handleClick}
                endIcon={<KeyboardArrowDown />}
                onMouseDown={(e) => e.preventDefault()}
              >
                Export
              </Button>
              <Menu
                id="basic-menu"
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                MenuListProps={{
                  "aria-labelledby": "basic-button",
                }}
              >
                <MenuItem onClick={handleWebVTT}>as WebVTT</MenuItem>
                <MenuItem onClick={handleSaveAsText}>as Text</MenuItem>
              </Menu>
            </div>
          </div>
        </div>
      </div>
    </ThemeProvider>
  );
};

export default ActionBar;
