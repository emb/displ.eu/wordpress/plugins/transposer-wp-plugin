import * as React from "react";
import { useEffect, useState } from "react";
import EditTranscript from "../../components/EditTranscript";
import _ from "lodash";
import { appStore } from "../../store/app.store";
import { MenuItem, Select } from "@mui/material";
import { LANGUAGE } from "../../constant";

const TranscriptSection = () => {
  const {
    translationContent,
    transcriptContent,
    combineTranscriptData,
    setCombineTranscriptData,
    setLanguageChanged,
    languageChanged,
  } = appStore();

  useEffect(() => {
    if (transcriptContent) {
      setLanguageChanged(transcriptContent.language);
      let result = [];
      let mapping = new Map();
      for (let index = 0; index < transcriptContent.segments.length; index++) {
        const transcript = transcriptContent.segments[index];
        mapping.set(`${transcript.start}-${transcript.end}`, {
          id: transcript.id,
          start: transcript.start,
          end: transcript.end,
          transcript: transcript.text,
          translated: "",
        });
      }
      if (translationContent) {
        setLanguageChanged(translationContent.language);
        for (
          let index = 0;
          index < translationContent.segments.length;
          index++
        ) {
          const translated = translationContent.segments[index];
          let key = `${translated.start}-${translated.end}`;
          if (mapping.has(key)) {
            let value = mapping.get(key);
            value.translated = translated.text;
          }
        }
      }
      let array = Array.from(mapping, ([name, value]) => value);
      setCombineTranscriptData(array);
    }
  }, [translationContent, transcriptContent]);
  console.log(translationContent);
  const handleChangeLanguage = (e) => {
    setLanguageChanged(e.target.value);
  };
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        height: "calc(100% - 400px)",
        gap: 5,
      }}
    >
      <div
        style={{
          display: "flex",
          width: "100%",
          height: 120,
          gap: 25,
          justifyContent: "space-between",
          padding: "15px 15px",
        }}
      >
        <div
          style={{
            width: !translationContent ? "100%" : "50%"
          }}
        >
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            label="Type"
            style={{ background: "white", width: 200 }}
            size="small"
            value={!!translationContent ? null : languageChanged}
            onChange={handleChangeLanguage}
            disabled={!!translationContent}
          >
            {Object.keys(LANGUAGE).map((v, k) => (
              <MenuItem value={LANGUAGE[v].value}>{LANGUAGE[v].name}</MenuItem>
            ))}
          </Select>
        </div>
        {translationContent && (
          <div style={{ width: "50%" }}>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              label="Type"
              style={{ background: "white", width: 200 }}
              size="small"
              value={languageChanged}
              onChange={handleChangeLanguage}
            >
              {Object.keys(LANGUAGE).map((v, k) => (
                <MenuItem value={LANGUAGE[v].value}>
                  {LANGUAGE[v].name}
                </MenuItem>
              ))}
            </Select>
          </div>
        )}
      </div>

      <div
        style={{
          display: "flex",
          overflow: "auto",
          backgroundColor: "#1D1B20",
          color: "white",
          padding: 40,
          justifyContent: translationContent === null ? "center" : "",
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            gap: 25,
            maxWidth: translationContent === null ? 1000 : "unset",
            width: "100%",
          }}
        >
          {combineTranscriptData.map((item, k) => (
            <div
              style={{
                display: "flex",
                gap: 25,
                alignItems: "center",
                height: "100%",
              }}
            >
              <div
                style={{ width: translationContent === null ? "100%" : "50%" }}
              >
                <EditTranscript
                  isTwoCol={translationContent !== null}
                  item={item}
                  text={item.transcript}
                  type={"transcript"}
                />
              </div>

              {translationContent && (
                <div style={{ width: "50%" }}>
                  <EditTranscript
                    isTwoCol={translationContent !== null}
                    item={item}
                    text={item.translated}
                    type={"translated"}
                  />
                </div>
              )}
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default TranscriptSection;
