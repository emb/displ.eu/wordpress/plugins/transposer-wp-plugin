<?php

class TRP_Regions {

	private $_post_id = 0;

	public  $regions = array();


	public function __construct( $post_id ) {
		$this->_post_id = $post_id;
		$this->regions = $this->get_regions();
	}


	/**
	 * Retrieve existing regions
	 *
	 * @return    Return the array of regions from the database
	 */
	public function get_regions() {

		// TODO: Rename to regions in the db
		$regions = get_post_meta( $this->_post_id, 'regions', true );

		if( empty( $regions ) )
			return array();

		$regions = json_decode( json_encode( $regions ), true ); // Legacy: Convert nested objects to arrays by calling json_encode() and json_decode() in combination

		return $regions;

	}


	/**
	 * Save regions in post meta
	 *
	 * Tests if the regions have the mandatory keys
	 * If so, save it. If not, clear all
	 *
	 * @param    object|array    An array of regions
	 */
	public function set_regions( $regions ) {

		$regions = json_decode( json_encode( $regions ), true ); // Convert objects to arrays

		// Test, if mandatory region fields are properly set (from, to, license)
		// If not, clear them all, otherwise set the regions as they are but sorted by their begin time
		foreach( $regions as $key => $region ) {

			$regions[$key] = (array) $regions[$key];

			if( ! isset( $region['from'] ) || ! is_numeric( $region['from'] ) )
				$regions = array();

			if( ! isset( $region['to'] ) || ! is_numeric( $region['to'] ) )
				$regions = array();

			if( ! isset( $region['license'] ) || empty( $region['license'] ) )
				$regions = array();

			if( isset( $region['computed'] ) )
				unset( $regions[$key]['computed'] );

		}

		// Sort by begin time
		$this->regions = $this->sort_regions( $regions );

		// TODO: Rename to regions in the db
		update_post_meta( $this->_post_id, 'regions', $this->regions );

		// TODO: Update the posts's and the post's parent date_modified dates !!!

	}


	/**
	 * The function filters regions based on the type
	 * If $regions has a $filter_key with the $filter_value the element will be removed
	 *
	 * Examples:
	 *   filter_regions( $regions, "license", "copyright", 1); // Filters copyright parts = result is non-copyrighted regions
	 *   filter_regions( $regions, "license", "copyright", 0); // Filters all non-copyright parts = result is copyright regions
	 *
	 * @param    array    The regions array
	 * @param    string   The array key to filter by each region (type, license)
	 * @param    string   The element's value to filter by ("music"/"spokenword" etc., "by-nc" etc., "true"/"false")
	 * @param    boolean  0 to match the $filter_key to $filter_value by comparison '!=' | 1 to match the $filter_key to $filter_value by comparision '=='
	 * @return   array    The filtered regions
	 */
	private function filter_regions( $regions, $filter_key = 'type', $filter_value = 'music', $filter_condition = 1 )  {

		if( $filter_condition != 1 )
			$filter_condition = 0;

		foreach( $regions as $key => $region ) {

			if( $filter_condition == 0 ) {
				if( isset( $regions[$key][$filter_key] ) && $regions[$key][$filter_key] != $filter_value )
					unset( $regions[$key] );
			}

			if( $filter_condition == 1 ) {
				if( isset( $regions[$key][$filter_key] ) && $regions[$key][$filter_key] == $filter_value )
					unset( $regions[$key] );
			}

			$regions = array_values( $regions );

		}

		return $regions;

	}


	/**
	 * Sort regions by begin time
	 *
	 * @param     array    A regions set (unserialized meta_key 'regions') - a two-dimensional regions array (= only one threshold)
	 * @return    array    Sorted array
	 */
	private function sort_regions( $regions = array() ) {

		foreach( $regions as $key => $r ) {
			$from[$key] = $r['from'];
			$to[$key] = $r['to'];

			if( isset( $r['title'] ) )
				$title[$key] = $r['title'];

			if( isset( $r['desc'] ) )
				$desc[$key] = $r['desc'];

			if( isset( $r['author'] ) )
				$author[$key] = $r['author'];

			$license[$key] = $r['license'];
			$type[$key] = $r['type'];
		}

		if( empty( $from ) ) {
			return array();
		} else {
			array_multisort( $from, SORT_ASC, $regions );
			return $regions;
		}

	}


	/**
	 * The function cuts an audiofile (mp3 or ogg) according to the regions the user chose
	 *
	 * It removes all regions marked with "license": "copyright" by extracting all none-copyright parts and putting them back together again
	 *
	 * It combines consecutive non-copyright parts to avoid unnecessary cutting
	 *
	 * @return    boolean    True on success. False if an error occured
	 *
	 */
	function cut() {

		$concat = array();

		$post_id = $this->_post_id;

		$regions = $this->get_regions();

		// No regions? Nothing to do for us, but ok
		if( count( $regions ) < 1 )
			return true;

		$post = get_post( $post_id );

		// Get format for ffmpeg
		if( $post->post_mime_type == 'audio/mpeg' ) {
			$f = 'mp3';
		} elseif( $post->post_mime_type == 'audio/ogg' ) {
			$f = 'ogg';
		} else {
			return false;
		}

		// Absolute server path to the original media file
		$original_file_path = get_attached_file( $post_id );

		// The projected absolute server path for the cut version of the file (adds suffix '_cut' before the extension)
		$cut_file_path = pathinfo( $original_file_path )['dirname'] . '/' . pathinfo( $original_file_path )['filename'] . '_cut.' . pathinfo( $original_file_path )['extension'];

		$metadata = wp_get_attachment_metadata( $post_id );
		// The file's duration in milliseconds
		$end_of_file = isset( $metadata['length'] ) ? $metadata['length'] * 1000 : 0;

		// Sort by begin time
		$regions = $this->sort_regions( $regions );

		/**
		 * Test for completeness: Are all regions continuous from beginning to the end?
		 * If not complete, then reject the request by returning false and don't do anything. The editor's JS needs to take care of generating continous regions
		 */
		$a = 1;

		foreach( $regions as $key => $region ) {

			// Does the first region start at 0?
			if( $a == 1 ) {

			   if( $region['from'] != 0 )
					echo __( 'Regions not starting at 0', 'trp');

			// For all other regions in between
			// Does the previous region's end time match the start time of the current one?
			} else {

				if( $regions[$key-1]['to'] != $region['from'] )
					echo  __( 'Regions are not continuous', 'trp'); // false;

			}

			/**
			 * Test result: Let's not test if the last marker reaches the end exactly, since the duration information can vary, esp. with variable bitrates
			 */

			// Does the last region reach (at least) the end of the file?
			/*
			if( $a == count( $regions ) ) {

				if( $region['to'] < $end_of_file )
					echo __( "Regions don't get to the end of file. Region end time: " . $region['to'] . " - End of file: " . $end_of_file , 'trp');

			}
			*/


			$a++;
		}


		/* If the regions are continuous and complete */

		// Get rid of the copyright parts
		// What remains are the non-copyrighted parts that we want to extract
		$regions = $this->filter_regions( $regions , 'license', 'copyright' );

		$y = 1; // Index of next array element

		// Build an array with the necessary data to extract regions by ffmpeg and oggCut
		// Extract all parts which are not copyrighted and generate shell commands for executing ffmpeg and oggCut (without executing them yet)
		for( $x = 0; $x < count( $regions ); $x++ ) {

			$regions[$x]["from_date"] = TRP_Helper::milliseconds_to_date( $regions[$x]["from"] );
			$regions[$x]["to_date"] = TRP_Helper::milliseconds_to_date( $regions[$x]["to"] );
			$regions[$x]["duration"] = ( $regions[$x]["to"] - $regions[$x]["from"] ) / 1000;
			$regions[$x]["duration_ms"] = ( $regions[$x]["to"] - $regions[$x]["from"] );
			$regions[$x]["part"] = self::generate_cut_part_filepath( $cut_file_path, $x + 1 ); // Generate the filename for the part (adds numbers to the filename until it is unique)
			$regions[$x]["cmd-mp3"] = "ffmpeg -y -ss " . $regions[$x]["from_date"]." -t " . $regions[$x]["duration"] . " -i " . $original_file_path . " -acodec copy " . $regions[$x]["part"] . " > " . trailingslashit( dirname( $cut_file_path ) ) ."cut-mp3.log 2>&1";	// Generating command for extracting the part and saving it to originalfilename_cut_1.mp3 and so on
			$regions[$x]["cmd-ogg"] = "oggCut -s " . $regions[$x]["from"]." -l " . $regions[$x]["duration_ms"] . " " . $original_file_path . " " . $regions[$x]["part"] . " > " . trailingslashit( dirname( $cut_file_path ) ) ."cut-ogg.log 2>&1";	// Generating command for extracting the part and saving it to originalfilename_cut_1.mp3 and so on

			/* Combine consecutive voice parts */

			// Is it not the last region?
			if( isset( $regions[$y] ) ) {

				//echo "<br>x: ".$x.", y: ".$y." : ".$regions[$x]["to"]." - ".$regions[$y]["from"]." ";

				// If the next region has the same beginning time as our current end
				if( $regions[$x]["to"] == $regions[$y]["from"] ) {

					$regions[$x]["from_date"] = TRP_Helper::milliseconds_to_date( $regions[$x]["from"] );
					$regions[$x]["to_date"] = TRP_Helper::milliseconds_to_date( $regions[$y]["to"] ); // Set the current end region to the end of the next one
					$regions[$x]["to"] = $regions[$y]["to"]; // Set the current end region to the end of the next one
					$regions[$x]["duration"] = $regions[$x]["duration"] + $regions[$y]["duration"]; // Sum up the duration
					$regions[$x]["duration_ms"] = $regions[$x]["duration"] * 1000;
					$regions[$x]["part"] = self::generate_cut_part_filepath( $cut_file_path, $x + 1 ); // Generate the filename for the part (adds numbers to the filename until it is unique)
					$regions[$x]["cmd-mp3"] = "ffmpeg -y -ss " . $regions[$x]["from_date"] . " -t " . $regions[$x]["duration"] . " -i " . $original_file_path . " -acodec copy " . $regions[$x]["part"] . " > " . trailingslashit( dirname( $cut_file_path ) ) ."cut-mp3.log 2>&1";	// Generating command for extracting the part and saving it to originalfilename_cut_1.mp3 and so on
					$regions[$x]["cmd-ogg"] = "oggCut -s " . $regions[$x]["from"] . " -l " . $regions[$x]["duration_ms"] . " " . $original_file_path . " " . $regions[$x]["part"] . " > " . trailingslashit( dirname( $cut_file_path ) ) ."cut-ogg.log 2>&1";	// Generating command for extracting the part and saving it to originalfilename_cut_1.mp3 and so on

					unset( $regions[$y] ); // Unset the next region
					$regions = array_values( $regions ); // And set continuous element keys

					// The array shrinked by one
					$x--;
					$y--;

				} else {
					// If there are no consecutives, just add a single cut region
					$regions[$x]["from_date"] = TRP_Helper::milliseconds_to_date( $regions[$x]["from"] );
					$regions[$x]["to_date"] = TRP_Helper::milliseconds_to_date( $regions[$x]["to"] );
					$regions[$x]["duration"] = ( $regions[$x]["to"] - $regions[$x]["from"] ) / 1000;
					$regions[$x]["duration_ms"] = ( $regions[$x]["to"] - $regions[$x]["from"] );
					$regions[$x]["part"] = self::generate_cut_part_filepath( $cut_file_path, $x + 1 ); // Generate the filename for the part (adds numbers to the filename until it is unique)
					$regions[$x]["cmd-mp3"] = "ffmpeg -y -ss " . $regions[$x]["from_date"]." -t " . $regions[$x]["duration"] . " -i " . $original_file_path . " -acodec copy " . $regions[$x]["part"] . " > " . trailingslashit( dirname( $cut_file_path ) ) . "cut-mp3.log 2>&1";	// Generating command for extracting the part and saving it to originalfilename_cut_1.mp3 and so on
					$regions[$x]["cmd-ogg"] = "oggCut -s " . $regions[$x]["from"]." -l " . $regions[$x]["duration_ms"] . " " . $original_file_path . " " . $regions[$x]["part"] . " > " . trailingslashit( dirname( $cut_file_path ) ) ."cut-ogg.log 2>&1";	// Generating command for extracting the part and saving it to originalfilename_cut_1.mp3 and so on
				}

			}

			$y++;
			$n++;

			if( $x == count( $regions ) ) // The count of the array changes because elements were unset. stop when end reached.
				break;

		} // for


		// Execute the commands: do the actual extraction
		foreach( $regions as $region ) {

			// Extract each part to a new file
			if( $f == 'mp3' ) {
				system( $region["cmd-mp3"], $retval );
			} elseif( $f == 'ogg' ) {
				system( $region["cmd-ogg"], $retval );
			}

			if( $retval == 0 ) {
				$concat[] = $region["part"]; // On success collect files for concatenation
			} else {
				@unlink( $region["part"] ); // Otherwise delete the part and break -> TODO: if it's not the first one it will not get deleted, so delete everything!
				return false;
				break;
			}

		}

		// Prevent the original audio from being overwritten
		// -> set $cfg["cut_suffix"] !
		if( $cut_file_path == $original_file_path ) {
			return false;
		}



		// Put the parts together again

		// If there are parts to concatenate
		if( count( $concat ) > 0 ) {

			// Delete the old cut version
			if( file_exists( $cut_file_path ) )
				@unlink( $cut_file_path );

			// Concatenate all parts to the cut version
			if( count( $concat ) > 0 ) {

				if ( $f == 'mp3' ) {
					$concat_cmd = "cat " . implode( " ", $concat ) . " | ffmpeg -y -f " . $f . " -i - -acodec copy " . $cut_file_path . " > " . trailingslashit( dirname( $cut_file_path ) ) . "cut-mp3.log 2>&1";
					system( $concat_cmd, $retval );
				} elseif( $f == 'ogg' ) {
					$concat_cmd = "oggCat " . $cut_file_path . " ". implode( " ", $concat ) . " > " . trailingslashit( dirname( $cut_file_path ) ) . "cut-ogg.log 2>&1";
					system( $concat_cmd, $retval );
				}

			} else {

				/* Two possibilities: either the file was wholely marked as non-copyright or there was just one extraction
					 1st case: copy the original to cut version */

				// If there was only one region all in all it means we have nothing to cut
				if( count( $regions ) == 1 ) {
					 if( @copy( $original_file_path, $cut_file_path ) ) // TODO: Can't just copy everything where there isn't anything to cut -> flag it otherwise?
						 $retval = 0;
					 else
						 $retval = 1;
				} elseif( count( $regions ) == 2 ) {

					/* 2nd case: rename the only existing part to cut version and copy id3 header from original to it */
					$concat = array_values( $concat );

					// If there's only one part to extract rename the extraction after the cut version
					if( @rename( $concat[0], $cut_file_path ) )
						$retval = 0;
					else
						$retval = 1;
				}

			}

			// Unlink all the parts
			foreach( $regions as $part ) {
				if( @file_exists( $part["part"] ) )
					@unlink( $part["part"] );
			}

			if( $retval == 0 ) {
				return true;
			} else {
				return false;
			}
		} else {
			return true; // If there aren't parts to concatenate it's ok too
		}

	}


	/**
	 * Returns an absolute path which is not existing yet
	 *
	 * If $startby is given, it will return a projected filename, which is not there yet
	 * This is the major difference to the wp function wp_unique_filename(),
	 * as it only tests for already existing files and will only increase the filename suffix if the file is actually there
	 *
	 * Looks up if the designated filename already exists
	 * Puts a '_1' at the end of the filename if it exists
	 * Increases the end number if file exists
	 * Calls itself recursively as long as there is a new filename generated which doesn't exist yet
	 *
	 * @param     string    The designated target absolute file path
	 * @param     int       If >1 it will emulate an existing path and returns the next path
	 * @return    string
	 */
	private static function generate_cut_part_filepath( $new_path, $startby = false ) {

	  $startnumber = ( $startby ) ? $startby : 1;

	  // If the destination_path is already existing generate another filename putting _1, _2, _3, ... to the end
	  if( file_exists( $new_path ) || $startby ) {

	      // Get dirname, basename and extension
	      $tmp_fileinfo = pathinfo( $new_path );
	      $tmp_extension = $tmp_fileinfo['extension'];
	      $tmp_basename = $tmp_fileinfo['basename'];
	      $tmp_plainname = str_replace( '.' . $tmp_extension, '', $tmp_basename );

	      // If the filename contains an underscore
	      if( str_replace( "_", "", $tmp_plainname ) != $tmp_plainname ) {

	        // Get everything after the last _ of the plain filename
	        $plainname_parts = explode( '_', $tmp_plainname );
	        $last_index = count( $plainname_parts ) - 1;
	        $plainname_suffix = $plainname_parts[$last_index];

	        if( is_numeric( $plainname_suffix ) ) {
	          // If the last part after the underscore is a number, increase it
	          $plainname_parts[$last_index] = $plainname_suffix + 1;
	          // ...and set the parts of the plainname together again
	          $tmp_plainname = implode( '_', $plainname_parts );
	        } else {
	          // Otherwise start with a _1 at the end
	          $tmp_plainname = $tmp_plainname . '_' . $startnumber;
	        }

	      } else {
	        // If there is no underscore in the filename we start with _1
	        $tmp_plainname .= '_' . $startnumber;
	      }

	      // Put together the new basename again
	      $tmp_basename = $tmp_plainname . '.' . $tmp_extension;

	      // Put together the whole target path
	      $tmp_destination = $tmp_fileinfo['dirname'] . '/' . $tmp_basename;

	      // Check again if file exists and increase the number if neccessary
	      $new_path = self::generate_cut_part_filepath( $tmp_destination );

	    }

	  return $new_path;

	}


}

?>