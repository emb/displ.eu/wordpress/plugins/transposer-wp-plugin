<?php

/**
 * Depublication log
 *
 * Is used by the ECB replication framework REPCO to remove depublished posts from the database
 * Depublications need to be considered in order to be depublished on other platforms as well
 *
 * Adds the REST endpoint transposer/v1/depublication_log
 *
 * Logs when a post of the default post types is set to
 *  - trashed
 *  - deleted
 *  - private
 *  - unlisted
 *  [or when an attachment was unlinked to a post]
 *
 *  If a post is republished, the post is deleted from the log
 *
 */
Class TRP_DepublicationLog {

	public function __construct() {

		/**
		 * Register REST endpoint transposer/v1/depublication_log
		 */
		add_action( 'rest_api_init', function () {
		    //Path to meta query route
		    register_rest_route( 'transposer/v1', '/depublication_log/', array(
					'methods' => 'GET',
					'callback' => array( $this, 'rest_depublication_log' ),
					'permission_callback' => '__return_true'
		    ) );
		});

		// Register the 'unlisted' status, otherwise the filter won't fire
		register_post_status( 'unlisted', array( 'label' => 'unlisted', 'exclude_from_search' => true, 'public' => true ) );

		// Add relevant actions for default post types
		$post_types = get_option( 'transposer_default_post_types', array( 'post' ) );

		if( is_array( $post_types ) ) {

			foreach( $post_types as $post_type ) {

				/**
				 * Check if a trashed, private or unlisted post is being republished
				 * If so, remove it from the depublication log
				 */
				add_action( 'save_post_' . $post_type, array( $this, 'log_republication' ), 99, 2 );

				/**
				 * Following are called after a post was set to a new post status
				 *
				 * Action name: {$new_status}_{$post->post_type}
				 */

				/**
				 * Deleting posts of any post type
				 * Hook will be called when a post is deleted (no matter what post type)
				 * Don't call 'after_delete_post' since it doesn't cover attachments
				 */
				add_action( 'deleted_' . $post_type, array( $this, 'log_deletion' ), 10, 2 );

				/**
				 * Trashing posts and changing status to 'private'
				 * 'post' in this case is any kind of post type so it's called on every trash action
				 */
				add_action( 'trashed_' . $post_type, array( $this, 'log_depublication' ), 10, 1 );

				/**
				 * Is called after a post was set to 'private' in cases when we can't use wp_update_post()
				 */
				add_action( 'private_' . $post_type, array( $this, 'log_depublication' ), 10, 1 );

				/**
				 * Is called after a post was set to 'unlisted'
				 * The wp hook 'unlisted_post' doesn't work for whatever reason, maybe because custom post statuses are not fully implemented yet
				 */
				add_action( 'unlisted_' . $post_type, array( $this, 'log_depublication' ), 10, 1 );

				/**
				 * When an attachment is set to private or deattached from a post
				 */
				//add_action( 'private_attachment', array( $this, 'log_depublication' ) );
				//add_action( 'unlink_attachment', array( $this, 'log_depublication' ) );

			}

		}

	}


	/**
	 * Logs a deletion of a post entity
	 * Is called after deletion. Post Object is preserved in $post
	 *
	 * @param	int 		The Post ID
	 * @param	object	WP_Post Object
	 */
	public function log_deletion( $post_id, $post ) {

		// Constrain log to allowed post types
		if( ! in_array( $post->post_type, get_option( 'transposer_default_post_types', array( 'post' ) ) ) )
			return;

		global $wpdb;

		// Write the deletion to the db
		$this->log_depublication_insert( $post->ID, $post->post_type, 'delete' );

	}


	/**
	 * Logs a depublication of a post entity
	 *
	 * @param	int	The Post ID
	 */
	public function log_depublication( $post_id ) {

		$post = get_post( $post_id );

		// Constrain log to allowed post types
		if( ! in_array( $post->post_type, get_option( 'transposer_default_post_types', array( 'post' ) ) ) )
			return;

		global $wpdb;

		// Write the deletion to the db
		$this->log_depublication_insert( $post_id, $post->post_type, $post->post_status );

	}


	/**
	 * Delete an post from the depublication log if it's republished
	 *
	 * @param	int 	Post ID to republish
	 */
	public function log_republication( $post_id, $post ) {

		if( ! in_array( $post->post_type, get_option( 'transposer_default_post_types', array( 'post' ) ) ) )
			return;

		/**
		 * Called only if:
		 *
		 * - a default post type gets published (if republished again)
		 *
		 * - a default post type is set to future (after it might have been trashed or private before)
		 *   All posts of these types won't appear in any feed anymore until the future date is reached,
		 *   still they're also not flagged as depublished (but projected to be published)
		 *
		 * - a default post type is archived
		 *   'archive' is a custom post status
		 *
		 * - an attachment is set to inherit (after it was trashed before)
		 */
		if( in_array( $post->post_status, array( 'inherit', 'publish', 'future', 'archive' ) ) ) {

			// If the post is logged as depublished, delete it from the log
			if( $this->is_logged( $post->ID ) ) {
				$this->log_depublication_delete( $post_id );
			}

		}

	}


	/**
	 * Whether a post is logged as depublished
	 * (= exists in the depublication log table)
	 *
	 *	@param	int        The Post ID
	 * @return	boolean    False on failure, true if existing
	 */
	private function is_logged( $post_id ) {
		global $wpdb;
		return ( is_null( $wpdb->get_var( $wpdb->prepare( "SELECT post_id FROM " . $wpdb->prefix . "trp_depublication_log WHERE post_id = %d", (int) $post_id ) ) ) ) ? false : true;
	}


	/**
	 * Insert the post_id, status, action and user_id into the log
	 *
	 * @param	int       The Post ID
	 * @param	string    The post type we are dealing with
	 * @param	string    The new post status which is stored as 'action'
	 */
	private function log_depublication_insert( $post_id, $post_type, $post_status ) {
		global $wpdb;
		$sql = $wpdb->prepare( "INSERT INTO " . $wpdb->prefix . "trp_depublication_log (post_id, post_type, action, user_id, timestamp) VALUES( %d, %s, %s, %d, %s )", (int) $post_id, $post_type, $post_status, (int) get_current_user_id(), date('Y-m-d H:i:s') );
		$wpdb->query( $sql );
	}


	/**
	 * Delete given Post IDs from depublication log
	 *
	 * @param	int|array	One or more post IDs
	 * @return 	void
	 */
	private function log_depublication_delete( $post_id ) {
		global $wpdb;

		if( ! is_array( $post_id ) )
			$post_id = array( $post_id );

		// Delete every previous depublication log of this post
		$wpdb->query( "DELETE FROM " . $wpdb->prefix . "trp_depublication_log WHERE post_id IN(" . implode( ',', $post_id ) . ")" );

	}


	/**
	 * Retrieve the log and make it available on a REST API endpoint
	 *
	 * Provides:
	 *  - post_id
	 *  - post_type
	 *  - unpublish action (trashed, delete, private, unlisted)
	 *  - datetime of depublication
	 *
	 * Pagination
	 *
	 * Use ?offset=&limit=
	 * Default limit is 100.
	 * Max limit is 1000.
	 *
	 */
	public function rest_depublication_log() {

		global $wpdb;

		$init_limit = 100; // Standard limit
		$max_limit = 1000; // Maximum allowed limit

		$offset = ( isset( $_GET['offset'] ) && is_numeric( $_GET['offset'] ) && $_GET['offset'] > 0 ) ? $_GET['offset'] : 0;
		$limit = ( isset( $_GET['limit'] ) && is_numeric( $_GET['limit'] ) && $_GET['limit'] > 0 ) ? $_GET['limit'] : $init_limit;

		// Limit the limit number to $maxlimit
		if( $limit > $max_limit )
			$limit = $init_limit;

		$total = count( $wpdb->get_col( "SELECT post_id FROM " . $wpdb->prefix . "trp_depublication_log") );
		$results = $wpdb->get_results( $wpdb->prepare( "SELECT post_id, post_type, action, timestamp FROM " . $wpdb->prefix . "trp_depublication_log ORDER BY `timestamp` DESC LIMIT %d, %d", (int) $offset, (int) $limit ) );

		$pages = ceil( $total / $limit );

	   // Add headers for pagination
	   $response = new WP_REST_Response( $results, 200 );
	   $response->header( 'X-WP-Total', $total ); // Total number of found posts
	   $response->header( 'X-WP-TotalPages', $pages ); // Maximum number of pages

	   return $response;

	}

}


/**
 * Initialize the Depublication Log
 */
$TRP_DepublicationLog = new TRP_DepublicationLog();

?>