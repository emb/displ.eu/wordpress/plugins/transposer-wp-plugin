<?php

/**
 * Interacting with the Krytyka Polityczna's database tables when retrieving originals and translations
 */
class TRP_Krytyka {

	public function __construct() {
	}


	/**
	 * Returns whether this is KrytykaPolityczna.pl
	 * The constant has to be set in wp-config.php or the plugin for Krytyka has to be installed to set this variable
	 *
	 * @return    boolean
	 */
	public static function is_krytyka() {
		return defined( 'IS_KRYTYKA' ) && IS_KRYTYKA;
	}

}


/**
 * Hack for Krytyka Polityczna
 * They use a plugin which saves the post_excerpt in postmeta 'post_lead_text'
 * If the postmeta is called we pass it on to trp_replace_ost_field() where it will only be replaced in the correct context
 *
 * @param    string    Meta value
 * @param    int       The Post ID
 * @param    string    Name of the meta key
 * @param    boolean   Whether the post meta is single or not
 * @param    string    The meta value or the replaced post_excerpt translation
 */
function trp_replace_krytyka_lead_text( $metadata, $object_id, $meta_key, $single ){

	if ( isset( $meta_key ) && $meta_key == 'post_lead_text' ) {
		return trp_replace_post_field( $metadata, 'post_excerpt', $object_id );
	}

	// Return original if the check does not pass
	return $metadata;

}
add_filter( 'get_post_metadata', 'trp_replace_krytyka_lead_text', 99, 4 );

?>