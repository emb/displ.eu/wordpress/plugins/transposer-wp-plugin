<?php
/*
Plugin Name: Transposer
Plugin URI: https://displayeurope.eu
Description: Get access to Transposer's language & post-processing tools. Includes editors for editing subtitles, chapters & audio cutting.
Version: 1.47
Author: Michael Goldbeck, Ingo Leindecker, Ngoc Tram Luu
Author URI: https://displayeurope.eu
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

error_reporting( E_ERROR | E_PARSE ); // Try to surpress notices and warnings

// Set our version
if( ! defined( 'TRP_VERSION' ) )
	define( 'TRP_VERSION', '1.47' );

define( 'TRP_ABSPATH', __DIR__ );

// URL to the Transposer API
define( 'TRP_API_URL', 'https://transposer.displayeurope.eu/' );

// URL to endpoint of available services
define( 'TRP_API_SERVICES_ENDPOINT', TRP_API_URL . 'service/' );

// Helper functions
require( TRP_ABSPATH . '/transposer-helper.php' );

// Admin backend and AJAX functions
require( TRP_ABSPATH . '/transposer-admin.php' );

// WebVTT functions
require( TRP_ABSPATH . '/transposer-webvtt.php' );

// REST API extensions
require( TRP_ABSPATH . '/transposer-rest.php' );

// Marker functions
require( TRP_ABSPATH . '/transposer-regions.php' );

// WPML compatibility functions
require( TRP_ABSPATH . '/transposer-wpml.php' );

// Eurozine compatibility functions
require( TRP_ABSPATH . '/transposer-eurozine.php' );

// Krytyka Polityczna compatibility functions
require( TRP_ABSPATH . '/transposer-krytyka.php' );

// Frontend functions
function trp_include_frontend_functions() {
	require( TRP_ABSPATH . '/transposer-frontend.php' );
}
add_action( 'wp_loaded', 'trp_include_frontend_functions' );

// Load Depublication Log as early as possible to make sure all the filters work
function trp_depublication_log() {
	require( TRP_ABSPATH . '/transposer-depublication-log.php' );
}
add_action( 'init', 'trp_depublication_log' );

// Add db tables and language taxonomy and terms on plugin activation
register_activation_hook( __FILE__, array( 'TRP', 'plugin_activation' ) );

// Deregister cron job on plugin deactivation
// TODO: Option to delete all the data
register_deactivation_hook( __FILE__, array( 'TRP', 'plugin_deactivation' ) );


/**
 * Add a five seconds interval to the WP cron job scheme
 */
function trp_cron_add_interval( $schedules ) {

	$schedules['trp_five_seconds'] = array(
		'interval' => 5,
		'display' => __( 'Five seconds', 'trp' )
	);

	$schedules['trp_ten_seconds'] = array(
		'interval' => 10,
		'display' => __( 'Ten seconds', 'trp' )
	);

	return $schedules;
}
add_filter( 'cron_schedules', 'trp_cron_add_interval' );


/**
 * We name these functons with trp_ because they are outside of the class and the hook ('trp_call_process_renderqueue') appears in the cron jobs
 * People should clearly be able to identify it
 */
function trp_call_process_renderqueue() {
	$TRP = new TRP;
	$TRP->process_renderqueue();
}
add_action( 'trp_cron_process_renderqueue', 'trp_call_process_renderqueue', 10 );


function trp_call_process_responses() {
	$TRP = new TRP;
	$TRP->process_responses();
}
add_action( 'trp_cron_process_responses', 'trp_call_process_responses', 10 );


/**
 * Check if cron jobs are scheduled at every init
 * If only scheduled on plugin activation, WP cron jobs schedules can get lost
 */
function trp_schedule_cron() {

	if( ! wp_next_scheduled ( 'trp_cron_process_renderqueue' ) ) {
		wp_schedule_event( time(), 'trp_ten_seconds', 'trp_cron_process_renderqueue' );
	}

	if( ! wp_next_scheduled ( 'trp_cron_process_responses' ) ) {
		wp_schedule_event( time(), 'trp_ten_seconds', 'trp_cron_process_responses' );
	}

}
add_action( 'init', 'trp_schedule_cron' );


class TRP {

	// Make sure every function uses the same cURL handle and follows a continous cURL session
	private $crl = false;

	public $endpoint = '';

	private $crl_authorization_header = 'Authorization: Bearer xyz';

	public $response = '';

	public $languages;


	public function __construct() {
		$this->languages = TRP_Helper::get_all_languages();

		// Add styles to the frontend
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ) );

	}


	/**
	 * Include styles for frontend
	 */
	public function enqueue_styles() {
		wp_register_style( 'trp-frontend', plugins_url( '/css/frontend-style.css', __FILE__ ), array(), TRP_VERSION );
		wp_enqueue_style( 'trp-frontend' );
	}


	/**
	 * Returns a list of available Transposer services
	 * Tries if the option 'transposer_available_services' exists, otherwise asks the API and updates the option
	 *
	 * If $_GET['update_services'] is given, it will call the Transposer API and updates the services list in option 'transposer_available_services'
	 *
	 */
	public function get_services() {

		$services = get_option( 'transposer_available_services', array() );
		usort( $services, array( $this, 'compare_services_names' ) );

		// Call the API and update the option
		if( ! is_array( $services) || empty( $services ) || isset( $_GET['update_services'] ) ) {

			$this->init_request( TRP_API_SERVICES_ENDPOINT	);

			// Get the available services from the API and make sure we get an array of objects to sort
			// TODO: Error handling if wrong response or not available
			$services = (array) json_decode( curl_exec( $this->crl ) );

			// Sort services alphabetically by name
			usort( $services, array( $this, 'compare_services_names' ) );

			update_option( 'transposer_available_services', $services );

		}

	   return $services;

	}


	/**
	 * Creates a new 'queued' Transposer API job in the db
	 * Puts together the input object and saves it
	 * process_renderqueue() will then send the request to the API
	 *
	 * TODO: Prevent that this can be called if there are already post or transcript translations (done for transcript translations)
	 * TODO: Check if a WPML/Eurozine translation already exists for this post and prevent it from being created ??
	 *
	 * {
	 *   'post_id' => 1,
	 *   'service_id' => 'translate',
	 *   'context' => 'post_translation'|'transcript_translation'
	 *   'input'=> 'My text to translate.',
	 *   'parameters' => Array( // Optional. Dependent on the service
	 *      'language' => 'de',
	 *      'languages' => Array( 'en', 'fr', 'it', 'pt', 'sr' ),
	 *   )
	 * }
	 *
	 * @param    array            The postdata to call the API
	 * @return   array            The job row inserted
	 */
	public function create_job( $postdata ) {

		// TODO: Permission check
		//if( ! current_user_can( 'edit_post', $postdata['post_id'] ) )
		//	return 'Permission denied';

		// Test if the necessary data are there
		if( ! isset( $postdata['post_id'] ) || ! is_numeric( $postdata['post_id'] ) )
			return 'Invalid post ID';

		if( ! isset( $postdata['service_id'] ) || empty( $postdata['service_id'] ) )
			return 'Invalid service';

		if( ! isset( $postdata['context'] ) || empty( $postdata['context'] ) )
			return 'Invalid context';

		// WPML has a post for every translation
		// If the post to translate is a translation by itself - and not the original - we make sure that we always translate the original post, not the translation
		if( TRP_WPML::is_wpml() && $postdata['context'] == 'post_translation' ) {
			$postdata['post_id'] = TRP_WPML::get_original( $_POST['post_id'] );
		}

		// Check if posts exists
		if( ! $post = get_post( $postdata['post_id'] ) )
			return 'Post not found';

		// Exception for Eurozine
		// Retrieve post_content and subtitle from the postmeta instead of the posts table
		if( TRP_Eurozine::is_eurozine() ) {
			$post->subtitle = get_post_meta( $postdata['post_id'], 'subtitle', true );
			$post->post_content = get_post_meta( $postdata['post_id'], 'article_content', true ); // Unrendered post_content
		}

		// Exception for Krytyka
		// post_excerpt is in postmeta 'post_lead_text'
		// subtitle is in 'post_excerpt' (if existing)
		if( TRP_Krytyka::is_krytyka() ) {
			$post->subtitle = $post->post_excerpt; // Use the excerpt as the subtitle
			$post->post_excerpt = get_post_meta( $postdata['post_id'], 'post_lead_text', true ); // and the lead postmeta as the excerpt (= the real lead text)
		}

		$services = $this->get_services();

		// Is the called service one of the available ones?
		$service_ids = array_column( $services, 'id' );
		if( ! in_array( $postdata['service_id'], $service_ids ) )
			return 'Service not found';

		// Set the endpoint
		foreach( $services as $service ) {
			if( $service->id == $postdata['service_id'] ) {
				$this->endpoint = TRP_API_URL . $service->endpoint;
				break;
			}
		}

		$trp_postdata_collection = array();

		// Remove source language from target languages
		if( isset( $postdata['parameters']['languages'] ) && is_array( $postdata['parameters']['languages'] ) ) {
			foreach( $postdata['parameters']['languages'] as $key => $language ) {
				if( $language == $postdata['parameters']['language'] )
					unset( $postdata['parameters']['languages'][$key] );
			}
		}

		// TODO: Only temporary until Transposer API is unified
		// Remove when ready
		switch( $postdata['service_id'] ) {

			case 'transcribe':

				$trp_postdata = new StdClass;

				// URL to file
				$attached_file = get_post_meta( $postdata['post_id'], '_wp_attached_file', true );
				$url = wp_get_upload_dir()['baseurl'] . '/' . $attached_file;

				$trp_postdata->service_id = $postdata['service_id'];

				if( isset( $postdata['parameters'] ) && ! empty( $postdata['parameters'] ) )
					$trp_postdata->request = new StdClass;

				if( isset( $postdata['parameters']['language'] ) )
					$trp_postdata->request->language = $postdata['parameters']['language'];

				// Check if the transcription already exists
				// If so, don't create the job at all
				if( $this->transcript_exists( $postdata['post_id'], $postdata['parameters']['language'] ) )
					return false;

				$trp_postdata->request->url = $url;
				$trp_postdata->context = 'transcript';
				$trp_postdata_collection[] = $trp_postdata;

			break;


			case 'translate':

				/**
				 * Post translations
				 *
				 * When translating a post, create up to 4 jobs for post_title, subtitle, post_excerpt and post_content
				 *
				 * TODO: Check if translation already exists and don't create a job if so
				 * TODO: Separate Block Editor Comments from post_content before translating, temporarily save them and put them back together afterwards
				 *
				 */
				if( $postdata['context'] == 'post_translation' ) {

					// Add job for post_title
					// TODO: Rebuild all of this later
					if( ! empty( $post->post_title ) ) {
						$trp_postdata_collection[] = $this->prepare_translation_postdata( $postdata, $post, 'post_title' );
					}

					// Add job for subtitle (currently only applies for Eurozine, saved in postmeta 'subtitle')
					if( isset( $post->subtitle ) && ! empty( $post->subtitle ) ) {
						$trp_postdata_collection[] = $this->prepare_translation_postdata( $postdata, $post, 'subtitle' );
					}

					// Add job for post excerpt
					if( ! empty( $post->post_excerpt ) ) {
						$trp_postdata_collection[] = $this->prepare_translation_postdata( $postdata, $post, 'post_excerpt' );
					}

					// Add job for post_content
					if( ! empty( $post->post_content ) ) {
						$trp_postdata_collection[] = $this->prepare_translation_postdata( $postdata, $post, 'post_content' );
					}

				} // context == 'post_translation'


				/**
				 * Transcript translations
				 */
				if( $postdata['context'] == 'transcript_translation' ) {

						$trp_postdata = new StdClass;
						$trp_postdata->service_id = $postdata['service_id'];

						if( isset( $postdata['parameters'] ) && ! empty( $postdata['parameters'] ) )
							$trp_postdata->request = new StdClass;


						if( isset( $postdata['parameters']['language'] ) )
							//$trp_postdata->request->parameters->source_language_id = $postdata['parameters']['language'];
							$trp_postdata->request->source_language_id = $postdata['parameters']['language'];

						// Exclude already existing translations from target_language_ids
						$translations = $this->get_transcripts_list( $postdata['post_id'] );
						$language_codes = array_column( (array) $translations, 'language' ); // Array of already existing language codes
						$postdata['parameters']['languages'] = array_values( array_diff( $postdata['parameters']['languages'], $language_codes ) ); // Filtered array excluding the already existing ones

						// Set the remaining languages as the target languages
						if( isset( $postdata['parameters']['languages'] ) )
							$trp_postdata->request->target_language_ids = $postdata['parameters']['languages'];
							//$trp_postdata->request->parameters->target_language_ids = $postdata['parameters']['languages'];

						// Get the post's transcript in the given language including its JSON segments
						$transcript = $this->get_transcripts( $postdata['post_id'], $postdata['parameters']['language'], false );

						// Put together the input
						$input = new StdClass;
						$input->language = $transcript[0]->language;
						$input->text = $transcript[0]->transcript;


						/**
						 * Filter segments instead of adding the whole Whisper object to the request
						 *
						 * We only keep the necessary information for translation and segmenting (particularly getting rid of the word timestamps that we'll lose anyway during translation)
						 * and thus drastically reduce the payload sent to the API
						 */
						$segments = $transcript[0]->segments;

						// The filtered segments array that we pass on later
						$filtered_segments = array();

						foreach( $segments as $skey => $segment ) {

							// Create the object for each segment with only what we need
							$fsegment = new StdClass;
							$fsegment->id = $skey;
							$fsegment->start = $segment->start;
							$fsegment->end = $segment->end;
							$fsegment->text = $segment->text;

							// Add the object to our segments array
							$filtered_segments[$skey] = $fsegment;

						}

						$input->segments = $filtered_segments;

						/* Filter segments end */


						// Save the parent transcript_id in order that process_renderqueue() knows about it when saving the response to the transcripts table
						// In the way we called it it will always deliver one
						$trp_postdata->transcript_id = $transcript[0]->id;
						$trp_postdata->context = 'transcript';
						$trp_postdata->request->input = $input;
						$trp_postdata->request->format = 'transcription'; // Tell API it's a transcript (input is the Whisper JSON object)
						$trp_postdata_collection[] = $trp_postdata;

				}

			break;

		} // switch


		/**
		 * Insert the jobs into the db
		 */
		$job_ids = array();

		foreach( $trp_postdata_collection as $data ) {

			$args['post_id'] = $postdata['post_id'];

			if( ! empty( $data->transcript_id ) )
				$args['transcript_id'] = $data->transcript_id;

			$args['service'] = $data->service_id;
			$args['context'] = $data->context;
			$args['request'] = $data->request;
			$args['status'] = 'queued';
			$args['status_message'] = 'Waiting for the request to be sent';
			$args['start'] = date( 'Y-m-d H:i:s' ); // TODO: Start should be the time the API tells us, that the real Transposer job started
			$args['date_created'] = date( 'Y-m-d H:i:s' ); // When the transposer job in our db was created

			// Try to insert job into db
			// TODO: Error object
			if( ! $job_id = $this->save_job( $args ) ) {
				$args['status'] = 'error';
				$args['message'] = "Couldn't insert job into database";
				return $args;
			}

			$job_ids[] = $job_id;

		}

		return $job_ids;


		/**
		 * TODO: Get the right input for the given post ID!
		 *   services
		        - translate: if transcription given, get transcription
		        - audio2video: get URL of attachment
		        - detectmusicspeech: get URL of attachment
		        -
		 */


	}


	/**
	 * Puts together the input object for the Transposer API
	 */
	public function prepare_translation_postdata( $postdata, $post, $field ) {

		$return = new StdClass;
		$return->service_id = $postdata['service_id'];

		if( isset( $postdata['parameters'] ) && ! empty( $postdata['parameters'] ) )
			$return->request = new StdClass;

		if( isset( $postdata['parameters']['language'] ) )
			$return->request->source_language_id = $postdata['parameters']['language'];
			//$return->request->parameters->source_language_id = $postdata['parameters']['language'];

		if( isset( $postdata['parameters']['languages'] ) )
			$return->request->target_language_ids = $postdata['parameters']['languages'];
			//$return->request->parameters->target_language_ids = $postdata['parameters']['languages'];

		$return->context = $field; // Context in the db is used to distribute the data to the right tables after fetching from the API

		// TODO: Separate Block Editor comments from post content
		$return->request->input = $post->$field;

		return $return;

	}


	/**
	 * Put a job to status 'queued' in order to be processed (again)
	 *
	 * @param   int         The job ID
	 * @return  int|false   The job ID when saved, false if error
	 */
	public function retry_job( $job_id ) {
		$job = $this->get_job( $job_id );
		$job->status = 'queued';

		return $this->save_job( $job );
	}


	/**
	 * Starts a cURL request
	 * Sets configuration, request method, SSL and authenticates
	 *
	 * @param   string   The URL to call
	 * @param   string   Request method (either 'POST' or 'GET')
	 * @return  void
	 */
	public function init_request( $endpoint, $method = 'GET', $content_type = 'application/json' ) {

		$this->crl = curl_init();

		$this->set_curl_header( $content_type );

		curl_setopt( $this->crl, CURLOPT_URL, $endpoint ); // Set target URL
		curl_setopt( $this->crl, CURLOPT_CUSTOMREQUEST, $method ); // Set request method

		curl_setopt( $this->crl, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $this->crl, CURLOPT_FOLLOWLOCATION, 1 ); // Follow redirects
		curl_setopt( $this->crl, CURLOPT_SSL_VERIFYHOST, 2 ); // Enable SSL
		curl_setopt( $this->crl, CURLOPT_SSL_VERIFYPEER, 1 ); // Enable SSL

		// User Agent is necessary, otherwise it will return 500 Internal Server Error
		// TODO: What user agent to set here?
		$user_agent = 'cba.media';
		curl_setopt( $this->crl, CURLOPT_USERAGENT, $user_agent );

	}


	/**
	 * Sets the cURL header
	 */
	public function set_curl_header( $content_type = 'text/plain' ) {

		// Authorization header
		$header = array(
			'Content-Type: ' . $content_type . '; charset=UTF-8',
			$this->crl_authorization_header
		);

		curl_setopt( $this->crl, CURLOPT_HTTPHEADER, $header );

	}


	/**
	 * DEPRECATED
	 *
	 * Update job status if an error was returned or return true
	 *
	 * @param   object   The response object returned by Transposer
	 * @return  boolean	Whether there was an error or not
	 */
	public function handle_response_error( $job_id, $response ) {

		// If failure is set, there was an error with the Transposer API
		// E.g if authentication failed or if service is not found
		if( isset( $response->failure ) ) {

			$errors = 'error: ';

			if( isset( $response->failure->errors ) ) {

				foreach( $response->failure->errors as $error ) {

					// This error message means the poll function isn't supported by this service
					// TODO: Where do we get the result then?
					if( $error->code == 'elg.service.not.found' ) {
					}
					$errors .= $error->text . "\n";
				}

			}

			$this->update_job_status( $job_id, $errors );
			return false;
		}

		// If there was an error inside the servie
		if( isset( $response->error ) && ! empty( $response->error ) ) {
			$this->update_job_status( $job_id, 'error: ' . $response->error );
			return false;
		}

		return true;

	}


	/**
	 * Update a job status
	 * Sets a stop time on error or if finished
	 *
	 * @param   int       The job
	 * @param   string    The status message (queued, inprogress, finished, error)
	 * @return  boolean
	 */
	public function update_job_status( $job_id, $status = '' ) {

		global $wpdb;

		$set_stop = '';

		// In case of error or finished set the stop time since this process won't be updated anymore
		if( stristr( $status, 'error' ) || $status == 'finished' )
			$set_stop = ', stop=NOW()';

	   return $wpdb->query( $wpdb->prepare( "UPDATE " . $wpdb->prefix ."trp_transposer SET status = %s " . $set_stop . " WHERE id = %d", $status, (int) $job_id ) );

	}


	/**
	 * Is called by the WP Cron Job every x seconds and checks all jobs that are currently queued or in progress
	 * It tries to set the status to a final state, namely 'api-finished' or 'error'
	 *
	 */
	public function process_renderqueue() {

		// Don't execute if queue was disabled
		if( get_option( 'transposer_enable_cron' ) != 1 )
			return;

		// Set the max execution time to 30 minutes to avoid server timeouts
		// TODO: Make this dependent on the service: Text translations need seconds while transcriptions of 1+ hour might need more than half an hour+
		set_time_limit( 1800 );

		global $wpdb;

		$num_max_processes = get_option( 'transposer_max_processes', 1 );

		// Get the most recent post IDs that are currently in progress ordered as they were initially submitted
		$inprogress = $wpdb->get_results( "SELECT id, start FROM " . $wpdb->prefix ."trp_transposer WHERE status='inprogress'" );

		$num_inprogress = count( $inprogress );

		$num_max_processes -= $num_inprogress; // Decrease the number of remaining processes
		$num_max_processes = $num_max_processes < 0 ? 0 : $num_max_processes; // Make sure its not lower than 0

		// Set jobs to error if they took longer than half an hour
		foreach( $inprogress as $ip ) {
			if( strtotime( $ip->start ) <= ( time() - 1800 ) ) {
				$wpdb->query( $wpdb->prepare( "UPDATE ". $wpdb->prefix ."trp_transposer SET status='error', status_message='Timed out. Never got a response by Transposer API', stop=NOW() WHERE id = %d", (int) $ip->id ) );
				$num_max_processes++; // Since we set this one to error, there is a new free slot
			}
		}

		// If the max number of processes is reached, abort
		if( $num_max_processes < 1 )
			exit( "Number of maximum processes reached. Aborting." );

		$queue_order = get_option( 'transposer_queue_order', 'ASC' );
		$queue_order = in_array( $queue_order, array( 'ASC', 'DESC' ) ) ? $queue_order : 'ASC';

		// Get the most recent post IDs that are currently queued in order to process them
		$jobs = $wpdb->get_col( $wpdb->prepare( "SELECT id FROM " . $wpdb->prefix ."trp_transposer WHERE status='queued' ORDER BY date_created " . $queue_order . " LIMIT 0, %d", (int) $num_max_processes ) );

		// Call the Transposer API
		foreach( $jobs as $job_id ) {

			$start = time();
			$error = false; // Check for errors to determine whether to keep the full response object in the db
			$jstatus = ''; // Collect possible (error) messages for the status_message

			// Send the request to the Transposer API
			// Do the actual job and save the response
			$this->get_job_status( $job_id );

			// Retrieve the response data
			$job = $this->get_job( $job_id );

			// Skip if response_data is empty
			if( empty( $job->response_data ) || ! is_object( $job->response_data ) ) {
				$job->status_message = 'Response data is empty';
				$job->status = 'error';
				$this->save_job( $job );

				continue; // Skip job
			}

			// If job failed
			// Save error messages and skip to next job
			if( ! isset( $job->response_data->success ) || $job->response_data->success != true ) {
				//$job->status_message = 'Transposer ' . $job->service . ' not successful';

				// Save error messages if they exist
				if( isset( $job->response_data->error ) && ! empty( $job->response_data->error ) )
					$jstatus .= $job->response_data->error . ' ';

				if( isset( $job->response_data->message ) && ! empty( $job->response_data->message ) )
					$jstatus .= $job->response_data->message;

				$job->status = 'error';
				$job->status_message = $jstatus;
				$this->save_job( $job );

				continue; // Skip job
			}

		} // foreach

	}


	/**
	 * Is called by the WP Cron Job every x seconds and checks all api-finished responses ready to be distributed to other tables
	 * It tries to set the status to a final state, namely 'finished' or 'error'
	 *
	 */
	public function process_responses() {

		global $wpdb;

		$start = time();

		/**
		 * If we received a response we distribute its content to the other tables
		 */

		$jobs = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix ."trp_transposer WHERE status='api-finished' ORDER BY date_created ASC" );

		// No job found, nothing to do
		if( ! $jobs )
			exit('No jobs found.');

		foreach( $jobs as $job ) {

			$error = false;
			$errors = array();

			// The Transposer's API response
			$response = unserialize( $job->response_data );

			// Test if we got a valid response object
			if( ! is_object( $response ) ) {
				$job->status = 'error';
				$job->status_message = 'Invalid response data received.';
				$this->save_job( $job->id );
			}

			// XXX: Hack for now, remove later after Transposer API is unified
			if( $job->service == 'transcribe' ) {
				$language_code = $response->data->output->language;
				$payload[$language_code] = new StdClass;
				$payload[$language_code]->payload = $response->data->output->segments;
				$payload[$language_code]->success = ( $response->data->success == 1 ) ? 1 : 0;
			} else {
				$language_code = array_keys( (array) $response->payload )[0]; // Language code is the key of the response's payload
				$payload = $response->payload;
			}

			//echo "<pre>";
			//print_r( $payload );
			//exit;

			// Save the response data to the correct database destinations
			// 'post_title', 'post_excerpt', 'post_content' are post translations and will be saved in wp_translations in one single row as one translation
			// 'transcript' will be saved in wp_transcripts with a relation to its parent
			switch( $job->context ) {

				case 'post_title':
				case 'subtitle':
				case 'post_excerpt':
				case 'post_content':

					foreach( $payload as $language_code => $tr ) {

						$args = array();

						// If the translation doesn't exist yet, create it and get its object
						if( ! $translation = $this->get_post_translation( $job->post_id, $language_code ) ) {

							$args['post_id'] = $job->post_id;
							$args['language'] = $language_code;
							$args['job_id'] = $job->id;


							if( isset( $tr->success ) && $tr->success == 1 ) {
									$args[$job->context] = $tr->text; // Context = the field to save to (post_title, subtitle, post_excerpt, post_content)
								} else {
									$args['status'] = 'Transposer could not ' . $job->service . ' ' . $job->context . ' into ' . $language_code;
									$errors[] = $args['status'];
									$error = true; // Keep the full response object later if there was an error
								}

								// Insert the new translation
								if( ! $translation_id = $this->save_translation( $args ) ) {
									$errors[] = 'Could not insert ' . print_r( $args, true ). "\n";
									$error = true;
								}

							// Now it's there and can be retrieved
							$translation = $this->get_post_translation( $job->post_id, $language_code );

						}

						// $translation has all the saved fields in the db
						$args['id'] = $translation->id;
						$args['job_id'] = $job->id;
						$args['post_id'] = $translation->post_id;
						$args['post_title'] = $translation->post_title;
						$args['subtitle'] = $translation->subtitle;
						$args['post_excerpt'] = $translation->post_excerpt;
						$args['post_content'] = $translation->post_content;
						$args['language'] = $translation->language;
						$args['type'] = $translation->type;
						$args['status'] = $translation->status;

						// Overwrite just our current field that we want to update right now
						if( isset( $tr->success ) && $tr->success == true ) {
							// Write the translation into the field
							// But prevent the field from being overwritten if it's already there
							if( empty( $args[$job->context] ) )
								$args[$job->context] = trim( $tr->text );

						} else {
							$args['status'] .= 'Transposer could not ' . $job->service . ' ' . $job->context . ' into ' . $language_code; //Add the error to the status
							$errors[] = $args['status'];
							$error = true; // Keep the full response object later if there was an error
						}

						//echo "<pre>";
						//print_r( $args );

						// Update it
						$this->save_translation( $args );

					}

				break;



				case 'transcript':

					foreach( $payload as $language_code => $tr ) {

						$args = array();
						$text = '';

						if( ! isset( $tr->success ) || $tr->success != true ) {
							$error = true; // Keep the full response object later if there was an error
							$errors[] = 'Transposer could not ' . $job->service . ' ' . $job->context . ' into ' . $language_code; // Add the error to the status
							continue;
						}

						// Put together the whole plain text sentence by sentence.
						foreach( $tr->payload as $segment ) {
							$text .= trim( $segment->text ) . ' ';
						}

						$args['post_id'] = $job->post_id;
						$args['job_id'] = $job->id;
						$args['duration'] = ( time() - $start ); // How long did the transcription process take? -> should be removed or moved to the jobs table
						$args['transcript'] = $text;
						$args['language'] = $language_code;
						$args['segments'] = (array) $tr->payload; // Payload contains the segments as an array of objects
						$args['parent_id'] = $job->transcript_id;

						//echo "<pre>";
						//print_r( $args );

						// Insert it
						if( ! $this->save_transcript( $args ) ) {
							$error = true;
							$errors[] = 'Could not insert transcript';
						}

					}

				break;


			} // switch

			// If the job didn't throw an error we get rid of the occasionally quite big response data object
			// and just leave a message
			if( $error ) {

				$job->status  = 'error';

				if( ! empty( $errors ) )
					$job->status_message = implode( "\n", $errors );

			} else {
				$job->response_data = 'Response data successfully distributed';
				$job->status = 'finished';
			}

			$this->save_job( $job );

		}

	}


	/**
	 * Save a job to the database
	 *
	 * If $args['id'] is NULL it will insert
	 * If $args['id'] is given it will update
	 *
	 * @param   array        The data to save
	 * @return	int|false    The ID that was inserted into wp_transposer table or false if failed.
	 */
	public function save_job( $args ) {

		// Typecast to Array if object
		$args = (array) $args;

		if( ! isset( $args['post_id'] ) || empty( $args['post_id'] ) )
			return false;

		$insert = true;

		$defaults = array(
			'id' => NULL,
			'post_id' => 0,
			'job_id' => '',
			'transcript_id' => 0,
			'service' => '',
			'request' => '',
			'status' => '',
			'status_message' => '',
			'response_data' => '',
			'duration' => 0,
			'start' => '',
			'stop' => '',
			'context' => '',
			'date_created' => date( 'Y-m-d H:i:s' ),
			'date_modified' => date( 'Y-m-d H:i:s' ),
			'user_id' => get_current_user_id()
		);

		// Sanitize fields for insert
		$sanitize = array( NULL, '%d', '%s', '%d', '%s', '%s', '%s', '%s', '%s', '%f', '%s', '%s', '%s', '%s', '%s', '%d' );

		$args = wp_parse_args( $args );
		$args = array_merge( $defaults, $args );

		// If an id is given it means we update
		if( is_numeric( $args['id'] ) && $args['id'] > 0 ) {
			$insert = false;

			$id = $args['id']; // Save ID for update

			// Remove the ID from query
			unset( $args['id'] ); // Remove it from the query
			unset( $sanitize[0] ); // Remove it from the sanitize array

			// Remove date_created field since we won't update that
			unset( $args['date_created'] ); // Remove it from the query
			unset( $sanitize[13] ); // Remove it from the sanitize array

		} else {
			$args['date_modified'] = '';
		}

		$sanitize = array_values( $sanitize ); // Restore continuous indices

		// Serialize object if needed
		if( is_object( $args['request'] ) || is_array( $args['request'] ) )
			$args['request'] = serialize( $args['request'] );

		// Serialize object if needed
		if( is_object( $args['response_data'] ) || is_array( $args['response_data'] ) )
			$args['response_data'] = serialize( $args['response_data'] );

		global $wpdb;

		// Insert or update
		if( $insert ) {
			$return = $wpdb->insert( $wpdb->prefix . 'trp_transposer', $args, $sanitize );
			return $wpdb->insert_id;
		} else {
			$wpdb->update( $wpdb->prefix . 'trp_transposer', $args, array( 'ID' => $id ), $sanitize, array( '%d' ) );
			return $id;
		}

	}


	/**
	 * Save the transcript to the database
	*
	 * If $args['id'] is NULL it will insert a new row
	 * If $args['id'] is given it will update the row
	 *
	 * Updates both the attachment's and parent post's modification date
	 *
	 * If the transcript is empty it will return false
	 *
	 * @param   array    The data to save
	 * @return	boolean  Whether the insert/update was successful or not
	 */
	public function save_transcript( $args ) {

		// Typecast to Array if object
		$args = (array) $args;

		if( ! isset( $args['transcript'] ) || empty( $args['transcript'] ) )
			return false;

		$insert = true;

		$defaults = array(
			'id' => NULL,
			'post_id' => 0,
			'job_id' => 0,
			'media_id' => '',
			'model' => '',
			'duration' => 0,
			'transcript' => '',
			'language' => '',
			'type' => 'auto',
			'author' => '',
			'license' => '',
			'meta' => '',
			'segments' => '',
			'score' => 0,
			'parent_id' => 0,
			'date_created' => date( 'Y-m-d H:i:s' ),
			'date_modified' => date( 'Y-m-d H:i:s' ),
			'user_id' => get_current_user_id()
		);

		// Sanitize fields for insert
		$sanitize = array( NULL, '%d', '%d', '%s', '%s', '%f', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%f', '%d', '%s', '%s', '%d' );

		$args = wp_parse_args( $args );
		$args = array_merge( $defaults, $args );

		// Strip slashes in text field
		$args['transcript'] = stripslashes( $args['transcript'] );

		// If an id is given it means we update
		if( is_numeric( $args['id'] ) && $args['id'] > 0 ) {
			$insert = false;

			$id = $args['id']; // Save ID for update

			// Remove the ID from query
			unset( $args['id'] ); // Remove it from the query
			unset( $sanitize[0] ); // Remove it from the sanitize array

			// Remove date_created field since we won't update that
			unset( $args['date_created'] ); // Remove it from the query
			unset( $sanitize[15] ); // Remove it from the sanitize array

		} else {
			$args['date_modified'] = '';
		}

		$sanitize = array_values( $sanitize ); // Restore continuous indices

		// Convert object to JSON if needed
		if( is_object( $args['meta'] ) || is_array( $args['meta'] ) )
			$args['meta'] = json_encode( $args['meta'] );

		// Convert array to JSON if needed
		if( is_object( $args['segments'] ) || is_array( $args['segments'] ) )
			$args['segments'] = json_encode( $args['segments'] );

		global $wpdb;

		// Insert or update
		if( $insert ) {

			if( $wpdb->insert( $wpdb->prefix . 'trp_transcripts', $args, $sanitize ) ) {
				return $wpdb->insert_id;
			} else {

				if( isset( $_REQUEST['process_responses'] ) )
					var_dump( $wpdb->last_error );

				return false;
			}

		} else {
			$return = $wpdb->update( $wpdb->prefix . 'trp_transcripts', $args, array( 'ID' => $id ), $sanitize, array( '%d' ) );

			// Update the attachment's modification date ($args['post_id']
			self::update_modification_date( $args['post_id'] );

			// Update the post's modification date (attachment's post_parent)
			$parent_post = get_post_parent( $args['post_id'] );

			if( isset( $parent_post->ID ) && $parent_post->ID > 0 )
				self::update_modification_date( $parent_post->ID );

		}

	}


	/**
	 * Save the translation to the database table wp_translations
	*
	 * If $args['id'] is NULL it will insert a new row
	 * If $args['id'] is given it will update the row
	 *
	 * Updates both the attachment's and parent post's modification date
	 *
	 * @param   array        The data to save
	 * @return	int|boolean  After successfully inserting, it will return the translation ID or false if failed. On updating it returns boolean.
	 */
	public function save_translation( $args ) {

		// Typecast to Array if object
		$args = (array) $args;

		$insert = true;

		$defaults = array(
			'id' => NULL,
			'job_id' => 0,
			'post_id' => 0,
			'post_title' => '',
			'subtitle' => '',
			'post_excerpt' => '',
			'post_content' => '',
			'language' => '',
			'date_created' => date( 'Y-m-d H:i:s' ),
			'date_modified' => date( 'Y-m-d H:i:s' ),
			'type' => 'auto',
			'status' => '',
			'user_id' => get_current_user_id()
		);

		// Sanitize fields for insert
		$sanitize = array( NULL, '%d', '%d', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%d' );

		$args = wp_parse_args( $args );
		$args = array_merge( $defaults, $args );

		// Strip slashes from text fields
		$stripsl_fields = array( 'post_title', 'subtitle', 'post_excerpt', 'post_content' );
		foreach( $stripsl_fields as $field ) {
			$args[$field] = stripslashes( $args[$field] );
		}

		// If an id is given it means we update
		if( is_numeric( $args['id'] ) && $args['id'] > 0 ) {
			$insert = false;

			$id = $args['id']; // Save ID for update

			// Remove the ID from query
			unset( $args['id'] ); // Remove it from the query
			unset( $sanitize[0] ); // Remove it from the sanitize array

			// Remove date_created field since we won't update that
			unset( $args['date_created'] ); // Remove it from the query
			unset( $sanitize[8] ); // Remove it from the sanitize array

		} else {
			$args['date_modified'] = '';
		}

		$sanitize = array_values( $sanitize ); // Restore continuous indices

		global $wpdb;

		$now = date( 'Y-m-d H:i:s' );

		// Insert or update
		if( $insert ) {

			if( $wpdb->insert( $wpdb->prefix . 'trp_translations', $args, $sanitize ) ) {
				$insert_id = $wpdb->insert_id;

				// Update the post's modification date
				self::update_modification_date( $args['post_id'] );

				return $insert_id;

			}

			return false;

		} else {
			$wpdb->update( $wpdb->prefix . 'trp_translations', $args, array( 'ID' => $id ), $sanitize, array( '%d' ) );

			self::update_modification_date( $args['post_id'] );

			if( get_post_type( $args['post_id'] ) == 'attachment' ) {

				// Update the parent_post's modification date
				$parent_post = get_post_parent( $args['post_id'] );

				if( isset( $parent_post->ID ) && $parent_post->ID > 0 )
					self::update_modification_date( $parent_post->ID );

			}

		}
	}


	/**
	 * Returns a job's response data, generated by the Transposer API
	 *
	 * TODO: Should be temporary until it is saved
	 *
	 * @param    int       The Job ID
	 * @return   object    The response object returned by Transposer API
	 *
	 */
	public function get_response( $job_id ) {

		if( ! is_numeric( $job_id ) )
			return new StdClass;

		global $wpdb;

		return unserialize( $wpdb->get_var( $wpdb->prepare( "SELECT response_data FROM " . $wpdb->prefix . "trp_transposer WHERE id = %d", (int) $job_id ) ) );
	}


	/**
	 * Get a transcript by its ID
	 *
	 * @param    int     The transcript's ID
	 * @return   object  The transcript's db row including the subtitles in WebVTT format
	 */
	public function get_transcript( $transcript_id ) {

		//if( ! $this->user_can_edit_transcript( $transcript ) )
		//	return false;

		global $wpdb;

		$transcript = $wpdb->get_row( $wpdb->prepare( "SELECT id, post_id, model, duration, transcript, language, type, segments, parent_id, date_created, date_modified, author, license, user_id FROM " . $wpdb->prefix . "trp_transcripts WHERE id = %d", (int) $transcript_id ) );

		if( is_null( $transcript ) )
			return false;

		// Get the printable name to the language code (pass 'en', returns 'English')
		$transcript->language_string = $this->languages[$transcript->language];

		$transcript->segments = json_decode( $transcript->segments );

		$transcript->subtitles = new StdClass;
		$transcript->subtitles = TRP_WebVTT::get_webvtt_url( $transcript->post_id, $transcript->language );

		return $transcript;
	}


	/**
	 * Get a translation by its ID
	 * TODO: Permission check
	 *
	 * @param    int     The translation ID
	 * @return   array   An Array of
	 */
	public function get_translation( $translation_id ) {

		global $wpdb;

		$translation = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM " . $wpdb->prefix . "trp_translations WHERE id = %d", (int) $translation_id ) );

		// Strip slashes for the content fields to avoid cumulation of escaping
		if( $translation ) {
			$translation->post_title = stripslashes( $translation->post_title );
			$translation->post_excerpt = stripslashes( $translation->post_excerpt );
			$translation->post_content = stripslashes( $translation->post_content );
		}

		return $translation;

	}



	/**
	 * Get all transcripts of a post ID, putting the original on top
	 *
	 * @param    int      The attachment's ID (audio or video)
	 * @param    string   A two-character language code or empty for all
	 * @param    boolean  Whether to return subtitles as WebVTT URL or to return the JSON segments instead
	 * @return   object   The transcript's db rows including the subtitles in WebVTT format
	 */
	public function get_transcripts( $post_id, $language = '', $subtitles = true ) {
		global $wpdb;

		$add_where = strlen( $language ) == 2 ? "AND language='" . esc_sql( $language ) . "'" : "";

		$results = $wpdb->get_results( $wpdb->prepare( "SELECT id, post_id, model, duration, transcript, language, type, segments, parent_id, date_created, date_modified, author, license, user_id FROM " . $wpdb->prefix . "trp_transcripts WHERE post_id = %d " . $add_where . " ORDER BY parent_id ASC, date_created ASC", (int) $post_id ) );

		if( $subtitles ) {

			foreach( $results as $key => $result ) {
				$results[$key]->subtitles = new StdClass;
				$results[$key]->subtitles = TRP_WebVTT::get_webvtt_url( $result->post_id, $result->language );
				unset( $results[$key]->segments ); // Remove the segments
			}

		} else {

			foreach( $results as $key => $result ) {
				$results[$key]->segments = json_decode( $result->segments );
			}

		}

		// Get the printable name to the language code (e.g. pass 'en', returns 'English')
		foreach( $results as $key => $result ) {
			$results[$key]->language_string = $this->languages[$result->language];
		}

		return $results;

	}


	/**
	 * Get all translations of a given post to display in a preview list
	 * Select without full text or segments to make it faster
	 *
	 * @param    int     Post ID of any type
	 * @return   array   An array of the row
	 */
	public function get_transcripts_list( $post_id ) {

		global $wpdb;
		return $wpdb->get_results( $wpdb->prepare( "SELECT id, post_id, model, language, type, parent_id, date_created, date_modified, author, license, user_id FROM " . $wpdb->prefix . "trp_transcripts WHERE post_id = %d ORDER BY parent_id ASC, date_created ASC", (int) $post_id ) );

	}


	/**
	 * Get all translations of a post in a given language
	 * Select without full text or segments to make it faster
	 *
	 * @param    int     Post ID of any type
	 * @return   array   An array of the row
	 */
	public function get_post_transcript( $post_id, $language = '' ) {
		global $wpdb;
		return $wpdb->get_row( $wpdb->prepare( "SELECT id, model, language, type, author, license FROM " . $wpdb->prefix . "trp_transcripts WHERE post_id = %d AND language = %s", (int) $post_id, $language ) );
	}


	/**
	 * Get all translations of a given post
	 *
	 * @param    int     Post ID of any type
	 * @return   array   An array of objects
	 */
	public function get_translations( $post_id ) {
		global $wpdb;
		return $wpdb->get_results( $wpdb->prepare( "SELECT * FROM " . $wpdb->prefix . "trp_translations WHERE post_id = %d ORDER BY language ASC", (int) $post_id ) );
	}


	/**
	 * Returns the post translation of a given post and language
	 */
	public function get_post_translation( $post_id, $language = 'de' ) {
		global $wpdb;
		return $wpdb->get_row( $wpdb->prepare( "SELECT * FROM " . $wpdb->prefix . "trp_translations WHERE post_id = %d AND language = %s ORDER BY date_created DESC LIMIT 0,1", (int) $post_id, $language ) );
	}


	/**
	 * Returns the HTML for generating new translations of a post
	 *
	 * @param     int      The Post ID to create a translation for
	 * @return    string   The HTML markup
	 */
	public function get_generate_translations_button( $post_id ) {
		$return = '<a class="button trp-create-translations" title="' . esc_attr__( 'Create auto-translations', 'trp' ) . '" post_id="' . esc_attr( $post_id ) . '" service_id="translate" nonce="' . wp_create_nonce( 'trp_create_job' ) . '">+ <span class="dashicons dashicons-translation"></span></a>';
		return $return;
	}


	/**
	 * DEPRECATED
	 *
	 * Returns the HTML for selecting and starting a Transposer service for a post or media file
	 *
	 * If post is of type 'post': Display the buttons for generating post translations
	 *
	 * If post is of type 'attachment' and is an audio or video: Display the service selector for media files
	 * - Will only show supported services for the mime type
	 *
	 * @param    int       The Post ID
	 * @return   string    The HTML markup
	 */
	public function get_service_selector( $post_id ) {

		$post = get_post( $post_id );

		/*
		 * Service selector for post translations
		 */

		// If it's a post, display the translate posts button
		// TODO: Define post types in option
		if( in_array( $post->post_type, array( 'post', 'series', 'station', 'channel' ) ) ) {
			$return = '<div class="trp-service-selector" status="finished">';
			$return .= $this->get_generate_translations_button( $post_id ); // TODO: Add this later
			$return .= $this->get_translations_icons( $post_id );

			$return .= '</div>';
			return $return;
		}


		/**
		 * Service selector for media attachments/files
		 */

		// If the post is otherwise not an attachment, we won't display a selector
		if( $post->post_type != 'attachment' )
			return;

		// Exclude non audio-visual media (images, documents, etc.)
		$mime_type = $post->post_mime_type;
		if( ! stristr( $mime_type, 'audio' ) && ! stristr( $mime_type, 'video' ) )
			return '';

		echo $this->get_transcripts_icons( $post_id );

		$transcript = $this->get_original_transcript( $post_id );
		$language = $transcript->language;

		// TODO: Remove this later
		return '<span class="button trp-create-transcript-translations" title="' . esc_attr__( 'Auto-translate transcripts', 'cba' ) . '" post_id="' . $post_id .'" nonce="' . wp_create_nonce( 'trp_create_job' ) . '" language="' . esc_attr( $language ) . '"><span class="dashicons dashicons-translation"></span><span class="dashicons dashicons-editor-ul"></span></div>';


		// TODO: Add this later
		$services = $this->get_services();

		// Drop Down Menu and start button
		$return = '<div class="trp-service-selector" status="finished">';
		$return .= '<select class="trp-service-selector" id="trp-service-selector-' . esc_attr( $post_id ) . '" post_id="' . esc_attr( $post_id ) . '">';

		foreach( $services as $service ) {

			// If the service supports the mime type of the attachment, display it as an option
			if( in_array( $mime_type, $service->formats ) )
				$return .= '<option value="' . esc_attr( $service->id ) . '">' . esc_attr( $service->name ) . '</option>';

		}

		$return .= '</select>';

		$return .= '<span class="trp-language-select-container"></span>';

		$return .= '<a class="button trp-start-service" title="' . esc_attr__( 'Start a service', 'trp' ) . '"  post_id="' . esc_attr( $post_id ) . '" nonce="' . wp_create_nonce( 'trp_create_job' ) . '"><span class="dashicons dashicons-controls-play"></span></a>';
		$return .= '</div>';

		return $return;

	}


	/**
	 * Displays existing transcripts by displaying corresponding icons
	 *
	 * @param   int     A post ID of an attachment
	 * @return  string  HTML markup of icons representing the transcripts for each language
	 */
	public function get_transcripts_icons( $post_id ) {

		$transcripts = $this->get_transcripts_list( $post_id );

		$icons = array();

		foreach( $transcripts as $transcript ) {
			$orig_class = $transcript->parent_id == 0 ? 'original' : ''; // Highlight the original

			//$icons[] = '<span><a class="trp-btn trp-edit-webvtt ' . esc_attr( $transcript->language ) .'" title="' . esc_attr( TRP_Helper::get_all_languages()[$transcript->language] ) . '" transcript_id="'. $transcript->id . '" post_id="' . $transcript->post_id . '" language="' . esc_attr( $transcript->language ) . '" nonce="' . wp_create_nonce( 'trp_get_webvtt_from_post' ) . '">' . esc_html( $transcript->language ) . '</a></span>';
			$icons[] = '<span><a class="trp-btn trp-edit-transcript ' . esc_attr( $transcript->language ) .' ' . $orig_class . '" title="' . esc_attr( TRP_Helper::get_all_languages()[$transcript->language] ) . '" transcript_id="'. $transcript->id . '" post_id="' . $transcript->post_id . '" language="' . esc_attr( $transcript->language ) . '" nonce="' . wp_create_nonce( 'trp_edit_transcript' ) . '">' . esc_html( $transcript->language ) . '</a></span>';
		}

		return implode( '', $icons );

	}


	/**
	 * Returns the HTML markup for opening the region editor
	 *
	 * @param    int    Post ID of type 'attachment'
	 */
	public function get_region_editor_icon( $post_id ) {
		return '<span class="trp-btn trp-edit-regions" title="' . esc_attr__( 'Edit chapters and cut editor', 'trp' ) . '" post_id="'. $post_id . '"  nonce="' . wp_create_nonce( 'trp_edit_regions' ) . '"><span class="dashicons dashicons-format-audio"></span></span>';
	}


	/**
	 * Returns the absolute server filepath for a transcript's JSON file
	 * The JSON file is the original one and is never changed. It can be used to restore the original transcript
	 *
	 * @param    int       Post ID of type 'attachment'
	 * @param    string    The Whisper model name (tiny|base|small|medium|large). The larger the model is the more RAM it consumes, the longer it takes and the more accurate it will be. We generally use 'medium'
	 * @return   string    The absolute server filepath, e.g. /var/www/mysite/wp-content/uploads/{model}_myoriginalfilename.json
	 */
	public function get_transcript_filename( $attachment_id, $model = 'base' ) {

		// Absolute server path to attachment
		// E.g. /var/www/wp-content/uploads/file.mp3
		$file = wp_get_upload_dir()['basedir'] . '/' . get_post_meta( $attachment_id, '_wp_attached_file', true );

		$pathinfo = pathinfo( $file );
		$basename = $pathinfo['basename']; // Base filename without directory ('file.mp3')
		$filename = $pathinfo['filename']; // Base filename without directory and extension ('file')
		$dirname = $pathinfo['dirname']; // Directory path ('/var/www/wp-content/uploads/xyz')

		// The generated file will follow the convention: {model}_{filename}.json
		return $dirname . '/' . $model . '_' . $filename . '.json';

	}


	/**
	 *  Returns the attachment's original transcript
	 */
	public function get_original_transcript( $attachment_id ) {
		global $wpdb;
		return $wpdb->get_row( $wpdb->prepare( "SELECT * FROM " . $wpdb->prefix . "trp_transcripts WHERE post_id = %d AND parent_id = 0", (int) $attachment_id ) );
	}


	/**
	 * Returns existing jobs of a post
	 * Constrain to a service if given
	 *
	 * @param    int       The Post ID
	 * @param    string    The service's ID, e.g. 'transcribe', 'translate', ...
	 * @return   object    The db object returned by $wpdb
	 */
	public function get_jobs_by_post( $post_id, $service_id = '' ) {

		global $wpdb;

		$sql = $wpdb->prepare( "SELECT id, post_id, context, service, request, status, status_message, duration, date_created, date_modified FROM " . $wpdb->prefix . "trp_transposer WHERE post_id = %d", (int) $post_id );

		if( ! empty( $service_id ) )
			$sql .= $wpdb->prepare( " AND service = %s", $service_id );

		$sql .= " ORDER BY date_created";

		return $wpdb->get_results( $sql );

	}


	/**
	 * Returns the job's row in the transposer table
	 *
	 * @param   int       Job ID
	 * @return	object    The Job in the db
	 */

	public function get_job( $job_id ) {
		global $wpdb;

		$job = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM " . $wpdb->prefix . "trp_transposer WHERE id= %d", (int) $job_id ) );

		// Unserialize request and response data
		$job->request = ! empty( $job->request ) ? unserialize( $job->request ) : $job->request;
		$job->response_data = ! empty( $job->response_data ) ? unserialize( $job->response_data ) : $job->response_data;

		return $job;

	}


	/**
	 * Checks the status of an translation job
	 *
	 * @param   int                 Job ID, returned by generate_translation_from_transcript_id()
	 * @return	array|false|float   The status returned by Transposer or the HTTP code
	 */
	public function get_job_status( $job_id) {

		if( ! $job_id ) {
			echo "No job ID given.";
			return false;
		}

		global $wpdb;

		$job = $this->get_job( $job_id );

		// If finished or error the job runtime is over: just return the status
		// If inprogress: ask the API later as soon as there's a response
		if( ! in_array( $job->status, array( 'queued' ) ) ) //, 'inprogress' ) ) )
			return $job->status;

		$services = $this->get_services();

		// Is the called service one of the available ones?
		$service_ids = array_column( $services, 'id' );
		if( ! in_array( $job->service, $service_ids ) ) {
			$args['status'] = 'error';
			$status_message = __( 'Service not found', 'trp' );
		}

		// Set the endpoint
		foreach( $services as $service ) {
			if( $service->id == $job->service ) {
				$this->endpoint = TRP_API_URL . $service->endpoint;
				break;
			}
		}

		/**
		 * Send API Request if job status is 'queued'
		 */

		// Update the job with the result
		$job->start = date( 'Y-m-d H:i:s' );
		$job->status = 'inprogress';
		$job->status_message = __( 'Transposer is processing...', 'trp' );

		$this->save_job( $job );

		// Start the job by posting an API request to Transposer
		$this->init_request( $this->endpoint, 'POST', 'application/json' );
		curl_setopt( $this->crl, CURLOPT_POSTFIELDS, json_encode( $job->request ) );

		$curl_response = curl_exec( $this->crl ); // TODO: Deal with cURL error?
		$response = json_decode( $curl_response );

		/**
		 * Handle response
		 */

		// Update the job with the result
		$job->duration = round( ( time() - strtotime( $job->start ) ), 2 ); // Duration in seconds
		$job->stop = date( 'Y-m-d H:i:s' ); // TODO: Stop time should be the time the API tells us
		$job->response_data = $response;

		// Update according to the final response status
		// TODO: Define and handle error object
		if( isset( $response->error ) ) {
			$job->status = 'error';
			$job->status_message = $response->error;
		} elseif( isset( $response->message ) ) {
			$job->status = 'error';
			$job->status_message = $response->message;
		} else {
			//$job->status = 'finished';
			$job->status_message = sprintf( __( '%s finished', 'trp' ), $job->service );
			$job->status = 'api-finished';
		}

		// Update the job status according to the response and return the job ID
		return $this->save_job( $job );


/*

		// If queued or in progress, ask Transposer for the progress

		// If poll url is empty it means the original job was created directly (not async)
		// so we don't do anything and wait for the process that is currently running (generate_translation()) to return its result which triggers the update of the status
		if( empty( $job->poll_url ) ) {
			//$this->update_job_status( $job->id, 'error: poll url was not provided' );
			return $job->status;
		}

		$this->init_request( $job->poll_url, 'GET' );
		$response = json_decode( curl_exec( $this->crl ) );

		echo "calling " . $job->poll_url . " ...<br>";

		echo "<pre>";
		print_r( $response );
		echo "</pre>";

		// Save error messages and return false on error
		if( ! $this->handle_response_error( $job->id, $response ) )
			return false;

		// Return a progress percentage if we got one
		if( isset( $response->progress ) && isset( $response->progress->percent ) ) {

			// Set the status to 'finished' if the process reached 100%
			if( $response->progress->percent == 100 )
				$this->update_job_status( $job->id, 'finished' );
			// Otherwise return the current progress in percent
			else
				return $response->progress->percent;
		}

		return;
*/
	}





	/**
	 * Retrieves the job status from the db and displays it
	 *
	 * @param   int      The Post ID
	 */
	public function display_job_status( $post_id ) {

		// In case of WPML and if it's a post translation, we ask for the original post ID instead of the given one
		// Since we always translate from the original post, also the relation is
		if( TRP_WPML::is_wpml() ) {
			$post_id = TRP_WPML::get_original( $post_id );
		}

		// Display running jobs
		$jobs = $this->get_jobs_by_post( $post_id );

		$running_jobs = array();
		$num_queued = 0;
		$num_inprogress = 0;
		$num_api_finished = 0;

		foreach( $jobs as $job ) {

			if( $job->status == 'queued' ) {
				$running_jobs[] = $job;
				$num_queued++;
			}

			if( $job->status == 'inprogress' ) {
				$running_jobs[] = $job;
				$num_inprogress++;
			}

			if( $job->status == 'api-finished' ) {
				$running_jobs[] = $job;
				$num_api_finished++;
			}

		}

		$num_running = $num_queued + $num_inprogress + $num_api_finished;

		if( $num_running > 0 ) {

			echo '<span id="trp-container-' . $post_id . '" class="trp-container" status="running" nonce="' . wp_create_nonce('trp_display_job_status') . '">';
			echo '<span class="trp-caption">' . TRP_Helper::get_status_icon( 'inprogress' ) . ' ' . $running_jobs[0]->context . ' in progress</span>';
			echo '</span>';

		} else {

			// Get the post's original language codes and take the first one as the main one
			$main_language_code = TRP_Helper::get_original_language_codes( $post_id )[0];

			/**
			 * Service selector
			 */
			echo '<span id="trp-container-' . $post_id . '" class="trp-container" status="finished" language="' . esc_attr( $main_language_code ) .'" nonce="' . wp_create_nonce('trp_display_job_status') . '">';

			// TODO: Later: Add trp_manage_transcripts as soon as we let Transposer transcribe via its API
			//if( in_array( get_current_user_id(), get_option( 'transposer_translation_permissions', array() ) ) || current_user_can( 'trp_manage_translations') || current_user_can( 'manage_options' ) ) {
			if( in_array( get_current_user_id(), get_option( 'options_users_with_transposer_permission', array() ) ) || current_user_can( 'trp_manage_translations') || current_user_can( 'manage_options' ) ) {
				// Only display if the current item is an attachment //and it has transcripts
				if( get_post_type( $post_id ) == 'attachment' ) // && count( $this->get_transcripts_list( $post_id ) ) > 0 )
					echo '  <span class="trp-load-service-selector" post_id="' . esc_attr( $post_id ) . '" nonce="' . wp_create_nonce( 'trp_create_job' ) .'"><span class="dashicons dashicons-admin-generic"></span></span>';

				// If it's another post type then display it, because it's translatable
				if( get_post_type( $post_id ) != 'attachment' )
					echo '  <span class="trp-load-service-selector" post_id="' . esc_attr( $post_id ) . '" nonce="' . wp_create_nonce( 'trp_create_job' ) .'"><span class="dashicons dashicons-admin-generic"></span></span>';

			}

			echo '  <span id="trp-selector-container-' . esc_attr( $post_id ) . '" class="trp-selector-container"></span>';
			echo '</span>';

			echo '<span class="trp-transcripts-container">';
			echo $this->get_transcripts_icons( $post_id );
			echo $this->get_translations_icons( $post_id );
			echo '</span>';


		}

	}


	/**
	 * Displays existing post translations by displaying corresponding icons
	 *
	 * @param   int     A post ID
	 * @return  string  HTML markup of icons representing the translations for each language
	 */
	public function get_translations_icons( $post_id ) {

		// Only for WPML
		// If the current post is not the original, get the list of translations from the original post
		if( TRP_WPML::is_wpml() ) {
			$post_id = TRP_WPML::get_original( $post_id );
		}

		$translations = $this->get_translations( $post_id );

		$icons = array();

		foreach( $translations as $translation ) {
			$icons[] = '<a class="trp-btn trp-edit-translation" title="' . esc_attr( TRP_Helper::get_all_languages()[$translation->language] ) . '" translation_id="'. esc_attr( $translation->id ) . '" nonce="' . wp_create_nonce( 'trp_get_translation' ) . '">' . esc_html( $translation->language ) . '</a>';
		}

		return implode( '', $icons );

	}


	/**
	 * Update the modification date of a post without calling wp_update_post() in order not to trigger update actions by other plugins
	 *
	 * @param     int            The Post ID to update
	 * @return    boolean|int    Number of affected rows or false on error
	 */
	public static function update_modification_date( $post_id ) {
		$now = date( 'Y-m-d H:i:s' );

		global $wpdb;
		return $wpdb->query( $wpdb->prepare( "UPDATE ". $wpdb->posts . " SET post_modified='" . $now . "', post_modified_gmt='" . get_gmt_from_date( $now ) . "' WHERE ID = %d", (int) $post_id ) );
	}


	/**
	 * Test whether a transcript exists or not
	 *
	 * @param    int       Post ID of an attachment
	 * @param    string    2 character language code
	 * @return   boolean
	 */
	public function transcript_exists( $post_id, $language = '' ) {
		global $wpdb;

		$transcript = $wpdb->get_row( $wpdb->prepare( "SELECT id FROM " . $wpdb->prefix . "trp_transcripts WHERE post_id = %d AND language = %s", (int) $post_id, $language ) );

		if( is_null( $transcript ) || empty( $transcript ) )
			return false;

		return true;
	}


	/**
	 * Whether the current user can edit a given transcript
	 *
	 * @param    object|int    The transcript object or the transcript's ID
	 * @return   boolean
	 */
	public function user_can_edit_transcript( $transcript ) {

		// Is the user the admin?
		if( current_user_can( 'manage_options' ) )
			return true;

		// Does the user have the particular capability?
		if( current_user_can( 'trp_manage_transcripts') )
			return true;

		// Is the user the author of the transcript?
		if( ! is_object( $transcript ) )
			$transcript = $this->get_transcript( $transcript );

		if( $transcript->user_id == get_current_user_id() )
			return true;

		if( $transcript->user_id == 0 ) {
			// If wp_transcripts.user_id is 0 (when it was created automatically), test if the post belongs to the current user
			// Test for the author of the post
			if( current_user_can( 'edit_post', $transcript->post_id ) )
				return true;

		}

		// Make it possible to do other permission tests here
		do_action( 'trp_user_can_edit_transcript', $transcript_id );

		return false;

	}


	/**
	 * Whether the current user can edit a given post translation
	 *
	 * @param    object|int    The translation object or the translation's ID
	 * @return   boolean
	 */
	public function user_can_edit_translation( $translation ) {

		// Is the user the admin?
		if( current_user_can( 'manage_options' ) )
			return true;

		// Does the user have the particular capability?
		if( current_user_can( 'trp_manage_translations') )
			return true;

		// Is the user the author of the transcript?
		if( ! is_object( $translation ) )
			$translation = $this->get_transcript( $translation );

		if( $translation->user_id == get_current_user_id() )
			return true;

		if( $translation->user_id == 0 ) {
			// If wp_transcripts.user_id is 0 (when it was created automatically), test if the post belongs to the current user
			// Test for the author of the post
			if( current_user_can( 'edit_post', $translation->post_id ) )
				return true;

		}

		// Make it possible to do other permission tests here
		do_action( 'trp_user_can_edit_translation', $translation_id );

		return false;

	}


	/**
	 * Add the database tables, default options and roles/capabilities on plugin activation
	 */
	public static function plugin_activation() {

		global $wpdb;

		// Transcripts (and their translations)
		$wpdb->query("
			CREATE TABLE IF NOT EXISTS `" . $wpdb->prefix . "trp_transcripts` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `post_id` int(11) NOT NULL,
			  `job_id` int(11) NOT NULL,
			  `media_id` varchar(255) NOT NULL,
			  `model` varchar(255) NOT NULL,
			  `duration` float NOT NULL,
			  `transcript` longtext NOT NULL,
			  `language` varchar(6) NOT NULL,
			  `type` varchar(255) NOT NULL,
			  `author` varchar(255) NOT NULL,
			  `license` varchar(255) NOT NULL,
			  `meta` longtext NOT NULL,
			  `segments` longtext NOT NULL,
			  `score` float NOT NULL,
			  `parent_id` int(11) NOT NULL,
			  `date_created` datetime NOT NULL,
			  `date_modified` datetime NOT NULL,
			  `user_id` int(11) NOT NULL,
			  PRIMARY KEY (`id`),
			  KEY `id` (`id`,`post_id`,`job_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4
		");

		// Post translations
		$wpdb->query("
			CREATE TABLE IF NOT EXISTS `" . $wpdb->prefix . "trp_translations` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `job_id` int(11) NOT NULL,
			  `post_id` int(11) NOT NULL,
			  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
			  `subtitle` text COLLATE utf8mb4_unicode_ci NOT NULL,
			  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
			  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
			  `language` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
			  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
			  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
			  `date_created` datetime NOT NULL,
			  `date_modified` datetime NOT NULL,
			  `user_id` int(11) NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
		");

		// Transposer Job Queue
		$wpdb->query("
			CREATE TABLE IF NOT EXISTS `" . $wpdb->prefix . "trp_transposer` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `post_id` int(11) NOT NULL,
			  `job_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
			  `transcript_id` int(11) NOT NULL,
			  `service` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
			  `request` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
			  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
			  `status_message` TEXT COLLATE utf8mb4_unicode_ci NOT NULL,
			  `response_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
			  `duration` float NOT NULL,
			  `start` datetime NOT NULL,
			  `stop` datetime NOT NULL,
			  `context` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
			  `date_created` datetime NOT NULL,
			  `date_modified` datetime NOT NULL,
			  `user_id` int(11) NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4
		");

		// Repco Depublication Log
		$wpdb->query("
			CREATE TABLE IF NOT EXISTS `" . $wpdb->prefix ."trp_depublication_log` (
			  `id` int(255) NOT NULL AUTO_INCREMENT,
			  `post_id` int(255) DEFAULT NULL,
			  `post_type` varchar(255) NOT NULL,
			  `action` varchar(255) NOT NULL,
			  `user_id` int(255) DEFAULT NULL,
			  `timestamp` datetime DEFAULT NULL,
			  PRIMARY KEY (`id`)
		  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
		");

		$wpdb->query( "ALTER TABLE ". $wpdb->prefix . "trp_transposer MODIFY status_message TEXT NOT NULL" );

		/**
		 * Add default options if they aren't set yet
		 * Doesn't overwrite existing option values
		 */

		$default_options = TRP_Helper::get_default_options();

		foreach( $default_options as $option_name => $option_value ) {
			$option_v = get_option( $option_name );

			if( ! $option_v || empty( $option_v ) )
				update_option( $option_name, $option_value );

		}


		/**
		 * Set user roles capabilities when plugin is activated
		 */

		global $wp_roles;

		// Add capabilities for the admin to access this plugin
		if( class_exists( 'WP_Roles' ) ) {

			if ( ! isset( $wp_roles ) )
				$wp_roles = new WP_Roles();

			if( is_object( $wp_roles ) ) {
				$wp_roles->add_cap( 'administrator', 'trp_manage_transposer' );
				$wp_roles->add_cap( 'administrator', 'trp_manage_translations' );
				$wp_roles->add_cap( 'administrator', 'trp_manage_transcripts' );

				$wp_roles->add_cap( 'editor', 'trp_manage_transposer' );
				$wp_roles->add_cap( 'editor', 'trp_manage_translations' );
				$wp_roles->add_cap( 'editor', 'trp_manage_transcripts' );
			}
		}


		/**
		 * Repair incorrect data
		 *
		 * When using WPML we could translate from a translation, so the translation relates to the translation and not the original
		 * That causes inconsistencies in the API and is also confusing in the UI
		 * So we make sure that all translations always relate to the original post, not another translation
		 */
		if( TRP_WPML::is_wpml() ) {
			// Get all post IDs from the trp_translations table
			$post_ids = $wpdb->get_col( "SELECT DISTINCT(post_id) FROM " . $wpdb->prefix . "trp_translations" );

			$original_post_ids = array();

			foreach( $post_ids as $post_id ) {
				$original_post_id = TRP_WPML::get_original( $post_id );

				$original_post_ids[] = $original_post_id;

				// If the saved post ID is not the original, rewrite it
				if( $original_post_id != $post_id ) {
					$wpdb->query( $wpdb->prepare( "UPDATE " . $wpdb->prefix . "trp_translations SET post_id = %d WHERE post_id = %d", (int) $original_post_id, (int) $post_id ) );
				}
			}

			// Update the modification date to trigger the re-ingest by Repco
			if( count( $original_post_ids ) > 0 ) {
				$wpdb->query( "UPDATE " . $wpdb->posts . " SET post_modified = DATE_ADD(post_modified, INTERVAL 1 second), post_modified_gmt = DATE_ADD(post_modified_gmt, INTERVAL 1 second) WHERE ID IN(" . implode(',', $original_post_ids ) . ")" );
			}

		}

	}


	public static function plugin_deactivation() {

		// Deactivate cron jobs
		wp_clear_scheduled_hook( 'trp_cron_process_renderqueue' );
		wp_clear_scheduled_hook( 'trp_cron_process_responses' );

		// TODO: Maybe let tables be deleted? The data are valuable and costed money.

	}


	/**
	 * Sorting function to sort services alphabetically by name
	 */
	public function compare_services_names( $a, $b ) {
	    if( $a->name == $b->name )
	        return 0;

	    return( $a->name < $b->name ) ? -1 : 1;
	}

}


/**
 * Initializes the class
 */
function trp_init() {
	$TRP = new TRP;
	new TRP_Admin;

	// Settings functions (need to be called after the main class)
	require( TRP_ABSPATH . '/transposer-settings.php' );

	// REPCO API extensions
	require( TRP_ABSPATH . '/transposer-repco.php' );

}
add_action( 'plugins_loaded', 'trp_init' );

?>