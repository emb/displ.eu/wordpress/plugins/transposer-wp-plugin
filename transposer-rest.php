<?php

/**
 * This is only applied for cba.media
 */
class TRP_Rest {

	public function __construct() {

		/**
		 * Register REST endpoint
		 * /transposer/v1/transcripts/<transcript_id>
		 *
		 * Returns a transcript by its ID and a list of the transcript's translations
		 */
		add_action( 'rest_api_init', function () {
		    //Path to meta query route
		    register_rest_route( 'transposer/v1/', '/transcripts/(?P<id>\d+)', array(
					'methods' => 'GET',
					'callback' => array( $this, 'rest_transcripts' ),
					'permission_callback' => '__return_true' // TODO: Authentication?
		    ) );
		});


		/**
		 * Register REST endpoint
		 * /transposer/v1/media/<attachment_id>
		 *
		 * Displays data for an attachment
		 */
		add_action( 'rest_api_init', function () {
		    //Path to meta query route
		    register_rest_route( 'transposer/v1/', '/media/(?P<id>\d+)', array(
					'methods' => 'GET',
					'callback' => array( $this, 'rest_attachment' ),
					'permission_callback' => '__return_true' // TODO: Authentication?
		    ) );
		});


		/**
		 * Adds transcripts to the media endpoint /wp-json/wp/v2/media/12345
		 */
		add_action( 'rest_api_init', array( $this, 'rest_media_add_transcripts' ) );


		/**
		 * For every post type defined in the options add
		 *  - the language code of the original language
		 *  - the translations to the REST API
		 *
		 * TODO: Create the options page
		 */
		$rest_api_post_types = array( 'post', 'attachment', 'series', 'station', 'channel' );
		foreach( $rest_api_post_types as $post_type ) {

			/**
			 * Adds language codes to the endpoint of a given post type /wp/v2/<post_type>/<id>
			 *
			 */
			// schema
			$language_codes_schema = array(
				'description'   => 'Language codes for a post type',
				'type'          => 'array',
				'context'       => ['view']
			);

			register_rest_field(
				$post_type,
				'language_codes',
				array(
					'get_callback'      => array( $this, 'add_language_codes' ),
					'update_callback'   => null,
					'schema'            => $language_codes_schema
				)
			);


			/**
			 * Adds post translations to the endpoint of a given post type /wp/v2/<post_type>/<id>
			 *
			 */

			// schema
			$translations_schema = array(
				'description'   => 'Translations for a post type',
				'type'          => 'array',
				'context'       => ['view']
			);

			register_rest_field(
				$post_type,
				'translations',
				array(
					'get_callback'      => array( $this, 'add_post_translations' ),
					'update_callback'   => null,
					'schema'            => $translations_schema
				)
			);


		}

	}


	public function rest_attachment( $data ) {

		if( ! $attachment = get_post( $data['id'] ) )
			return new WP_REST_Response( array( 'message' => 'Media file not found.' ), 404 );

		if( ! in_array( $attachment->post_mime_type, array( 'audio/mpeg', 'audio/mp3', 'audio/ogg', 'video/mp4' ) ) )
			return new WP_REST_Response( array( 'message' => 'Mime type not supported.' ), 400 );

		$return = new StdClass;
		$return->title = $attachment->post_title;
		$return->excerpt = $attachment->post_excerpt;
		$return->content = $attachment->post_content;
		$return->user_id = $attachment->post_author;
		$return->date_created = $attachment->post_date;
		$return->date_modified = $attachment->post_modified;

		$filename = get_post_meta( $attachment->ID, '_wp_attached_file', true );

		// URL to the original media file
		$return->url = wp_upload_dir()['baseurl'] . '/' . $filename;

		$pathinfo = pathinfo( $filename );

		$filename_cut = $pathinfo['dirname'] . '/' . $pathinfo['filename'] . '_cut.' . $pathinfo['extension'];

		// If there's already a cut file, include it
		if( file_exists( wp_upload_dir()['basedir'] . '/' . $filename_cut ) )
			$return->cut_url = wp_upload_dir()['baseurl'] . '/' . $filename_cut;
		else
			$return->cut_url = '';

		// Add attachment's metadata
		$metadata = wp_get_attachment_metadata( $attachment->ID );
		$return->fileformat = $metadata['fileformat'];
		$return->bitrate = $metadata['bitrate'];
		$return->mime_type = $metadata['mime_type'];
		$return->length = $metadata['length'];
		$return->length_formatted = $metadata['length_formatted'];
		$return->license = get_post_meta( $attachment->ID, 'license', true );

		// Add regions
		$regions = new TRP_Regions( $attachment->ID );
		$return->regions = $regions->regions;

		$return->nonce = wp_create_nonce( 'trp_edit_transcript' );

		return new WP_REST_Response( $return, 200 );
	}


	/**
	 * JSON output for REST endpoint /transposer/v1/transcripts/<transcript_id>
	 */
	public function rest_transcripts( $data ) {

		$transcripts = array(); // The output array
		$parent_transcript = false;
		$TRP = new TRP;

		// The transcript ID to edit
		$transcript_id = $data['id'];

		// If not existing, return an error
		if( ! $transcript = $TRP->get_transcript( $transcript_id ) )
		   return new WP_REST_Response( array( 'message' => 'Transcript not found' ), 404 );

		// Add the media file's data to the output
		if( ! $attachment = get_post( $transcript->post_id ) )
			return new WP_REST_Response( array( 'message' => 'Attachment not found' ), 404 );

		$transcripts['file'] = new StdClass;
		$transcripts['file']->id = $attachment->ID;
		$transcripts['file']->title = $attachment->post_title;
		$transcripts['file']->excerpt = $attachment->post_excerpt;
		$transcripts['file']->content = $attachment->post_content;
		$transcripts['file']->user_id = $attachment->post_author;
		$transcripts['file']->date_created = $attachment->post_date;
		$transcripts['file']->date_modified = $attachment->post_modified;
		$transcripts['file']->url = wp_upload_dir()['baseurl'] . '/' . get_post_meta( $attachment->ID, '_wp_attached_file', true ); // The original media file

		// Add attachment's metadata
		$metadata = wp_get_attachment_metadata( $attachment->ID );
		$transcripts['file']->fileformat = $metadata['fileformat'];
		$transcripts['file']->bitrate = $metadata['bitrate'];
		$transcripts['file']->mime_type = $metadata['mime_type'];
		$transcripts['file']->length = $metadata['length'];
		$transcripts['file']->length_formatted = $metadata['length_formatted'];


		// Add the requested transcript object
		unset( $transcript->transcript ); // Remove full text
		unset( $transcript->duration ); // Remove duration
		$transcripts['transcript'] = $transcript;


		// Add the list of existing transcripts
		$transcripts['transcripts'] = $TRP->get_transcripts( $transcript->post_id );

		// Remove the plain text, we don't need it here, just slows everything down
		foreach( $transcripts['transcripts'] as $key => $tr ) {
			if( isset( $transcripts['transcripts'][$key]->transcript ) )
			 	unset( $transcripts['transcripts'][$key]->transcript );
		}

		$transcripts['nonce'] = wp_create_nonce( 'trp_get_transcript' );

/* TODO: Do we really need that?


		// Add the parent post's data too if it's a file that was already published
		if( $attachment->post_parent > 0 && $post = get_post( $attachment->post_parent ) ) {
			$transcripts['post'] = new StdClass;
			$transcripts['post']->id = $post->ID;
			$transcripts['post']->title = $post->post_title;
			$transcripts['post']->excerpt = $post->post_excerpt;
			$transcripts['post']->content = $post->post_content;
			$transcripts['post']->user_id = $post->post_author;
			$transcripts['post']->date_created = $post->post_date;
			$transcripts['post']->date_modified = $post->post_modified;
			$transcripts['post']->url = get_post_permalink( $post->ID );
		}
*/
		return new WP_REST_Response( $transcripts, 200 );

	}


	/**
	 * Add transcripts to the WP media endpoint /wp/v2/media/<id>
	 *
	 */
	public function rest_media_add_transcripts() {
		// Schema
		$media_transcripts_schema = array(
			'description'   => 'Transcripts for the media item',
			'type'          => 'array',
			'context'       => ['view']
		);

		// Registering the field
		register_rest_field(
			'attachment',
			'transcripts',
			array(
				'get_callback'      => array( $this, 'add_transcripts' ),
				'update_callback'   => null,
				'schema'            => $media_transcripts_schema
			)
		);
	}

	/**
	 * Callback: Add transcripts for a given attachment (/media endpoint)
	 *
	 * @param  array            $object         The current post object
	 * @param  string           $field_name     The name of the field
	 * @param  WP_REST_request  $request        The current request
	 * @return string                           The return value
	 */
	public function add_transcripts( $object, $field_name, $request) {

		$TRP = new TRP;

		// If language was given, just return the transcript in this language
		$language = isset( $_GET['language'] ) && in_array( $_GET['language'], array_keys( TRP_Helper::get_transcription_languages() ) ) ? $_GET['language'] : '';
		return $TRP->get_transcripts( $object['id'], $language );
	}


	/**
	 * Callback: Add translations to a post type
	 *
	 * @param  array            $object         The current post object
	 * @param  string           $field_name     The name of the field
	 * @param  WP_REST_request  $request        The current request
	 * @return string                           The return value
	 */
	public function add_post_translations( $object, $field_name, $request) {

		if( ! isset( $_GET['multilingual'] ) )
			return array();

		$TRP = new TRP;
		$post_translations = $TRP->get_translations( $object['id'] );

		$translations = array();

		foreach( $post_translations as $translation ) {
			$translations[$translation->language] = new StdClass;
			$translations[$translation->language]->language = $translation->language;
			$translations[$translation->language]->title = $translation->post_title;
			$translations[$translation->language]->excerpt = $translation->post_excerpt;
			$translations[$translation->language]->content = do_blocks( apply_filters( 'the_content', $translation->post_content ) ); // Apply the_content filter to render short codes and do_blocks() to render block editor comments (e.g. <!-- wp:paragraph -->)
			$translations[$translation->language]->license = '';
			$translations[$translation->language]->type = $translation->type;
			$translations[$translation->language]->author = '';

			// Add the author's name if type is not 'auto'
			if( isset( $translation->type ) && $translation->type != 'auto' )
				$translations[$translation->language]->author = get_the_author_meta( 'display_name', $translation->user_id );


		}

		return $translations;

	}


	/**
	 * Callback: Add language codes to post type endpoints
	 *
	 * @param  array            $object         The current post object
	 * @param  string           $field_name     The name of the field
	 * @param  WP_REST_request  $request        The current request
	 * @return string                           The return value
	 */
	public function add_language_codes( $object, $field_name, $request) {

		$post_type = get_post_type( $object['id'] );

		$default_lang = 'de';

		switch( $post_type ) {

			case 'post':

				// Get the languages from the taxonomy 'language'
				$post_languages = get_the_terms( $object['id'], 'language' );
				if( ! $post_languages )
					return array( $default_lang );

				//echo "<pre>";
				//print_r( $post_languages );
				//exit;

				foreach( $post_languages as $lang ) {
					$language_codes[] = get_term_meta( $lang->term_id, 'language_code', true );
				}

			break;

			case 'attachment':

				$attachment = get_post( $object['id'] );

				if( $attachment->post_parent > 0 ) {

					// If published, try to retrieve the languages from the parent post
					$post_languages = get_the_terms( $attachment->post_parent, 'language' );

					if( ! $post_languages )
						return array( $default_lang );

					foreach( $post_languages as $lang ) {
						$language_codes[] = get_term_meta( $lang->term_id, 'language_code', true );
					}

				} else {
					// If unpublished return the default language
					$language_codes = array( $default_lang );
				}

			break;

			case 'series':
			case 'station':
			case 'channel':
			default:
				// Just return the default language
				$language_codes = array( $default_lang );
			break;

		}

		return $language_codes;

	}

}


class TRP_RestConfig {

	public function __construct() {

		/**
		 * Register REST endpoint
		 * /transposer/v1/config
		 */
		add_action( 'rest_api_init', function () {
		    //Path to meta query route
		    register_rest_route( 'transposer/v1/', '/config', array(
					'methods' => 'GET',
					'callback' => array( $this, 'rest_config' ),
					'permission_callback' => '__return_true'
		    ) );
		});

	}


	/**
	 * Return transposer options and values in endpoint /transposer/v1/config
	 */
	public function rest_config( $data ) {

		$default_options = TRP_Helper::get_default_options();

		$return = array();
		$return['transposer_version'] = TRP_VERSION; // Add plugin version

		// Return the saved option values
		foreach( $default_options as $option_name => $default_value ) {
			$return[$option_name] = get_option( $option_name );
		}

		$return['is_wpml'] = TRP_WPML::is_wpml(); // Whether WPML is installed
		$return['php_version'] = phpversion(); // PHP Version

		global $wpdb;

		// Test database tables
		$tables = array( 'trp_transposer', 'trp_translations', 'trp_transcripts', 'trp_depublication_log' );

		foreach( $tables as $table ) {
			$result = $wpdb->get_results( "SHOW TABLE STATUS FROM " . $wpdb->dbname . " WHERE Name = '" . $wpdb->prefix . $table . "'" );
			$return['table_status_' . $table] = $result;
		}

		return new WP_REST_Response( $return, 200 );
	}

}

// Only load for cba
if( defined( 'IS_CBA' ) && IS_CBA )
	$TRP_Rest = new TRP_REST;

// Load the config endpoint for everyone
$TRP_RestConfig = new TRP_RestConfig;

?>