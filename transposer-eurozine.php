<?php

/**
 * Interacting with the Eurozine database tables when retrieving originals and translations
 */
class TRP_Eurozine {

	public function __construct() {
	}


	/**
	 * Returns whether this is Eurozine.com
	 * The constant has to be set in wp-config.php
	 *
	 * @return    boolean
	 */
	public static function is_eurozine() {
		return defined( 'IS_EUROZINE' ) && IS_EUROZINE;
	}


	/**
	 * Returns all original post IDs
	 * No further filtering for post_types necessary as in TRP_WPML, since it will be filtered later to given post types anyway and WPML needs the distinction beforehand
	 */
	public static function get_all_originals( $post_types ) {
		global $wpdb;

		// All originals post ids are saved in postmeta with meta_key 'origin_lang'
		// Get all the original languages first
		//$posts_origin = $wpdb->get_results( "SELECT post_id, meta_value as origin_lang FROM " . $wpdb->postmeta . " WHERE meta_key='origin_lang'" );
		$posts_origin = $wpdb->get_results( "SELECT pm.post_id, pm.meta_value as origin_lang FROM " . $wpdb->postmeta . " pm, " . $wpdb->posts ." p WHERE pm.post_id=p.ID AND pm.meta_key='origin_lang' AND p.post_type IN('" . implode( "','", $post_types ) . "')");

		// Now get the lang key of all post_ids
		$posts_lang = $wpdb->get_results( "SELECT post_id, meta_value as lang FROM " . $wpdb->postmeta . " WHERE meta_key='lang'");

		// And put them in an array that's easy to compare e.g [1234] = 'en'
		$posts_compare = array();
		foreach( $posts_lang as $post ) {
			$posts_compare[$post->post_id] = $post->lang;
		}

		$original_post_ids = array();

		// Compare: if origin_lang is the same as the lang, then we know its the original post
		// Collect and return the post IDs found
		foreach( $posts_origin as $post ) {
			if( isset( $posts_compare[$post->post_id] ) && $post->origin_lang == $posts_compare[$post->post_id] )
				$original_post_ids[] = $post->post_id;
		}

		return $original_post_ids;

	}


	/*
	 * Test if the given post is of the original language
	 * Solution: Match the two post metas 'origin_lang' and 'lang' against each other
	 *
	 * @param     int        Post ID
	 * @return    boolean
	 */
	public static function is_original( $post_id ) {

		$origin_lang = get_post_meta( $post_id, 'origin_lang', true );
		$lang = get_post_meta( $post_id, 'lang', true );

		return $origin_lang == $lang;

	}


	/**
	 * Retrieve all other existing post translations (Taxonomy translations don't exist)
	 * Does not return the given post itself (Really ?)
	 *
	 * @param    int      Post ID
	 * @return   array    Array of filtered translations
	 */
	public static function get_translations( $post_id ) {
		global $wpdb;

		$filtered_translations = array();

		// 'related_posts' is an array in the form of
		// Array( [$post_id] = 'en', ... )
		$related_posts = get_post_meta( $post_id, 'related_posts', true );

		if( empty( $related_posts ) || ! is_array( $related_posts ) )
			return $filtered_translations;

		$post_ids = array_keys( $related_posts ); // The keys contain the post IDs of the translations
		//$post_ids = array_diff( $post_translation_ids, array( $post_id ) ); // Exclude the given post ID, since it's already included as the original

		// If there aren't any translations, make sure to tell WP_Query that nothing should be returned by matching with ID=0
		// If the 'include' array is empty, all posts will be returned otherwise!
		if( empty( $post_ids ) )
			$post_ids = array(0);

		// Retrieve all found translations and consider that there might be a custom post type used (if not given, default is 'post')
		$args = array( 'include' => $post_ids,
		               'post_type' => get_option( 'transposer_default_post_types', array( 'post' ) ),
		               'post_status' => 'publish',
		               'numberposts' => -1 );

		$posts = get_posts( $args );

		// Filter the post objects to only the fields we need
		foreach( $posts as $post ) {

			$lang = get_post_meta( $post->ID, 'lang', true ); // Get language from the post meta
			$lang = empty( $lang ) ? get_option( 'transposer_fallback_language' ) : $lang; // If it's empty, try to use the fallback language instead

			$filtered_translations[$lang]['is_eurozine'] = 1;
			$filtered_translations[$lang]['language'] = $lang;
			$filtered_translations[$lang]['id'] = $post->ID;
			$filtered_translations[$lang]['post_title'] = get_the_title( $post );
			$filtered_translations[$lang]['subtitle'] = get_post_meta( $post->ID, 'subtitle', true );
			$filtered_translations[$lang]['post_excerpt'] = trim( apply_filters( 'get_the_excerpt', $post->post_excerpt, $post ) ); // The filter expects the post object as a 3rd parameter

			// The real post_content is in postmeta 'article_content'. posts.post_title will be empty
			$filtered_translations[$lang]['post_content'] = get_post_meta( $post->ID, 'article_content', true ); // Will be block- & shortcode-rendered later in RepcoPostTranslations->get_field_translations()
			$filtered_translations[$lang]['permalink'] = get_post_permalink( $post );
			$filtered_translations[$lang]['license'] = ''; //get_post_meta( $post->ID, 'copyright', true ); // TODO: Where to get this information?
			$filtered_translations[$lang]['type'] = 'manual'; // Eurozine native translations are always hand-made
			$filtered_translations[$lang]['created'] = ( $post->post_date_gmt != '0000-00-00 00:00:00' ) ? $post->post_date_gmt : $post->post_date;
			$filtered_translations[$lang]['modified'] = $post->post_modified;
			$filtered_translations[$lang]['type'] = 'manual'; // Translations of Eurozine are always manual

			// TODO: Check if the information really has the right meaning
			$filtered_translations[$lang]['author'] = get_post_meta( $post->ID, 'copyright', true ); // Eurozine's 'copyright' contains seems to contain the original author's information
			//$filtered_translations[$lang]['author'] = get_post_meta( $post->ID, 'first_published', true ); // Eurozine's 'first_published' contains information about who was the originator of the article as well as the translations

		}

		return $filtered_translations;

	}

}

?>