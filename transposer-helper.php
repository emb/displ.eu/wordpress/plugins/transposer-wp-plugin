<?php

class TRP_Helper {

	public function __construct() {
	}


	/**
	 * Add default options
	 */
	public static function get_default_options() {

		$services = '{"transcribe":{"id":"transcribe","name":"Transcribe","description":"Create a transcript","url":"","endpoint":"transcribe","formats":["audio/mp3","audio/mpeg","audio/wav","video/mp4","video/webm"],"parameters":["model","language"]},"translate":{"id":"translate","name":"Translate","description":"Translate texts into 15 languages using the European Language Grid (ELG)","url":"https://european-language-grid.eu","endpoint":"translate","formats":["text/plain"],"parameters":["text","source_language_id","target_language_ids"]},"summarize":{"id":"summarize","name":"Summarize","description":"Summarize a text","url":"","endpoint":"summarize","formats":["text/plain"],"parameters":["text"]},"extractterms":{"id":"extractterms","name":"Extract terms","description":"Extract meaningful terms with NLP","url":"","endpoint":"extractterms/","formats":["text/plain"],"parameters":["text"]},"generatewaveform":{"id":"generatewaveform","name":"Generate waveform","description":"Generate a waveform from MP3 or OGG","url":"","endpoint":"generatewaveform/","formats":["audio/mp3","audio/ogg","audio/mpeg"],"parameters":["url"]},"detectspeech":{"id":"detectspeech","name":"Detect music/speech","description":"Distinguish music from spoken word","url":"","endpoint":"detectspeech/","formats":["audio/mp3","audio/mpeg"],"parameters":["url"]},"extractaudio":{"id":"extractaudio","name":"Extract audio from a video","description":"Using FFMPEG or FREEAC","url":"","endpoint":"extractaudio/","formats":["video/mp4","video/webm"],"parameters":["url"]},"detectspeakers":{"id":"detectspeakers","name":"Detect speakers","description":"Detects who is speaking (Diarisation)","url":"","endpoint":"diarisation/","formats":["audio/mp3","audio/mpeg"],"parameters":["url"]},"generatewebvtt":{"id":"generatewebvtt","name":"Generate Web VTT subtitles","description":"Generate a subtitle file","url":"","endpoint":"generatewebvtt/","formats":["json"],"parameters":["data"]},"audio2video":{"id":"audio2video","name":"Convert audio to video","description":"Generate a video from an MP3","url":"","endpoint":"audio2video/","formats":["audio/mp3","audio/mpeg"],"parameters":["url"]}}';

		$prefer_plugin = TRP_WPML::is_wpml() ? 'wpml' : 'transposer';

		return array(

					/* Additional permissions */

					// Users that may create translations (others than having the capability 'trp_manage_translations')
					'transposer_translation_permissions' => array(),

					// Users that may create transcripts (others than having the capability 'trp_manage_transcripts')
					'transposer_transcript_permissions' => array(),

					/* Job queue related options */

					'transposer_api_url' => TRP_API_URL,
					'transposer_enable_cron' => 1,
					'transposer_max_processes' => 1,
					'transposer_queue_order' => 'ASC',

					/* Repco API related options */

					'transposer_fallback_language' => 'en',
					'transposer_prefer_translation_plugin' => $prefer_plugin,
					'transposer_use_tags' => array(),
					'transposer_default_post_types' => array( 'post' ),
					'transposer_exclude_taxonomies' => array(),
					'transposer_contributor_taxonomies' => array(),
					'transposer_add_post_author' => 0,
					'transposer_content_grouping_taxonomies' => array(),
					'transposer_add_post_parent' => 0,
					'transposer_default_ui_post_types' => array( 'post' ),
					'transposer_do_api_shortcodes' => 0,
					'transposer_available_services' => (array) json_decode( $services ),

					'transposer_do_frontend_shortcodes' => 0


				);
	}


	/**
	 * Returns a list of available language models for transcription
	 * Contains the supported languages by Whisper AI
	 */
	public static function get_transcription_languages() {
		$array = array(
			'af' => __( 'Afrikaans', 'trp' ),
			'ar' => __( 'Arabic', 'trp' ),
			'hy' => __( 'Armenian', 'trp' ),
			'az' => __( 'Azerbaijani', 'trp' ),
			'be' => __( 'Belarusian', 'trp' ),
			'bs' => __( 'Bosnian', 'trp' ),
			'bg' => __( 'Bulgarian', 'trp' ),
			'zh' => __( 'Chinese', 'trp' ),
			'hr' => __( 'Croatian', 'trp' ),
			'cs' => __( 'Czech', 'trp' ),
			'da' => __( 'Danish', 'trp' ),
			'nl' => __( 'Dutch', 'trp' ),
			'en' => __( 'English', 'trp' ),
			'et' => __( 'Estonian', 'trp' ),
			'fi' => __( 'Finnish', 'trp' ),
			'fr' => __( 'French', 'trp' ),
			'de' => __( 'German', 'trp' ),
			'el' => __( 'Greek', 'trp' ),
			'he' => __( 'Hebrew', 'trp' ),
			'hu' => __( 'Hungarian', 'trp' ),
			'hi' => __( 'Hindi', 'trp' ),
			'is' => __( 'Icelandic', 'trp' ),
			'id' => __( 'Indonesian', 'trp' ),
			'it' => __( 'Italian', 'trp' ),
			'ja' => __( 'Japanese', 'trp' ),
			'lv' => __( 'Latvian', 'trp' ),
			'lt' => __( 'Lithuanian', 'trp' ),
			'mk' => __( 'Macedonian', 'trp' ),
			'no' => __( 'Norwegian', 'trp' ),
			'fa' => __( 'Persian', 'trp' ),
			'pl' => __( 'Polish', 'trp' ),
			'pt' => __( 'Portuguese', 'trp' ),
			'ro' => __( 'Romanian', 'trp' ),
			'ru' => __( 'Russian', 'trp' ),
			'es' => __( 'Spanish', 'trp' ),
			'sr' => __( 'Serbian', 'trp' ),
			'sk' => __( 'Slovakian', 'trp' ),
			'sl' => __( 'Slovenian', 'trp' ),
			'sv' => __( 'Swedish', 'trp' ),
			'ta' => __( 'Tamil', 'trp' ),
			'th' => __( 'Thai', 'trp' ),
			'tr' => __( 'Turkish', 'trp' ),
			'uk' => __( 'Ukrainian', 'trp' ),
			'ur' => __( 'Urdu', 'trp' ),
			'vi' => __( 'Vietnamese', 'trp' ),
			'cy' => __( 'Welsh', 'trp' )
		);

		asort( $array );

		return $array;

	}


	/**
	 * Returns a list of available language models for text-to-text translation supported by Transposer
	 * TODO: This list should in the end be provided by the /service endpoint to change dynamically
	 */
	public static function get_translation_languages() {

		$array = array(
			//'ar' => __( 'Arabic', 'trp' ),
			//'bs' => __( 'Bosnian', 'trp' ),
			//'hr' => __( 'Croatian', 'trp' ),
			'cs' => __( 'Czech', 'trp' ),
			'nl' => __( 'Dutch', 'trp' ),
			'en' => __( 'English', 'trp' ),
			'fr' => __( 'French', 'trp' ),
			'de' => __( 'German', 'trp' ),
			//'el' => __( 'Greek', 'trp' ),
			'hu' => __( 'Hungarian', 'trp' ),
			'it' => __( 'Italian', 'trp' ),
			'pl' => __( 'Polish', 'trp' ),
			'pt' => __( 'Portuguese', 'trp' ),
			'sr' => __( 'Serbian', 'trp' ),
			'sk' => __( 'Slovakian', 'trp' ),
			//'sl' => __( 'Slovenian', 'trp' ),
			'es' => __( 'Spanish', 'trp' ),
			'ru' => __( 'Russian', 'trp' ),
			'tr' => __( 'Turkish', 'trp' ),
			'uk' => __( 'Ukrainian', 'trp' )
		);

		asort( $array );

		return $array;

	}


	/**
	 * Returns a list of all available language
	 */
	public static function get_all_languages() {

		$array = array(
			'af' => __( 'Afrikaans', 'trp' ),
			'ar' => __( 'Arabic', 'trp' ),
			'hy' => __( 'Armenian', 'trp' ),
			'az' => __( 'Azerbaijani', 'trp' ),
			'be' => __( 'Belarusian', 'trp' ),
			'bs' => __( 'Bosnian', 'trp' ),
			'bg' => __( 'Bulgarian', 'trp' ),
			'zh' => __( 'Chinese', 'trp' ),
			'hr' => __( 'Croatian', 'trp' ),
			'cs' => __( 'Czech', 'trp' ),
			'da' => __( 'Danish', 'trp' ),
			'nl' => __( 'Dutch', 'trp' ),
			'en' => __( 'English', 'trp' ),
			'et' => __( 'Estonian', 'trp' ),
			'fi' => __( 'Finnish', 'trp' ),
			'fr' => __( 'French', 'trp' ),
			'de' => __( 'German', 'trp' ),
			'el' => __( 'Greek', 'trp' ),
			'he' => __( 'Hebrew', 'trp' ),
			'hu' => __( 'Hungarian', 'trp' ),
			'hi' => __( 'Hindi', 'trp' ),
			'is' => __( 'Icelandic', 'trp' ),
			'id' => __( 'Indonesian', 'trp' ),
			'it' => __( 'Italian', 'trp' ),
			'ja' => __( 'Japanese', 'trp' ),
			'lv' => __( 'Latvian', 'trp' ),
			'lt' => __( 'Lithuanian', 'trp' ),
			'mk' => __( 'Macedonian', 'trp' ),
			'no' => __( 'Norwegian', 'trp' ),
			'fa' => __( 'Persian', 'trp' ),
			'pl' => __( 'Polish', 'trp' ),
			'pt' => __( 'Portuguese', 'trp' ),
			'ro' => __( 'Romanian', 'trp' ),
			'ru' => __( 'Russian', 'trp' ),
			'es' => __( 'Spanish', 'trp' ),
			'sr' => __( 'Serbian', 'trp' ),
			'sk' => __( 'Slovakian', 'trp' ),
			'sl' => __( 'Slovenian', 'trp' ),
			'sv' => __( 'Swedish', 'trp' ),
			'ta' => __( 'Tamil', 'trp' ),
			'th' => __( 'Thai', 'trp' ),
			'tr' => __( 'Turkish', 'trp' ),
			'uk' => __( 'Ukrainian', 'trp' ),
			'ur' => __( 'Urdu', 'trp' ),
			'vi' => __( 'Vietnamese', 'trp' ),
			'cy' => __( 'Welsh', 'trp' )
		);

		asort( $array );

		return $array;

	}


	public static function get_exclude_taxonomies() {
		return array( 'post_format',
						  'translation_priority',
						  'nav_menu',
						  'link_category',
						  'wp_theme',
						  'wp_template_part_area',
						  'wp_pattern_category',
						  'elementor_library_type',
						  'elementor_library_category',
						  'acf_category',
						  'elementor_font_type',
						  'language'
		);
	}


	public static function get_exclude_post_types() {
		return array( 'attachment',
						  'revision',
						  'nav_menu_item',
						  'custom_css',
						  'customize_changeset',
						  'oembed_cache',
						  'wp_block',
						  'wp_template',
						  'wp_template_part',
						  'wp_global_styles',
						  'wp_navigation',
						  'acf-taxonomy',
						  'acf-post-type',
						  'acf-field-group',
						  'acf-field',
						  'taxopress_logs',
						  'user_request',
						  'elementor_library',
						  'feedzy_imports',
						  'feedzy_categories',
						  'ap_follower',
						  'elementor_font',
						  'elementor_icons',
						  'ppma_boxes',
						  'ppmacf_field'
		);
	}



	/**
	 * Calculate seconds to minutes:seconds
	 *
	 * @param 	int		Number of seconds
	 * @return 	string	Duration in mm:ss
	 */
	public static function sec2min( $sec = 0 ) {

		$sec = round( $sec );

		$m = floor( $sec / 60 );
		$s = ( $sec % 60 );

		if( strlen( $s ) == 1 )
			$s = '0' . $s;
		if( strlen( $m ) == 1 )
			$m = '0' . $m;

		return $m . ":" . $s;
	}


	/**
	* Calculates big numbers of seconds into human readable times
	*
	* TODO: Extend for weeks, months and years, perhaps decades
	*
	* @param    int     Number of seconds
	* @param    string  If 'days', it only returns the duration in days without hours or minutes
	* @return   string  E.g. '122 days 93 hrs.' or '10 hrs. 33:09 min.'
	*/
	public static function format_duration( $seconds, $output = '' ) {

		$seconds = round( (float) $seconds );

		$td['total'] = $seconds;
		$td['sec'] = $seconds % 60;
		$td['min'] = ( ( $seconds - $td['sec'] ) / 60 ) % 60;
		$td['hrs'] = ( ( ( ( $seconds - $td['sec'] ) / 60 ) -
		$td['min'] ) / 60 ) % 24;

		$td['day'] = floor( ( ( ( ( ( $seconds - $td['sec'] ) / 60 ) - $td['min'] ) / 60 ) / 24 ) );

		$return = "";

		if( $td['day'] != 0 ) {

			$return .= self::nf( $td['day'] ) . ' ' . __( 'days', 'trp' ) . ' ';

			if( $td['hrs'] != 0 && $output != 'days' )
				$return .= $td['hrs'] . ' ' . __( 'hrs.', 'trp' );

		} else {

			if( $td['hrs'] != 0 )
				$return .= self::nf( $td['hrs'] ) . ' ' . __( 'hrs.', 'trp' ) . ' ';

			if( $td['min'] != 0 ) {
				$return .= ( strlen( $td['min'] ) < 2 ) ? "0" . $td['min'] : $td['min'];
				$return .= ":";
			}

			$unit = ( $td['min'] > 0 ) ? __( 'Min.', 'trp' ) : __( 'Sec.', 'trp' );

			$return .= ( strlen( $td['sec'] ) < 2 ) ? "0" . $td['sec'] : $td['sec'];
			$return .= " " . $unit;
		}

		return $return;

	}


	/**
	 * Calculates milliseconds to hh:mm:ss.ms
	 *
	 * @param    int     Milliseconds
	 * @return   string  String in format hh:mm:ss.ms
	 *
	 */
	public static function milliseconds_to_date( $ms ) {

			$milliseconds = (int)$ms % 1000;
			$timeInMilliseconds = $ms / 1000;

			$seconds = (float)$timeInMilliseconds % 60;
			$timeInMilliseconds = $timeInMilliseconds / 60;

			$minutes = (float)$timeInMilliseconds % 60;
			$timeInMilliseconds = $timeInMilliseconds / 60;

			$hours = (int)$timeInMilliseconds;

			$h = $hours < 10 ? "0" . $hours : $hours;
			$m = $minutes < 10 ? "0" . $minutes: $minutes;
			$s = $seconds < 10 ? "0" . $seconds: $seconds;

			if( $milliseconds < 10 )
				$ms = "00" . $milliseconds;
			elseif( $milliseconds < 100 )
				$ms = "0" . $milliseconds;
			else
				$ms = "" . $milliseconds;

			return $h . ":" . $m . ":" . $s . "." . $ms;

	}


	/**
	 * Formats a float for better human readability: No decimals, thousand separator is '.'
	 *
	 * TODO: Improve when it comes to millions and more (like 1.2 Mio. instead of e.g. 1.253.239)
	 *
	 * @param    float     A number
	 * @param    boolean   Whether to round hundreds
	 */
	public static function nf( $number, $roundhundreds = false ) {

		// 1.548 = 1.500
		// 1.552 = 1.600
		if( $roundhundreds && $number > 100 ) {
			$number = round( $number / 100 );
			return number_format( (float) $number * 100, 0, ',', '.' );
		}

		return number_format( (float) $number, 0, ',', '.' );

	}


	/**
	 * Truncates a string by given attributes
	 *
	 * DEPRECATED: Replace by wp_trim_words( string $text, int $num_words = 55, string $more = null )
	 *
	 * @param	string		The string to truncate
	 * @param	int			How many characters should rely
	 * @param	string		Everything after the truncated string
	 * @param	boolean		Hard break within words if true; leaves words if false even the number of characters is higher than $length
	 * @return	string		The truncated string
	 */
	public static function truncate_string( $string, $length = 80, $etc = '...', $break_words = false ) {

		if( $length == 0 )
	    return '';

		if( strlen( $string ) > $length ) {
	    $length -= strlen( $etc );

	    if( ! $break_words )
	      $string = preg_replace( '/\s+?(\S+)?$/', '', substr( $string, 0, $length + 1 ) );

	    return substr( $string, 0, $length ) . $etc;
		} else
	    return $string;
	}


	/**
	 * Returns the markup for a status icon
	 *
	 * @param    string    The job status ('queued', 'inprogress', 'finished' or 'error')
	 * @return   string    THe HTML markup for displaying the icon
	 */
	public static function get_status_icon( $status, $job_id = 0 ) {
		$icons = array(
						'queued' => '<span class="dashicons dashicons-ellipsis"></span>',
						'inprogress' => '<img src="' . plugins_url( '/css/images/loading.gif', __FILE__ ) . '" class="trp-loading" title="' . esc_attr__( 'in progress...', 'trp' ). '" />',
						'finished' => '<span class="dashicons dashicons-yes"></span>',
						'error' => '<span class="dashicons dashicons-no"></span> <span class="button trp-retry-job" job_id="' . $job_id . '" nonce="' . wp_create_nonce( 'trp_retry_job' ) . '">Retry</span>'
		);

		return in_array( $status, array_keys( $icons ) ) ? $icons[$status] : '';

	}

	/**
	 * Truncates request and response data for better and shorter human readable output
	 *
	 * @param     object    An max. 2 dimensional object of data
	 * @return    string    A truncated string
	 */
	public static function truncate_response( $data ) {

		if( empty( $data ) )
			return '';

		$output = array();
		$return = '';

		// The request data
		if( isset( $data->input ) && ! is_null( $data->input ) ) {

			// Is it the transcript? Then the text is in [0]->text
			if( is_array( $data->input ) )
				 $data->input = $data->input[0]->text;

			if( is_object( $data->input ) )
				$data->input = $data->input->text;

			$output['input'] = self::truncate_string( strip_tags( $data->input ), 150 );

		}

		// DEPRECATED
		if( isset( $data->text ) )
			$output['text'] = self::truncate_string( strip_tags( $data->text ) );

		if( isset( $data->format ) )
			$output['format'] = $data->format;

		if( isset( $data->service_id ) )
			$output['service_id'] = $data->service_id;

		if( isset( $data->message ) )
			$output['message'] = $data->message;

		if( isset( $data->source_language_ids ) )
			$output['source_language_id'] = $data->source_language_id;

		if( isset( $data->target_language_ids ) )
			$output['target_language_ids'] = $data->target_language_ids;


		if( isset( $data->parameters ) && is_array( $data->parameters ) ) {
			foreach( $data->parameters as $parameter => $value ) {
				if( is_array( $value ) )
					$output[$parameter] = implode( ', ', $value );
				else
					$output[$parameter] = $value;
			}
		}


		// The response data
		if( isset( $data->payload ) && ! empty( $data->payload ) ) {

			$i = 1;

			foreach( $data->payload as $language_code => $tr ) {

				// Transcripts
				if( isset( $tr->payload ) && is_array( $tr->payload ) ) {
					foreach( $tr->payload as $translation ) {

						if( $i > 4 )
							break;

						if( isset( $output[$language_code] ) )
							$output[$language_code] .= $translation->text . ' ';
						else
							$output[$language_code] = $translation->text . ' ';

						$i++;

					}

				// Text translations
				} else {
					if( isset( $tr->text ) )
						$output[$language_code] = self::truncate_string( strip_tags( $tr->text ) );
				}

			}
		}

		if( isset( $data->error ) )
			$output[] = $data->error;

		// Generate output
		foreach( $output as $key => $value ) {
			$value = is_array( $value ) ? implode( ', ', $value ) : $value;
			$return .= '<span class="badge">' . $key . '</span>: ' . $value . '<br>';
		}

		return $return;

	}


	/**
	 * Return an array of original language codes of a post
	 * If the information is not there, return the fallback language code
	 *
	 * @param    int    The post ID of a post or attachment
	 */
	public static function get_original_language_codes( $post_id ) {

			$main_language_codes = array();

			if( TRP_WPML::is_wpml() ) {
				$lc = TRP_WPML::get_original_language_code( $post_id );

				if( ! empty( $lc ) )
					$main_language_codes[] = $lc;
			}

			if( TRP_Eurozine::is_eurozine() ) {
				$lc = get_post_meta( $post_id, 'origin_lang', true );
				if( ! empty( $lc ) )
					$main_language_codes[] = $lc;
			}

			// If still empty, try to get it from the language taxonomy and its metas
			if( empty( $main_language_codes ) && taxonomy_exists( 'language' ) ) {
					$post_languages = get_the_terms( $post_id, 'language' );

					if( is_array( $post_languages ) ) {
						foreach( $post_languages as $lang ) {
							$main_language_codes[] = get_term_meta( $lang->term_id, 'language_code', true );
						}
					}
			}

			// If still empty, set fallback language as default
			if( empty( $main_language_codes ) )
				$main_language_codes[] = get_option( 'transposer_fallback_language', 'en' );

			return array_unique( $main_language_codes );

	}


	/**
	 * Write content to a given file.
	 * Try to create the file if not existing.
	 * Overwrites an existing file.
	 *
	 * @param    string    Absolute filepath of the file to be created
	 * @param    string    The file's content
	 * @return   boolean   Whether it was successful or not
	 */
	public static function write_to_file( $filename, $content ) {

		// For local debugging
		//if( ! is_dir ( dirname( $filename ) ) )
		//	mkdir( dirname( $filename ) );

		if( $fp = @fopen( $filename, 'w' ) ) {
			fputs( $fp, $content );
			fclose( $fp );

			return true;

		}

		return false;

	}


	/**
	 * Removes plain URLs in a text but keeps URLs inside an attribute of the <a> tag and URLs between <a> and </a> intact
	 * This is mainly used to get rid of URLs in the post_content used by Wordpress' oembed function that automatically transforms plain URLs into an embed code
	 *
	 * Example:
	 *
	 * Input:
	 * Hello. I have an URL to get rid of: https://myurl1.com. And here's a link that I want to keep: <a href="https://myurl2.com">My Link to keep</a> and another one to keep: <a href="https://myurl3.com">http://www.myurl4.com</a>
	 *
	 * Output:
	 * Hello. I have an URL to get rid of:  And here's a link that I want to keep: <a href="https://myurl2.com">My Link to keep</a> and another one to keep: <a href="https://myurl3.com">http://www.myurl4.com</a>
	 *
	 * @param    string    A string containing plain URLs o
	 * @return   string    A string not
	 *
	 */
	public static function remove_plain_urls( $input ) {
	    // Array to store the <a> tags
	    $a_tag_store = [];

	    // Step 1: Replace <a> tags with placeholders to preserve them
	    $input = preg_replace_callback( '/<a\s+[^>]*href=["\'](https?:\/\/[^\s]+)["\'][^>]*>.*?<\/a>/is', function( $matches ) use ( &$a_tag_store ) {
	        // Store the <a> tag and return a unique placeholder
	        $a_tag_store[] = $matches[0]; // Store the full <a> tag
	        return '##A_TAG_PLACEHOLDER_' . ( count( $a_tag_store ) - 1 ) . '##'; // Return unique placeholder
	    }, $input);

	    // Step 2: Remove plain URLs outside <a> tags
	    $input = preg_replace( '/https?:\/\/[^\s<]+/i', '', $input );

	    // Step 3: Restore the <a> tags by replacing the placeholders back
	    foreach( $a_tag_store as $index => $a_tag ) {
	        $input = str_replace( '##A_TAG_PLACEHOLDER_' . $index . '##', $a_tag, $input );
	    }

	    return $input;
	}


}

?>