# Transposer WP Plugin

## Installation

1. Add a subdirectory "transposer" in your Wordpress Plugin directory, which is by default:

`/wp-content/plugins/`

2. Copy all the files to the directory and activate the plugin in your Wordpress backend.

## Features

The plugin adds the following features to your Wordpress site:

### Transcripts and subtitles

* Auto-generate transcripts and subtitles of audios and videos for more than 50 languages

### Subtitle Editor

* Edit subtitles with the subtitle editor

### Translations

* Auto-translate transcripts and subtitles into 15 languages
* Auto-translate posts (title, excerpt and content) into 15 languages

### Chapters

* Add chapters to audios and videos

### Cut audio files

* Cut copyrighted parts of audio files and save it as a second file to be freely licensed and to avoid copyright violations

## Authors
Code by Ingo Leindecker, Ngoc Tram Luu, Michael Goldbeck, Aslihan Özüyilmaz

## License
MIT

## Project status
More features to be added soon.