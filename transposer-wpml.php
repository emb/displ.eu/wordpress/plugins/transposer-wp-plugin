<?php

/**
 * Interacting with the WPML database tables when retrieving originals and translations
 *
 * Discuss: Replace custom functions with WPML internal functions, filters or action hooks as much as possible to keep upwards compatibility
 *    - Decided not to do that for certain functions, because the probability for having the queries manipulated by plugin filters and actions are too high and unforeseeable
 *    - So conclusion for now is: Do the queries ourselves until we get to know about a real WPML database change (which is hard to imagine when it comes to the WPML core functions)
 */
class TRP_WPML {

	public function __construct() {
	}


	/**
	 * Returns whether WPML is installed
	 * TODO: Do we need a test whether the plugin is activated? We actually only look for existing post content, not if it is active
	 *
	 * @return    boolean
	 */
	public static function is_wpml() {
		return class_exists( 'SitePress' ) ? class_exists( 'SitePress' ) : class_exists( 'WPML' );
	}


	/**
	 * Returns all PUBLISHED original post IDs (the ones that were the source for other translations) of a given post type
	 *
	 * TODO: Get those *published* posts without a WPML translation! (if they really exist)
	 * They might exist in posts table but shouldn't exist in icl_translations.element_id: TEST THIS!
	 *
	 */
	public static function get_all_originals( $post_types ) {
		global $wpdb;

		$sql_in_post_types = array();

		foreach( $post_types as $post_type ) {
			$sql_in_post_types[] = "'post_" . esc_sql( $post_type ) . "'";
		}

		// TODO: Do we really need the post_type check here if it gets filtered later anyway? Slows down the query
		$element_ids = $wpdb->get_col( "SELECT element_id FROM " . $wpdb->prefix . "icl_translations i, " . $wpdb->posts . " p WHERE i.element_type IN(" . implode( ',', $sql_in_post_types ) . ") AND i.source_language_code IS NULL AND p.post_status='publish' AND i.element_id=p.ID" );

		return $element_ids;
	}


	/**
	 * Returns a language code of the original version of a post
	 * Give a translation (or the original) post ID as an attribute
	 *
	 * @param    int       Post ID
	 * @return   string    A 2-character language code
	 */
	public static function get_original_language_code( $post_id ) {
		global $wpdb;

		// Get trid (WPML translation group) for the post
		$trid = apply_filters( 'wpml_element_trid', NULL, $post_id );
		return $wpdb->get_var( $wpdb->prepare( "SELECT language_code FROM " . $wpdb->prefix . "icl_translations WHERE trid = %d AND source_language_code IS NULL", (int) $trid ) );
	}


	/*
	 * Test if the given post is of the original language
	 * If source_language_code IS NULL it means in WPML that it doesn't have a parent, so it's the original one
	 *
	 * @param     int        Post ID
	 * @return    boolean
	 */
	public static function is_original( $post_id ) {
		global $wpdb;
		$exists = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(translation_id) FROM " . $wpdb->prefix . "icl_translations WHERE element_id = %d AND source_language_code IS NULL", (int) $post_id ) );
		return $exists > 0 ? true : false;
	}


	/**
	 * Returns the original post_id of a translation
	 *
	 * @param    int    Post ID of a translation or the original
	 * @return   int    Post ID of the original post
	 */
	public static function get_original( $post_id ) {
		global $wpdb;

		// Get the translation group to search for posts in the group in the next statement
		// Replace by: apply_filters( 'wpml_element_trid', mixed $empty_value, integer $element_id, string $element_type )
		$tr_id = $wpdb->get_var( $wpdb->prepare( "SELECT trid FROM ". $wpdb->prefix . "icl_translations WHERE element_id = %d", (int) $post_id ) );

		// If the language group doesn't exist, it is something else than a post translation, so we just return the given one
		if( is_null( $tr_id ) )
			return $post_id;

		// Search for the original post_id in the translation group
		$original_post_id = $wpdb->get_var( $wpdb->prepare( "SELECT element_id FROM " . $wpdb->prefix . "icl_translations WHERE trid = %d AND source_language_code IS NULL", (int) $tr_id ) );

		return is_null( $original_post_id ) ? (int) $post_id : (int) $original_post_id;

	}

	/**
	 * Retrieve all other existing post translations when using WPML
	 * Does not return the given post itself
	 *
	 * @param    int      Post ID
	 * @return   array    Array of filtered translations
	 */
	public static function get_translations( $post_id ) {
		global $wpdb;

		// Get the translation group to search for posts in the group in the next statement
		// Replace by: apply_filters( 'wpml_element_trid', mixed $empty_value, integer $element_id, string $element_type )
		$tr_id = $wpdb->get_var( $wpdb->prepare( "SELECT trid FROM ". $wpdb->prefix . "icl_translations WHERE element_id = %d", (int) $post_id ) );

		// Search for posts in the same group (trid)
		// We don't apply the filter 'wpml_get_element_translations' here since it only matches for a given post_type that we don't want to exclude now. We do that later anyway
		$translation_ids = $wpdb->get_results( $wpdb->prepare( "SELECT element_id, language_code FROM " . $wpdb->prefix . "icl_translations WHERE trid = %d", (int) $tr_id ) );

		// Transform the $posts object into an array that returns the post's language code when calling it with the post ID as the array index
		// Like $post_language_codes[1234] => 'en'
		// In this way we keep the language information of the post to add it to it later and avoid additional db queries
		$post_language_codes = array();

		// Collect post_ids
		$post_ids = array();

		foreach( $translation_ids as $plcode ) {
			$post_language_codes[$plcode->element_id] = $plcode->language_code;
			$post_ids[] = $plcode->element_id;
		}

		// If there aren't any translations, make sure to tell WP_Query that nothing should be returned by matching with ID=0
		// If the 'include' array is empty, all posts will be returned otherwise!
		if( empty( $post_ids ) )
			$post_ids = array(0);

		// Retrieve all found translations and consider that there might be a custom post type used (if not given, default is 'post')
		$args = array( 'include' => $post_ids,
		               'post_type' => get_option( 'transposer_default_post_types', array( 'post' ) ),
		               'post_status' => 'publish',
		               'numberposts' => -1 );

		$posts = get_posts( $args );

		// Filter the post objects to only the fields we need
		$filtered_translations = array();

		foreach( $posts as $post ) {

			// In some seldom cases the language code is 5 characters (e.g. 'pt-pt' instead of just 'pt') to indicate localization
			// We melt it down in every case to 2 characters
			$lang = substr( $post_language_codes[$post->ID], 0, 2 );

			$filtered_translations[$lang]['is_wpml'] = 1;
			$filtered_translations[$lang]['language'] = $lang;
			$filtered_translations[$lang]['id'] = $post->ID;
			$filtered_translations[$lang]['post_title'] = get_the_title( $post );
			$filtered_translations[$lang]['subtitle'] = ''; // Not covered by WPML
			$filtered_translations[$lang]['post_excerpt'] = trim( apply_filters( 'get_the_excerpt', $post->post_excerpt, $post ) ); // The filter expects the post object as a 3rd parameter
			$filtered_translations[$lang]['post_content'] = $post->post_content; // Post content is rendered later
			$filtered_translations[$lang]['permalink'] = get_post_permalink( $post );
			$filtered_translations[$lang]['license'] = ''; // TODO: Where to get this information?
			$filtered_translations[$lang]['type'] = ''; // TODO: Where to get the information whether this was auto-translated or not?
			$filtered_translations[$lang]['created'] = ( $post->post_date_gmt != '0000-00-00 00:00:00' ) ? $post->post_date_gmt : $post->post_date;
			$filtered_translations[$lang]['modified'] = $post->post_modified;

			// Add the author's name if type is not 'auto'
			// TODO: Where to get this information? Apparently WPML does not distinguish
			//if( isset( $translation->type ) && $translation->type != 'auto' )
			//	$filtered_translations[$translation->language]->author = get_the_author_meta( 'display_name', $translation->user_id );

		}

		return $filtered_translations;

	}


	/**
	 * Get the WPML current language code
	 * @return string
	 */
	function wpml_current_language() {
		return apply_filters( 'wpml_current_language', null );
	}

}

?>