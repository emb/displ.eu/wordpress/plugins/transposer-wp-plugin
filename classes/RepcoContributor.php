<?php

/**
 * Return an instance of a Repco Contributor
 */
class TRP_RepcoContributor {

	// Term or user ID
	public $id = 0;

	// Human readable name of the person/organization
	public $name = '';

	// TODO: Retrieve this from term_meta somehow
	public $description = '';

	// The role will be taxonomy if not otherwise set explicitly
	public $role = '';

	// TODO: Can this information be retrieved?
	public $personOrOrganization = '';

	// The contact information passed
	public $contactInformation = '';

	// The contact email address passed
	public $contactEmail = '';

	// TODO: Retrieve this from term_meta somehow
	public $profilePictureUid = 0;

	// Author URL
	public $url = '';

	public function __construct() {
	}


	/**
	 * Return a RepcoContributor object
	 *
	 * @param    object         WP_Term object with additional and optional fields (id|term_id, slug, taxonomy|role, contactInformation)
	 * @return   object|null    TRP_RepcoContributor object or NULL
	 */
	public static function get_instance( $term ) {

		if( ! is_object( $term ) )
			return null;

		$contributor = new StdClass;
		$contributor->id = isset( $term->term_id ) ? $term->term_id : $term->id;
		$contributor->name = $term->name;
		//$contributor->slug = isset( $term->slug ) ? $term->slug : sanitize_title( $term->name );
		$contributor->description = $term->description;
		$contributor->role = isset( $term->taxonomy ) ? $term->taxonomy : $term->role;
		$contributor->personOrOrganization = '';
		$contributor->contactInformation = isset( $term->contactInformation ) ? $term->contactInformation : '';
		//$contributor->contactEmail = isset( $term->contactInformation ) ? $term->contactInformation : ''; // TODO: Retrieve user email
		//$contributor->profilePictureUid = 0;
		$contributor->url = isset( $term->term_id ) ? get_term_link( $term ) : get_the_author_meta( 'url', $term->id ); // In case the contributor is not a term but an author, we need to ask for the author link ($term->id in this case is the author's user ID)


		return $contributor;

	}

}

?>