<?php

/**
 * Return a Repco Concept instance
 */
class TRP_RepcoConcept {

	public  $id = 0;

	public  $name = '';

	public  $description = '';

	public  $slug = '';

	public  $kind = '';

	public  $parent = 0;

	//public summary = '';


	public function __construct() {
	}


	/**
	 * Returns a TRP_RepcoConcept object, a filtered version of the WP_Term object including term translations
	 *
	 * @param     object    WP_Term object
	 * @return    object    RepoConcept object
	 */
	public static function get_instance( $term ) {

		if( ! is_object( $term ) )
			return null;

		/**
		 * Add term translations
		 */
		$filtered_taxonomy_translations = array();

		// Retrieve the term translation from WPML
		if( TRP_WPML::is_wpml() ) {
			$trid = apply_filters( 'wpml_element_trid', null, $term->term_id, 'tax_' . $term->taxonomy ); // Get the translation group id (trid) of the term
			$taxonomy_translations = apply_filters( 'wpml_get_element_translations', null, $trid, 'tax_' . $term->taxonomy ); // Get all the existing translations including the original

			foreach( $taxonomy_translations as $lc => $tt ) {
				$lc = substr( $lc, 0, 2 ); // Make sure we only get the first 2 characters as the language code (partly there are locales in WPML like 'pt-pt')
				$filtered_taxonomy_translations[$lc] = new StdClass;
				$filtered_taxonomy_translations[$lc]->value = $tt->name;
			}

		// Or just return the current value in the fallback language
		// (Eurozine term translations don't exist at all)
		// TODO: Add taxonomy translations to Transposer Plugin. As soon as this is there, consider the transposer_prefer_translation_plugin option to overrule another one
		} else {
			$fallback_language = get_option( 'transposer_fallback_language', 'en' );
			$filtered_taxonomy_translations[$fallback_language] = new StdClass;
			$filtered_taxonomy_translations[$fallback_language]->value = $term->name;
		}

		// Map the 'kind' to what Repco expects
		// TODO: Further kinds to be defined
		$map_term_taxonomy_from = array( 'post_tag', 'category' );
		$map_term_taxonomy_to = array( 'TAG', 'CATEGORY' );

		$concept = new StdClass;

		if( isset( $term->term_id ) )
			$concept->id = $term->term_id;

		if( isset( $term->id ) )
			$concept->id = $term->id;

		$concept->name = $filtered_taxonomy_translations;
		$concept->description = $term->description;
		$concept->slug = $term->slug;
		$concept->kind = strtoupper( str_replace( $map_term_taxonomy_from, $map_term_taxonomy_to, $term->taxonomy ) );
		$concept->parent = $term->parent;

		//$concept->summary = ''; // TODO: Does anyone have such information?

		return $concept;
	}

}

?>