<?php

class TRP_RepcoPostTranslations {

	private $_post_id = 0;

	private $_post = null;

	private $_translations = array();

	public  $return_original = true;


	public function __construct( $post ) {

		// If a post ID is given, get the post object first
		$post = is_numeric( $post ) ? get_post( $post ) : $post;

		$this->_post_id = $post->ID;
		$this->_post = $post;
		$this->get_translations();
	}


	/**
	 * Add translations to a post
	 *
	 * Looks for everything it can find and unifies the output datamodel:
	 *
	 *  - Includes existing WPML translations
	 *  - Includes custom translations for Eurozine.com
	 *  - Includes existing Transposer translations
	 *
	 * @param  int     The current post object
	 * @return array   Array of translations
	 */
	private function get_translations() {

		$post_id = $this->_post_id;

		$TRP = new TRP;

		$wpml_translations = array();
		$eurozine_translations = array();
		$filtered_translations = array();
		$translations = array();

		/**
		 * WPML Translations
		 *
		 * Search in WPML tables for existing post translations
		 * If the site uses WPML, we take these translations first
		 */
		if( TRP_WPML::is_wpml() ) {
			$wpml_translations = TRP_WPML::get_translations( $post_id ); // Get all the translations including the original
		}


		/**
		 * Eurozine Translations
		 *
		 * Search in Eurozine via postmeta 'related_posts' for existing post translations
		 * If the site is Eurozine, we take these translations first
		 */
		if( TRP_Eurozine::is_eurozine() ) {
			$eurozine_translations = TRP_Eurozine::get_translations( $post_id ); // Get all the translations including the original
		}


		/**
		 * Transposer Translations
		 *
		 * Search in Transposer tables for existing post translations
		 */

		// Add existing Transposer translations
		$trp_translations = $TRP->get_translations( $post_id );

		if( ! empty( $trp_translations ) ) {

			foreach( $trp_translations as $tr ) {

				$filtered_translations[$tr->language]['is_transposer'] = 1;
				$filtered_translations[$tr->language]['language'] = $tr->language;
				$filtered_translations[$tr->language]['post_id'] = $tr->post_id;
				$filtered_translations[$tr->language]['post_title'] = $tr->post_title;
				$filtered_translations[$tr->language]['subtitle'] = $tr->subtitle;
				$filtered_translations[$tr->language]['post_excerpt'] = $tr->post_excerpt;
				$filtered_translations[$tr->language]['post_content'] = $tr->post_content; // Applying filters will happen when get_field_translations() is called

				// For Transposer translations just add ?trpl= to the URL (resp. &trpl= if '?' is already in the URL)
				$permalink = get_post_permalink( $tr->post_id );
				$filtered_translations[$tr->language]['permalink'] = stristr( $permalink, '?' ) !== false ? $permalink . '&trpl=' . $tr->language : $permalink . '?trpl=' . $tr->language;

				$filtered_translations[$tr->language]['license'] = ''; // TODO: Where to get this information? Add fields for it (if not cba?)
				$filtered_translations[$tr->language]['type'] = $tr->type;
				$filtered_translations[$tr->language]['created'] = $tr->date_created;
				$filtered_translations[$tr->language]['modified'] = $tr->date_modified;
				$filtered_translations[$tr->language]['author'] = '';

				//if( isset( $tr->type ) && $tr->type != 'auto' && $tr->user_id > 0 )
				//	$filtered_translations[$tr->language]['author'] = get_the_author_meta( 'display_name', $tr->user_id );

			}

		}


		// In case there are translations for the same languasges existing in both WPML and Transposer tables
		// Let the user decide which one to overrule the other
		// For Repco we need one unique language key

		// If the settings prefer WPML over Transposer, overwrite
		// Merge the two arrays, but use array_replace instead of array_merge since latter one won't overwrite the former one if it has associative keys

		if( get_option( 'transposer_prefer_translation_plugin' ) == 'wpml' ) {

			// Prefer WPML
			// Overwrite TRP translations by WPML's if existing
			$translations = array_replace( $filtered_translations, $wpml_translations );

		} elseif( get_option( 'transposer_prefer_translation_plugin' ) == 'eurozine' ) {

			// Prefer Eurozine
			// Overwrite TRP translations by Eurozine's if existing
			$translations = array_replace( $filtered_translations, $eurozine_translations );

		} else {

			// Prefer Transposer
			// Overwrite WPML translations with TRP's if existing
			$translations = array_replace( $wpml_translations, $eurozine_translations, $filtered_translations );

		}

		$this->_translations = $translations;

		return $this->_translations;

	}


	/**
	 * Return an object containing all translations of a certain field
	 *
	 * @param    string    The field name to select from (Possible values: 'post_title', 'subtitle', 'post_excerpt', 'post_content', 'permalink')
	 * @return   object    Object containing keys named after the language codes and their translations as values, e.g. expressed as JSON: { 'en': { 'value': 'My Title' } }
	 */
	public function get_field_translations( $field_name = '' ) {
		$field_values = array(); // The return array

		$allowed = array( 'post_title', 'post_excerpt', 'post_content', 'subtitle', 'permalink' ); // Allowed fields to select from. 'subtitle' is Eurozine-specific for now, but we include it in Repco.

		if( empty( $field_name ) || ! in_array( $field_name, $allowed ) )
			return '';


		// Add the original language as the first object if requested
		if( $this->return_original ) {
			// The first one of the original languages is used as the main one
			// (Yes, there can be more than just one language to a post, since in audios/videos people may speak in different languages)
			$original_language = TRP_Helper::get_original_language_codes( $this->_post_id )[0];

			$field_values[$original_language] = new StdClass;

			if( $field_name == 'post_content' ) {

				 // Run the_content filter to render shortcodes and do_blocks() to force the Block editor block comments (e.g. <!-- wp:paragraph -->) to be rendered as HTML
				if( TRP_Eurozine::is_eurozine() ) {
					// Eurozine has original post_content in postmeta 'article_content'
					// ... and seems to have a wrong theme implementation of apply_filter( 'the_content', ...) in the theme, as this throws very weird errors
					// We strip shortchodes because it's wrongly implemented (do_shortcode() will cause an error by the theme)
					$field_values[$original_language]->value = wpautop( trim( do_blocks( strip_shortcodes( get_post_meta( $this->_post_id, 'article_content', true ) ) ) ) );
				} else {

					// Render or remove shortcodes dependent on the settings
					$field = get_option( 'transposer_do_api_shortcodes', '0' ) == '1' ? do_shortcode( $this->_post->$field_name ) : strip_shortcodes( $this->_post->$field_name );

					//$field_values[$original_language]->value = do_blocks( trim( apply_filters( 'the_content', $this->_post->$field_name ) ) );
					//$field_values[$original_language]->value = wpautop( trim( do_blocks( do_shortcode( $this->_post->$field_name ) ) ) );
					$field_values[$original_language]->value = wpautop( trim( do_blocks( $field ) ) );
				}

				if( isset( $_GET['dev'] ) )
					$field_values[$original_language]->raw = $this->_post->$field_name;

			} else {

				// Exception for Krytyka Polityczna
				// They have their post_excerpt in postmeta 'post_lead_text'
				if( $field_name == 'post_excerpt' && TRP_Krytyka::is_krytyka() )
					$field_values[$original_language]->value = get_post_meta( $this->_post_id, 'post_lead_text', true );
				else
					$field_values[$original_language]->value = is_null( $this->_post->$field_name ) ? '' : $this->_post->$field_name;

			}

		}


		// Add all the other translations to the object
		foreach( $this->_translations as $language => $fields ) {

			if( ! is_string( $language ) )
				continue;

			$field_values[$language] = new StdClass;

			if( $field_name == 'post_content' ) { // Content needs to be rendered in order to return full HTML and not block editor comments or shortcodes, etc.

				// Render or remove shortcodes dependent on the settings
				$field = get_option( 'transposer_do_api_shortcodes', '0' ) == '1' ? do_shortcode( $fields[$field_name] ) : strip_shortcodes( $fields[$field_name] );

				if( TRP_Eurozine::is_eurozine() ) {
					// Eurozine has original post_content in postmeta 'article_content'
					// ... and seems to have a wrong theme implementation of apply_filter( 'the_content', ...) in the theme, as this throws very weird errors
					//$field_values[$language]->value = trim( do_blocks( do_shortcode( get_post_meta( $fields['post_id'], 'article_content', true ) ) ) );
					// We strip shortcodes because it's wrongly implemented (do_shortcode() will cause an error by the theme)
					$field_values[$language]->value = wpautop( trim( do_blocks( $field ) ) );
				} else {
					//$field_values[$language]->value = do_blocks( trim( apply_filters( 'the_content', $fields[$field_name] ) ) );
					//$field_values[$language]->value = wpautop( trim( do_blocks( do_shortcode( $fields[$field_name] ) ) ) );
					$field_values[$language]->value = wpautop( trim( do_blocks( $field ) ) );
				}

				if( isset( $_GET['dev'] ) )
					$field_values[$language]->raw = $fields[$field_name];

			} else {
				$field_values[$language]->value = is_null( $fields[$field_name] ) ? '' : $fields[$field_name];
			}

		}

		return (object) $field_values;

	}


	/**
	 * Returns an array of language codes of all existing language versions
	 */
	public function get_existing_languages() {

		$languages = array();

		foreach( $this->_translations as $lang => $tr ) {
			$languages[] = $lang;
		}

		return $languages;

	}


}

?>