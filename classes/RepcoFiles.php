<?php

/**
 * Return all the files of a Repco mediaAsset
 *
 * For images: Add thumbnail sizes as files including the original version as the first one
 *
 * @param    array    Array of post objects of post_type 'attachment'
 */
class TRP_RepcoFiles {

	public function __construct( $attachment ) {
	}


	/**
	 *
	 */
	public static function get_files( $attachment ) {

		$file = new StdClass; // The main file object
		$files = array(); // The output array containing the main file (and thumbnail sizes if available)


		$filemeta = (array) get_post_meta( $attachment->ID, '_wp_attachment_metadata', true );

		// ContentUrl

		// URL to media file
		// TODO: If audio/video: replace contentUrl with cut URL if existing
		$wp_attached_file = get_post_meta( $attachment->ID, '_wp_attached_file', true );
		$url = wp_get_upload_dir()['baseurl'] . '/' . $wp_attached_file;

		$file->contentUrl = $url;

		// Filesize: Get it from the metadata or from the file itself
		if( isset( $filemeta['filesize'] ) ) {
			$file->contentSize = $filemeta['filesize'];
		} else {
			// In case it's an image, there will be no filesize present, so get it from its absolute filepath
			$filepath = wp_get_upload_dir()['basedir'] . '/' . $wp_attached_file;

			if( file_exists( $filepath ) )
				$file->contentSize = filesize( $filepath );

		}

		$file->mimeType = $attachment->post_mime_type;

		// Duration: Only present if audio or video
		if( isset( $filemeta['length'] ) )
			$file->duration = $filemeta['length'];

		// Deprecated
		// TODO: Where to retrieve this? Do we even need it?
		if( isset( $filemeta['codec'] ) )
			$file->codec = $filemeta['codec'];

		// Bitrate: Only present if audio or video
		if( isset( $filemeta['bitrate'] ) )
			$file->bitrate = $filemeta['bitrate'];

		// Resolution: width and height, separated by 'x' as a string, e.g. '120x80'
		// Only present if image or video
		if( isset( $filemeta['width'] ) && isset( $filemeta['height'] ) )
			$file->resolution = $filemeta['width'] . 'x' . $filemeta['height'];

		$sizes = array();

		// additionalMetadata: Add everything that's available, except the sizes
		if( isset( $filemeta['sizes'] ) ) {
			$sizes = $filemeta['sizes']; // Add the sizes as attachments later, so...
			unset( $filemeta['sizes'] ); // ...remove them from additionalMetadata
		}

		$file->additionalMetadata = null;

		if( isset( $filemeta['image_meta'] ) )
			$file->additionalMetadata = $filemeta['image_meta'];


		// Add the original attachment as the first file
		$files[] = $file;

		// Image sizes
		// Add each size as a file
		if( count( $sizes ) > 0 ) {

			$i = 0;

			$thumbnail_files = array();

			foreach( $sizes as $size => $fversion ) {

				// If there's no file name, there's no file to relate to, so skip
				if( ! isset( $fversion['file'] ) )
					continue;

				// If the filename in the _wp_attachment_metadata key is not present
				// take get _wp_attached_file meta instead
				if( ! isset( $filemeta['file'] ) )
					$filemeta['file'] = $wp_attached_file;

				// $size will be smthg. like 'small', 'medium', 'large', 'preview-single' or anything else
				// $fversion contains the file's metadata
				$thumbnail_files[$i] = new StdClass;

				$thumbnail_files[$i]->contentUrl = wp_get_upload_dir()['baseurl'] . '/' . dirname( $filemeta['file'] ) . '/' . basename( $fversion['file'] ); // The size file is always in the same directory as the original
				// Filesize: Get it from the metadata or from the file itself
				if( isset( $fversion['filesize'] ) ) {
					$thumbnail_files[$i]->contentSize = $fversion['filesize'];
				} else {
					// In case it's an image, there will be no filesize present, so get it from its absolute filepath
					$filepath = wp_get_upload_dir()['basedir'] . '/' . dirname( $filemeta['file'] ) . '/' . basename( $fversion['file'] );

					if( file_exists( $filepath ) )
						$thumbnail_files[$i]->contentSize = filesize( $filepath );

				}

				if( isset( $fversion['mime-type'] ) )
					$thumbnail_files[$i]->mimeType = $fversion['mime-type'];

				$thumbnail_files[$i]->resolution = null;

				if( isset( $fversion['width'] ) && isset( $fversion['height'] ) )
					$thumbnail_files[$i]->resolution = $fversion['width'] . 'x' . $fversion['height'];

				$thumbnail_files[$i]->additionalMetadata = array( 'size' => $size );

				$i++;

			}

			// Add the sizes to the return objet
			$files = array_merge( $files, $thumbnail_files );

		}

		return $files;

	}

}

?>