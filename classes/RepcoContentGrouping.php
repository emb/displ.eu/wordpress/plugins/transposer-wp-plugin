<?php

class TRP_RepcoContentGrouping {

	public $id = 0;

	public $broadcastSchedule = array();

	// 'show', 'feed', ...?
	public $groupingType = 'show';

	// Unix timestamp without time zone
	public $startingDate = 0;

	public $subtitle = array();

	// Unix timestamp without time zone
	public $terminationDate = 0;

	// TODO: Do we really need that?
	public $licenseUid = 0;

	// TODO: Do we really need that?
	public $license = '';

	// EPISODIC|SERIAL
	public $variant = '';

	public $description = array();

	public $summary = array();

	public $title = array();

	// TODO: Add that to Repco
	public $url = '';

	public function __construct() {
	}


	/**
	 * Return a RepcoContentGrouping instance
	 *
	 * A ContentGrouping can either relate to a post or a taxonomy term
	 *
	 * @param    int      A post or term ID
	 * @return   object   RepcoContentGrouping object
	 */
	public static function get_instance( $object_or_id, $entity = 'term' ) {

		if( ! in_array( $entity, array( 'term', 'post' ) ) )
			return null;

		$object = new StdClass;

		if( $entity == 'term' ) {

			// Take the object or get the term object by its ID
			$term = is_object( $object_or_id ) ? $object_or_id : get_term( $object_or_id );
			$concept = TRP_RepcoConcept::get_instance( $term ); // Get the concept including all its translations

			$object->id = $concept->id;
			$object->groupingType = 'focus';
			$object->startingDate = ''; // Get first post_date from posts that relate to the term
			$object->variant = 'SERIAL'; // TODO: Where to get this information?
			$object->title = $concept->name;
			$object->description = $concept->description;
			$object->url = get_term_link( $term );
		}

		if( $entity == 'post' ) {

			// In case there is no parent post (if it is 0)
			if( $object_or_id === 0 )
				return null;

			$post = get_post( $object_or_id ); // No matter if we pass the post ID or the object, get_post() takes both

			$object->id = $post->ID;
			$object->groupingType = strtoupper( str_replace( ' ', '', str_replace( '-', '', str_replace( '_', '', $post->post_type ) ) ) ); // Take the post type but remove spaces, - and _
			$object->startingDate = strtotime( $post->post_date );
			$object->variant = ''; // TODO: Where to get this information?

			$RepcoPostTranslations = new TRP_RepcoPostTranslations( $post );
			$object->title = $RepcoPostTranslations->get_field_translations( 'post_title' );
			$object->summary = $RepcoPostTranslations->get_field_translations( 'post_excerpt' );
			$object->description = $RepcoPostTranslations->get_field_translations( 'post_content' );
			$object->url = get_permalink( $post );

		}

		$object->groupingType = ''; // TODO: Where to get this from? Setting?

		return $object;

	}

}

?>