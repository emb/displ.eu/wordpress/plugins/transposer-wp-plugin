<?php

/**
 * Return all of the post's concepts including their translations, if available
 * Excludes everything that was either excluded by core or manually by the settings
 */
class TRP_RepcoConcepts {


	// Post ID
	private $_post_id = 0;

	// Post object
	private $_post = null;

	// The output array of RepcoConcept objects
	private $_terms = array();


	public function __construct( $post ) {
		$this->_post_id = $post->ID;
		$this->_post = $post;
		$this->get_concepts();
	}


	/**
	 * Add Concepts to a post
	 *	- Add term translations to the name key
	 * - Add all of a term's parents to the array
	 *
	 * Concepts are taxonomy terms that describe the contentItem or mediaAsset
	 * They should not contain authors/editors/contributors, languages, licenses and information that describes a contentGrouping
	 *
	 * @return array   Array of RepcoConcept objects
	 */
	public function get_concepts() {

		// Get all taxonomies available (e.g. ['category', 'post_tag', 'post_format']
		$taxonomies = get_object_taxonomies( $this->_post );

		// Exclude manually and preset exclusion taxonomies
		$exclude_core_taxos = TRP_Helper::get_exclude_taxonomies();
		$exclude_manual_taxos = get_option( 'transposer_exclude_taxonomies', array() );

		// ...and make sure the dedicated Contributor taxonomy is not part of the Concepts
		$exclude_contributor_taxos = get_option( 'transposer_contributor_taxonomies', array() );

		$taxonomies = array_diff( $taxonomies, $exclude_core_taxos, $exclude_manual_taxos, $exclude_contributor_taxos );
		$taxonomies = array_values( $taxonomies ); // Set continuous array keys, otherwise wp_get_post_terms gets in trouble

		// Get all the terms of the taxonomies left
		if( ! empty( $taxonomies ) ) {
			$this->_terms = wp_get_post_terms( $this->_post_id, $taxonomies );

			// Add all the term's parent(s) recursively to the array
			$this->set_parent_terms();
		}

		// Additional Eurozine Concepts taken from the postmeta
		if( TRP_Eurozine::is_eurozine() ) {

			$ez_term = new StdClass;
			$ez_term->id = 0;
			$ez_term->name = '';
			$ez_term->description = '';
			$ez_term->slug = '';
			$ez_term->parent = 0;

			// Article type ('essay'|'interview'|'review'|'news')
			$article_type = get_post_meta( $this->_post_id, 'article_type', true );

			if( ! empty( $article_type ) ) {
				$ez_term->name = ucwords( $article_type );
				$ez_term->taxonomy = 'article_type'; // Taxonomy becomes the Concept's kind
				$this->_terms[] = $ez_term;
			}

			// Issue (e.g. '4/2018'): in which issue was it published?
			$issue = get_post_meta( $this->_post_id, 'issue', true );

			if( ! empty( $issue ) ) {
				$ez_term->name = $issue;
				$ez_term->taxonomy = 'issue';
				$this->_terms[] = $ez_term;
			}

		}

		$filtered_terms = array();

		$i = 0;

		/**
		 * Add all the concepts (terms) in all languages as defined in the settings
		 *   - Return all translations including the original as the first one in the name key
		 *   - Return term translations dependent on the prefered plugin setting
		 */
		foreach( $this->_terms as $term ) {
			$filtered_terms[$i] = TRP_RepcoConcept::get_instance( $term );
			$i++;
		}

		return $filtered_terms;
	}


	/**
	 * Cycles each term and calls set_parent_term() which recursively adds each parent (of the parent's parent and so on...) to the $this->terms array
	 */
	private function set_parent_terms() {
		foreach( $this->_terms as $term ) {

			// Only set the parents if the taxonomy is hierarchical
			if( ! is_taxonomy_hierarchical( $term->taxonomy ) )
				continue;

			$this->set_parent_term( $term );
		}
	}


	/**
	 * Adds parents of a single term to the $this->terms array recursively
	 *
	 * @param    object    A WP_Term object
	 */
	private function set_parent_term( $term ) {
		if( $term->parent > 0 ) { // If it has a parent
			$parent = get_term( $term->parent ); // Get the parent
			$this->_terms[] = $parent; // Add it to the array
			$this->set_parent_term( $parent ); // Call ourself again to test for another parent
		}
	}

}

?>