<?php

class TRP_Admin {

	public function __construct() {



		// Display notice to admins if the cron jobs are disabled
		if( get_option( 'transposer_enable_cron' ) != '1' ) {
			add_action( 'admin_notices', array( 'TRP_Admin', 'admin_notice_cron_disabled' ) );
		}


		// If a post is saved, also update the original post's modification date to be ingested by Repco
		add_action( 'save_post', array( 'TRP_Admin', 'update_original_post_modified' ) );


		/**
		 * Add menus, scripts and columns to the backend interface
		 * TODO: Review this with cba and WPML
		 */
		//add_action( 'init', array( 'TRP_Admin', 'register_language_taxonomy' ) );

		$user_id = get_current_user_id();

		if( current_user_can( 'manage_options' ) ||
		    current_user_can( 'trp_manage_transposer' ) ||
		    current_user_can( 'trp_manage_transcripts' ) ||
		    current_user_can( 'trp_manage_translations' ) ||
		    in_array( $user_id, get_option( 'options_users_with_transposer_permission', array() ) )
		) {

			// Add scripts and styles to the backend
			add_action( 'admin_enqueue_scripts', array( 'TRP_Admin', 'enqueue_scripts' ) );

			// Add Transposer menu page
			add_action( 'admin_menu', array( 'TRP_Admin', 'add_menu' ) );

			/**
			 * JS related
			 */

			// Expose needed JS or markup for the UI
			add_action( 'admin_head', array( 'TRP_Admin', 'add_js' ) );

			// Create a <div> used for visual notifications
			add_action( 'admin_footer', array( 'TRP_Admin', 'notification_html' ) );

			// Add language selector markup to the footer
			add_action( 'admin_footer', array( 'TRP_Admin', 'language_selector_js' ) );

			// Add post translation editor markup for the jQuery dialog
			add_action( 'admin_footer', array( 'TRP_Admin', 'edit_post_translation_js' ) );

		}


		/**
		 * Add column options to manage posts
		 */
		if( current_user_can( 'manage_options' ) ||
		    current_user_can( 'trp_manage_transposer' ) ||
		    current_user_can( 'trp_manage_translations' ) ||
		    in_array( $user_id, get_option( 'options_users_with_transposer_permission', array() ) )
		) {


			$post_types = get_option( 'transposer_default_ui_post_types', array( 'post' ) );

			foreach( $post_types as $post_type ) {

				// Add a column header to the edit posts screen for translation options
				add_filter( 'manage_' . $post_type . '_posts_columns', array( 'TRP_Admin', 'posts_column_header' ) );

				// Display the generate-translations-button and progress bar beside a post in the edit posts screen
				add_action( 'manage_' . $post_type . '_posts_custom_column', array( 'TRP_Admin', 'posts_column_content' ), 1, 2 );

			}

		}


		/**
		 * Add column options to manage media/attachments
		 */
		if( current_user_can( 'manage_options' ) ||
		    current_user_can( 'trp_manage_transposer' ) ||
		    current_user_can( 'trp_manage_transcripts' ) ||
		    in_array( $user_id, get_option( 'options_users_with_transposer_permission', array() ) )
		) {

			// Add a column header to the edit media screen for translation options
			add_filter( 'manage_media_columns', array( 'TRP_Admin', 'media_column_header' ) );

			// Display the service selector and progress bar beside a post in the edit media screen
			add_action( 'manage_media_custom_column', array( 'TRP_Admin', 'media_column_content' ), 1, 2 );

		}


		add_action( 'admin_post_transposer-csv-export', array( 'TRP_Admin', 'export_csv' ) );


		/**
		 * AJAX handlers for calling via JS
		 */

		// Start a transposer service
		add_action( 'wp_ajax_trp_create_job', array( 'TRP_Admin', 'ajax_create_job' ) );

		// AJAX handler for saving a post translation
		add_action( 'wp_ajax_trp_retry_job', array( 'TRP_Admin', 'ajax_retry_job' ) );

		// Retrieve the status of a job
		add_action( 'wp_ajax_trp_get_job_status', array( 'TRP_Admin', 'ajax_get_job_status' ) );

		// Retrieve jobs of a given post (and service)
		add_action( 'wp_ajax_trp_get_jobs', array( 'TRP_Admin', 'ajax_get_jobs_by_post' ) );

		// Get the original post of a given post_id translation
		add_action( 'wp_ajax_trp_get_original_post', array( 'TRP_Admin', 'ajax_get_original_post' ) );

		// Retrieve translations of a post
		add_action( 'wp_ajax_trp_get_translations', array( 'TRP_Admin', 'ajax_get_translations' ) );

		// Retrieve the icons of all translations of a post
		add_action( 'wp_ajax_trp_get_translations_icons', array( 'TRP_Admin', 'ajax_get_translations_icons' ) );

		// Get a transcript by its ID
		add_action( 'wp_ajax_trp_get_transcript_by_id', array( 'TRP_Admin', 'ajax_get_transcript_by_id' ) );

		// Get a post translation by its ID
		add_action( 'wp_ajax_trp_get_translation', array( 'TRP_Admin', 'ajax_get_translation_by_id' ) );

		// Save a transcript
		add_action( 'wp_ajax_trp_save_transcript', array( 'TRP_Admin', 'ajax_save_transcript' ) );

		// Save a post translation
		add_action( 'wp_ajax_trp_save_translation', array( 'TRP_Admin', 'ajax_save_translation' ) );

		// Delete a transcript
		add_action( 'wp_ajax_trp_delete_transcript', array( 'TRP_Admin', 'ajax_delete_transcript' ) );
		add_action( 'wp_ajax_trp_editor_delete_transcript', array( 'TRP_Admin', 'ajax_delete_transcript' ) ); // Alias
		//add_action( 'wp_ajax_nopriv_trp_editor_delete_transcript', array( 'TRP_Admin', 'ajax_delete_transcript' ) ); // TODO: Remove after testing

		// Get the service selector
		add_action( 'wp_ajax_trp_get_service_selector', array( 'TRP_Admin', 'ajax_get_service_selector' ) );


		// Retrieve the HTML of a job status/the service selector to directly display
		add_action( 'wp_ajax_trp_display_job_status', array( 'TRP_Admin', 'ajax_display_job_status' ) );

		// Get a post's WebVTT file content for a given language
		add_action( 'wp_ajax_trp_get_webvtt_from_post', array( 'TRP_Admin', 'ajax_get_webvtt_from_post' ) );

		// Returns the JSON data to load the transcript editor
		add_action( 'wp_ajax_trp_editor_load_transcript', array( 'TRP_Admin', 'ajax_editor_load_transcript' ) );
		//add_action( 'wp_ajax_nopriv_trp_editor_load_transcript', array( 'TRP_Admin', 'ajax_editor_load_transcript' ) ); // TODO: Remove after testing

		add_action( 'wp_ajax_trp_editor_save_transcript', array( 'TRP_Admin', 'ajax_save_transcript' ) ); // Alias for trp_save_transcript
		//add_action( 'wp_ajax_nopriv_trp_editor_save_transcript', array( 'TRP_Admin', 'ajax_save_transcript' ) ); // TODO: Remove after testing

		add_action( 'wp_ajax_trp_editor_import_webvtt', array( 'TRP_Admin', 'ajax_import_webvtt' ) ); // Alias for trp_save_transcript
		//add_action( 'wp_ajax_nopriv_trp_editor_import_webvtt', array( 'TRP_Admin', 'ajax_import_webvtt' ) ); // TODO: Remove after testing

		// Returns the JSON data to load the cut/regions editor
		add_action( 'wp_ajax_trp_editor_load_regions', array( 'TRP_Admin', 'ajax_editor_load_regions' ) );
		//add_action( 'wp_ajax_nopriv_trp_editor_load_regions', array( 'TRP_Admin', 'ajax_editor_load_regions' ) ); // TODO: Remove after testing

		// Saves the regions for a media file and removes copyrighted parts if requested
		add_action( 'wp_ajax_trp_editor_save_regions', array( 'TRP_Admin', 'ajax_editor_save_regions' ) );
		//add_action( 'wp_ajax_nopriv_trp_editor_save_regions', array( 'TRP_Admin', 'ajax_editor_save_regions' ) ); // TODO: Remove after testing

	}


	/**
	 * Includes the needed JS and CSS for this plugin
	 */
	public static function enqueue_scripts() {

		wp_register_style( 'trp', plugins_url( '/css/style.css', __FILE__ ), array(), TRP_VERSION );
		wp_enqueue_style( 'trp' );

		wp_register_style( 'trp-ui-dialog', plugins_url( '/css/jquery-ui-dialog.min.css', __FILE__), array(), TRP_VERSION );
		wp_enqueue_style( 'trp-ui-dialog' );

		wp_register_style( 'trp-ui-menu', plugins_url( '/css/jquery-ui-menu.css', __FILE__), array(), TRP_VERSION );
		wp_enqueue_style( 'trp-ui-menu' );

		wp_enqueue_script( 'jquery-ui' );
		wp_enqueue_script( 'jquery-ui-mouse' );
		wp_enqueue_script( 'jquery-ui-widget' );
		wp_enqueue_script( 'jquery-ui-dialog' );
		wp_enqueue_script( 'jquery-ui-menu' );

		wp_register_style( 'trp-editor', plugins_url( '/js/editor/build/static/css/main.62ecc9bb.css', __FILE__), array(), TRP_VERSION );
		//wp_enqueue_style( 'trp-editor' );

		wp_register_script( 'trp-editor', plugins_url( '/js/editor/build/static/js/main.9f56b6c6.js', __FILE__ ), array('wp-element'), TRP_VERSION, true );
		//wp_enqueue_script( 'trp-editor' );

		wp_register_script( 'trp-editor-load', plugins_url( '/js/editor/build/static/js/load.js', __FILE__ ), array('trp-editor'), TRP_VERSION, true );
		//wp_enqueue_script( 'trp-editor-load' );

		wp_register_script( 'trp', plugins_url( '/js/javascript.js', __FILE__ ), array(), TRP_VERSION, false );
		wp_enqueue_script( 'trp' );

		/**
		 * Adding scripts and styles for transposer editors
		 * Source of approach: https://danielpost.com/part-1-creating-a-wordpress-options-page-using-react-and-gutenberg/
		 */

		// Enqueue TinyMCE functions
		wp_enqueue_editor();

		// Avoiding adding elements to uninvolved pages
		$page = get_current_screen();
		if ( 'toplevel_page_transposer-cut-editor' !== $page->id && 'toplevel_page_transposer-transcript-editor' !== $page->id  ) return;

		// Deregistering styles
		// We do this to avoid spill over of input styles
		wp_deregister_style(
			'wp-admin',
			'ie',
			'colors',
			'colors-fresh',
			'colors-classic',
			'media',
			'install',
			'thickbox',
			'farbtastic',
			'jcrop',
			'imgareaselect',
			'admin-bar',
			'wp-jquery-ui-dialog',
			'editor-buttons',
			'wp-pointer',
			'jquery-listfilterizer',
			'jquery-ui-smoothness',
			'tooltips',
			'cba-admin',
		);

		// Adding dependencies of cut editor
		if ( 'toplevel_page_transposer-cut-editor' === $page->id ) {
			// Including assets file
			$asset_file = include( plugin_dir_path( __FILE__ ) . 'js/editor/build/cutEditor.asset.php' );

			// Adding styles
			wp_register_style( 'transposer-cut-editor-styles', plugins_url( 'js/editor/build/cutEditor.css', __FILE__ ), array(), $asset_file['version'] );
			wp_enqueue_style( 'transposer-cut-editor-styles' );

			// Adding scripts
			wp_enqueue_script(
				'transposer-cut-editor-scripts',
				plugins_url( 'js/editor/build/cutEditor.js', __FILE__ ),
				$asset_file['dependencies'],
				$asset_file['version'],
				true
			);
		}

		// Adding dependencies of transcript editor
		if ( 'toplevel_page_transposer-transcript-editor' === $page->id ) {
			// Including assets file
			$asset_file = include( plugin_dir_path( __FILE__ ) . 'js/editor/build/transcriptEditor.asset.php' );

			// Adding styles
			wp_register_style( 'transposer-transcript-editor-styles', plugins_url( 'js/editor/build/transcriptEditor.css', __FILE__ ), array(), $asset_file['version'] );
			wp_enqueue_style( 'transposer-transcript-editor-styles' );

			// Adding scripts
			wp_enqueue_script(
				'transposer-transcript-editor-scripts',
				plugins_url( 'js/editor/build/transcriptEditor.js', __FILE__ ),
				$asset_file['dependencies'],
				$asset_file['version'],
				true
			);
		}

		// Adding wp components styling
		// While this is mostly unnecessary for the app itself, we could find use-cases to the gutenberg blocks
		// Source: https://wordpress.github.io/gutenberg/?path=/docs/components-introduction--docs
		wp_enqueue_style( 'wp-components' );
	}


	/**
	 * Adds the 'Transposer' menu page
	 */
	public static function add_menu() {
		add_menu_page( __( 'Transposer', 'trp' ), __( 'Transposer', 'trp' ), 'trp_manage_transposer', 'transposer', array( 'TRP_Admin', 'page_job_queue' ), '' );

		// Add a submenu "Mass translation"
		/*
		add_submenu_page(
			'transposer',
			esc_html__( 'Mass translation', 'trp' ),
			esc_html__( 'Mass translation', 'trp' ),
			'manage_options',
			'transposer-mass-translation',
			array( 'TRP_Admin', 'page_mass_translation' )
		);
		*/

		// Add a submenu "Statistics"
		add_submenu_page(
			'transposer',
			esc_html__( 'Statistics', 'trp' ),
			esc_html__( 'Statistics', 'trp' ),
			'manage_options',
			'transposer-statistics',
			array( 'TRP_Admin', 'page_statistics' )
		);

		add_menu_page( __( 'Transposer Cut Editor', 'trp' ), __( 'Transposer Cut Editor', 'trp' ), 'edit_posts', 'transposer-cut-editor', array( 'TRP_Admin', 'page_editor' ), '' );
		add_menu_page( __( 'Transposer Subtitle Editor', 'trp' ), __( 'Transposer Subtitle Editor', 'trp' ), 'edit_posts', 'transposer-transcript-editor', array( 'TRP_Admin', 'page_editor' ), '' );
	}


	public static function add_js() {

		$TRP = new TRP;

		// Only display this on needed screens
		//if( ! in_array( get_current_screen()->id, array( 'edit-post', 'post', 'edit-series', 'series', 'edit-station', 'station', 'edit-channel', 'channel', 'attachment', 'upload', 'toplevel_page_transposer', 'toplevel_page_cba-transcripts', 'toplevel_page_cba-reviews' ) ) )
		//	return;

		/**
		 * Add nonces for different actions
		 */
		?>
		<script>
		var trp_job_status_nonce = '<?php echo wp_create_nonce( 'trp_get_job_status' ); ?>';
		var trp_get_translations_nonce = '<?php echo wp_create_nonce( 'trp_get_translations' ); ?>';
		var trp_jobs_nonce = '<?php echo wp_create_nonce( 'trp_get_jobs' ); ?>';

		// Loader icon
		var trp_loader = '<?php echo TRP_Helper::get_status_icon( 'inprogress' ); ?>';

		/**
		 * Expose available Transposer services as JSON
		 * Used to display the service selector
		 */
		var trp_services = <?php echo json_encode( $TRP->get_services() ); ?>

		/**
		 * Expose available ASR languages for transcription as JSON in the header
		 * Used to display the service selector
		 */
		var trp_transcription_languages = <?php echo json_encode( TRP_Helper::get_transcription_languages() ); ?>

		/**
		 * Expose available languages for text-to-text translations as JSON in the header
		 * Used to display the language selector
		 */
		var trp_translation_languages = <?php echo json_encode( TRP_Helper::get_translation_languages() ); ?>

		/**
		 * Relative server path to the transcript editor
		 */
		var trp_transcript_editor_path = '<?php echo admin_url( 'admin.php?page=transposer-transcript-editor' ); ?>';

		/**
		 * Relative server path to the cut editor
		 */
		var trp_region_editor_path = '<?php echo admin_url( 'admin.php?page=transposer-cut-editor' ); ?>';


		/**
		 * i18n for the JS UI
		 */
		var trp_trsl= {};
		trp_trsl['queued'] = '<?php esc_attr_e( 'queued', 'trp' ); ?>';
		trp_trsl['inprogress'] = '<?php esc_attr_e( 'in progress', 'trp' ); ?>';
		trp_trsl['finished'] = '<?php esc_attr_e( 'finished', 'trp' ); ?>';
		trp_trsl['errors'] = '<?php esc_attr_e( 'errors', 'trp' ); ?>';
		trp_trsl['subtitles'] = '<?php esc_attr_e( 'Subtitles', 'trp' ); ?>';
		trp_trsl['translations'] = '<?php esc_attr_e( 'Translations', 'trp' ); ?>';
		trp_trsl['original'] = '<?php esc_attr_e( 'Original', 'trp' ); ?>';
		trp_trsl['author'] = '<?php esc_attr_e( 'Author', 'trp' ); ?>';
		trp_trsl['created'] = '<?php esc_attr_e( 'Created:', 'trp' ); ?>';
		trp_trsl['modified'] = '<?php esc_attr_e( 'Modified:', 'trp' ); ?>';
		trp_trsl['delete'] = '<?php esc_attr_e( 'Delete', 'trp' ); ?>';
		trp_trsl['language'] = '<?php esc_attr_e( 'Language', 'trp' ); ?>';
		trp_trsl['export'] = '<?php esc_attr_e( 'Export', 'trp' ); ?>';
		trp_trsl['import'] = '<?php esc_attr_e( 'Import', 'trp' ); ?>';
		trp_trsl['save'] = '<?php esc_attr_e( 'Save', 'trp' ); ?>';
		trp_trsl['saved'] = '<?php esc_attr_e( 'Changes saved.', 'trp' ); ?>';
		trp_trsl['edit'] = '<?php esc_attr_e( 'Edit', 'trp' ); ?>';
		trp_trsl['aswebvtt'] = '<?php esc_attr_e( 'as WebVTT', 'trp' ); ?>';
		trp_trsl['astext'] = '<?php esc_attr_e( 'as Text', 'trp' ); ?>';
		trp_trsl['playback'] = '<?php esc_attr_e( 'Playback', 'trp' ); ?>';
		trp_trsl['pause'] = '<?php esc_attr_e( 'Pause', 'trp' ); ?>';
		trp_trsl['rewind'] = '<?php esc_attr_e( 'Rewind', 'trp' ); ?>';
		trp_trsl['forward'] = '<?php esc_attr_e( 'Forward', 'trp' ); ?>';
		trp_trsl['zoomin'] = '<?php esc_attr_e( 'Zoom in', 'trp' ); ?>';
		trp_trsl['zoomout'] = '<?php esc_attr_e( 'Zoom out', 'trp' ); ?>';
		trp_trsl['dragtozoom'] = '<?php esc_attr_e( 'Drag to zoom', 'trp' ); ?>';
		trp_trsl['metadata'] = '<?php esc_attr_e( 'Metadata', 'trp' ); ?>';
		trp_trsl['filename'] = '<?php esc_attr_e( 'File name', 'trp' ); ?>';
		trp_trsl['license'] = '<?php esc_attr_e( 'License', 'trp' ); ?>';
		trp_trsl['pleasewait'] = '<?php esc_attr_e( 'Please wait. The file may take some time to load...', 'trp' ); ?>';
		trp_trsl['importwebvtt'] = '<?php esc_attr_e( 'Import WebVTT files', 'trp' ); ?>';
		trp_trsl['importing'] = '<?php esc_attr_e( 'Importing...', 'trp' ); ?>';
		trp_trsl['ok'] = '<?php esc_attr_e( 'OK', 'trp' ); ?>';
		trp_trsl['cancel'] = '<?php esc_attr_e( 'Cancel', 'trp' ); ?>';
		trp_trsl['removeallselections'] = '<?php esc_attr_e( 'Remove all selections', 'trp' ); ?>';
		trp_trsl['cut'] = '<?php esc_attr_e( 'Cut', 'trp' ); ?>';
		trp_trsl['thisselectionis'] = '<?php esc_attr_e( 'This selection is', 'trp' ); ?>';
		trp_trsl['spokenword'] = '<?php esc_attr_e( 'Spoken word', 'trp' ); ?>';
		trp_trsl['music'] = '<?php esc_attr_e( 'Music', 'trp' ); ?>';
		trp_trsl['title'] = '<?php esc_attr_e( 'Title', 'trp' ); ?>';
		trp_trsl['unknownauthor'] = '<?php esc_attr_e( 'Unknown author', 'trp' ); ?>';
		trp_trsl['unknowntitle'] = '<?php esc_attr_e( 'Unknown title', 'trp' ); ?>';
		trp_trsl['link'] = '<?php esc_attr_e( 'Link', 'trp' ); ?>';
		trp_trsl['remove'] = '<?php esc_attr_e( 'Remove', 'trp' ); ?>';
		trp_trsl['publicdomain'] = '<?php esc_attr_e( 'Public domain', 'trp' ); ?>';
		trp_trsl['fillgaps'] = '<?php esc_attr_e( 'Fill gaps', 'trp' ); ?>';
		trp_trsl['selecttype'] = '<?php esc_attr_e( 'Select a type to describe the gaps', 'trp' ); ?>';
		trp_trsl['nocutversion'] = '<?php esc_attr_e( 'No cut version available yet. Create selections and click on "Cut" to add an additional file without copyrighted material.', 'trp' ); ?>';
		</script>
		<?php
	}


	/**
	 * Language selector jQuery dialog
	 * Will be displayed if the generate-translations button was clicked
	 */
	public static function language_selector_js() {

		// Only display this on needed screens
		//if( ! in_array( get_current_screen()->id, array( 'edit-post', 'post', 'attachment',  'edit-series', 'series', 'edit-station', 'station', 'edit-channel', 'channel', 'upload', 'toplevel_page_transposer', 'toplevel_page_cba-transcripts', 'toplevel_page_cba-reviews' ) ) )
		//	return;

		?>
		<div id="trp-language-selector" title="<?php esc_attr_e( 'Auto-translate', 'trp' ); ?>" class="hidden">

			<form id="trp-language-selector-form">
				<fieldset>
					<?php

					$source_languages = TRP_Helper::get_all_languages();
					$target_languages = TRP_Helper::get_translation_languages();

					?>

					<div><?php _e( 'Translate from the original version', 'trp' ); ?>:</div>
					<h3 id="trp-original-post-caption"></h3>

					<label for="trp-source-language"><?php esc_html_e( 'Source language', 'trp' ); ?>:</label>
					<select name="trp_source_language" id="trp-source-language">
					<?php foreach( $source_languages as $code => $l ) { ?>
						<option value="<?php echo esc_attr( $code ); ?>"><?php esc_html_e( $l, 'trp' ); ?></option>
					<?php } ?>
					</select>

					<p class="validateTips"><?php esc_html_e( 'Select the languages to translate into:', 'trp' ); ?></p>

					<?php
					// TODO: Exclude languages that are already there
					foreach( $target_languages as $code => $l ) { ?>
						<div>
						<input type="checkbox" name="languages[]" id="trp-<?php echo esc_attr( $code ); ?>" value="<?php echo esc_attr( $code ); ?>" checked="checked" class="text ui-widget-content ui-corner-all" />
						<label for="trp-<?php echo esc_attr( $code ); ?>"><?php esc_html_e( $l, 'trp' ); ?></label>
						</div>
					<?php } ?>

					<!-- Allow form submission with keyboard without duplicating the dialog button -->
					<input type="submit" tabindex="-1" style="position:absolute; top:-1000px" />
				</fieldset>
			</form>

		</div>
		<?php

	}


	/**
	* Edit Post Translation form
	* Is displayed if the trp-edit-translation button was clicked
	*/
	public static function edit_post_translation_js() {

		// Only display this on needed screens
		//if( ! in_array( get_current_screen()->id, array( 'edit-post', 'post', 'edit-series', 'series', 'edit-station', 'station', 'edit-channel', 'channel', 'attachment', 'upload', 'toplevel_page_transposer', 'toplevel_page_cba-transcripts', 'toplevel_page_cba-reviews' ) ) )
		//	return;

		?>
		<div id="trp-edit-translation" title="<?php esc_attr_e( 'Edit translation', 'trp' ); ?>" class="hidden">
			<input type="text" name="trp_post_title" id="trp-post-title" size="100" value="" /><br>

			<label for="trp-post-subtitle"><?php _e( 'Subtitle', 'trp' ); ?></label><br>
			<input type="text" name="trp_post_subtitle" id="trp-post-subtitle" size="100" value="" /><br>

			<label for="trp-post-excerpt"><?php _e( 'Excerpt', 'trp' ); ?></label><br>
			<textarea name="trp_post_excerpt" id="trp-post-excerpt" rows="5" cols="60"></textarea><br>

			<label for="trp-post-content"><?php _e( 'Content', 'trp' ); ?></label><br>
			<textarea style="width:100%; height:300px;" name="trp_post_content" id="trp-post-content" rows="5" cols="60"></textarea>

			<?php
			//wp_editor( '', 'trp-post-content', array( 'tinymce' => true, 'textarea_name' => 'trp_post_content' ) );
			?>

			<?php
			// <textarea name="trp_post_content" id="trp-post-content" rows="50" cols="60"></textarea>
			?>

			<p><div class="trp-message"></div></p>
		</div>

		<?php

	}


	public static function notification_html() {
		echo '<div id="root"></div>';
		echo '<div id="trp-notification"></div>';

	?>
	<script>
	jQuery(document).ready(function() {
	});
   </script>
	<?php

	}


	/**
	 * The Transposer backend page
	 *
	 * TODO: Integrate WP_Table
	 */
	public static function page_job_queue() {
		global $wpdb;

		$TRP = new TRP;

		if( isset( $_GET['process_renderqueue'] ) ) {
			echo "Force processing render queue: <br>";
			$TRP->process_renderqueue();
		}

		if( isset( $_GET['process_responses'] ) ) {
			echo "Force processing responses: <br>";
			$TRP->process_responses();
		}

		echo '<div class="wrap">';
		echo '<h1>' . __( 'Transposer job queue', 'trp' ) . '</h1>';

		if( get_option( 'transposer_enable_cron' ) == '0' )
			echo '<a href="admin.php?page=transposer&status=queued&process_renderqueue" class="page-title-action">' . sprintf( __( 'Process next %s queued job(s)', 'trp' ), get_option( 'transposer_max_processes' ) ) . '</a>';

		/**
		 * Queries for a given status filter by the UI
		 */

		$selected_service = isset( $_GET['service'] ) && ! empty( $_GET['service'] ) ? $_GET['service'] : '';
		$selected_status = isset( $_GET['status'] ) && ! empty( $_GET['status'] ) ? $_GET['status'] : '';

		$add_where = ! empty( $selected_service ) ? " AND service='" . esc_sql( $selected_service ) . "'" : "";

		// Filter jobs by service
		$used_services = $wpdb->get_col( "SELECT DISTINCT(service) FROM " . $wpdb->prefix . "trp_transposer ORDER BY service ASC" );

		echo '<nav class="nav-tab-wrapper">';
		echo '<a class="nav-tab '; echo empty( $selected_service ) ? 'nav-tab-active' : ''; echo '" href="?page=transposer&status=' . $selected_status . '">' . __( 'All services', 'trp' ) . '</a>';

		foreach( $used_services as $svc ) {
			echo '<a class="nav-tab '; echo ( $selected_service == $svc ) ? 'nav-tab-active' : ''; echo '" href="?page=transposer&service=' . $svc . '&status=' . $selected_status . '">' . ucwords( $svc ) . '</a>';
		}

		echo '</nav>';

		// Order finished and errors descending (most recent on top) and queued and inprogress ascending (most recent at bottom)
		$order = ( isset( $selected_status ) && in_array( $selected_status, array( 'finished', 'error' ) ) ) ? 'DESC' : 'ASC';

		// If chosen via the UI: Filter attachments by a given _transcript_status
		if( isset( $selected_status ) && in_array( $selected_status, array( 'queued', 'inprogress', 'finished', 'api-finished', 'error' ) ) ) {
			$jobs = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix ."trp_transposer WHERE status LIKE '" . esc_sql( $selected_status ) . "%' " . $add_where . " ORDER BY id " . $order );
		// Or display everything else than 'finished' by default
		} else {
			$jobs = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "trp_transposer WHERE status != 'finished' " . $add_where . " ORDER BY id " . $order );
		}

		/**
		 * TODO: Make another query to order attachments by job_id
		 */


		/**
		 * Filter navigation
		 */
		echo '<div class="tabnav top">';
		echo '<div class="alignleft actions">';
		echo '<ul class="subsubsub"><li>' . __( 'Status', 'trp' ) . ': </li>	';
		echo '<li><a href="?page=transposer&service=' . $selected_service . '"'; echo empty( $selected_status ) ? ' class="current"' : ''; echo '>' . __( 'Queue & errors', 'trp' ) . '</a> | </li>';

		// Get all possible _transcript_status values
		$stati = $wpdb->get_col( "SELECT DISTINCT(status) FROM " . $wpdb->prefix . "trp_transposer WHERE status NOT LIKE 'error%'" . $add_where );

		foreach( $stati as $status ) {
			$aclass = ( $status == $selected_status ) ? ' class="current"' : '';
			echo '<li><a href="?page=transposer&service=' . $selected_service . '&status=' . $status . '" ' . $aclass .'>' . ucwords( $status ) . '</a> | </li>';
		}

		echo '<li><a href="?page=transposer&service=' . $selected_service . '&status=error"'; echo $selected_status == 'error' ? ' class="current"' : ''; echo '>' . __( 'Errors', 'trp' ) . '</a></li></ul>';


		$num_jobs = count( $jobs );
		echo '</div>';
		echo '<div class="alignright tablenav-pages">' . sprintf( __( '%s jobs found.', 'trp' ), $num_jobs ) . '</div>';
		echo '<br class="clear"></div>';


		/**
		 * List all (filtered) jobs in a table
		 */
		echo '<table class="widefat striped">';
		echo '<thead>';
		echo '<th class="manage-column" scope="col">' . __( 'ID', 'trp' ) . '</th>';
		echo '<th class="manage-column">' . __( 'Post ID', 'trp' ) . '</th>';
		echo '<th class="manage-column">' . __( 'Start - Stop', 'trp' ) . '</th>';

		if( empty( $selected_service ) )
			echo '<th class="manage-column">' . __( 'Service', 'trp' ) . '</th>';

		echo '<th class="manage-column">' . __( 'Context', 'trp' ) . '</th>';;
		echo '<th class="manage-column">' . __( 'Request parameters', 'trp' ) . '</th>';
		echo '<th class="manage-column">' . __( 'Response data', 'trp' ) . '</th>';
		echo '<th class="manage-column">' . __( 'Duration', 'trp' ) . '</th>';
		echo '<th class="manage-column">' . __( 'Status', 'trp' ) . '</th>';
		echo '</thead>';
		echo '<tbody>';

		$i = 1;

			foreach( $jobs as $job ) {

			$class = '';
			$duration = TRP_Helper::sec2min( strtotime( $job->stop ) - strtotime( $job->start ) );

			echo '<tr id="job-' . $job->id . '" class="' . $class . '">';
			echo '<td>' . $job->id . '</td>';
			echo '<td>' . $job->post_id . '</td>';
			echo '<td>';
				echo $job->start > 0 ? mysql2date( "d.m.y H:i", $job->start, true ) : ''; echo ' - ';
				echo $job->stop > 0 ? mysql2date( "H:i", $job->stop, true ) : '';
			echo '</td>';

			if( empty( $selected_service ) )
				echo '<td>' . $job->service . '</td>';

			echo '<td>' . esc_html( $job->context ) . '</td>';
			echo '<td>' . TRP_Helper::truncate_response( maybe_unserialize( $job->request ) ) . '</td>';
			echo '<td>' . TRP_Helper::truncate_response( maybe_unserialize( $job->response_data ) ) . '</td>';
			echo '<td>'; echo $duration > 0 ? $duration : ''; echo '</td>';
			echo '<td>' . TRP_Helper::get_status_icon( $job->status, $job->id ) . ' ' . esc_html( $job->status_message ) . '</td>';
			echo '</tr>';

			if( isset( $_GET['dev'] ) ) {
				echo '<tr>';
				echo '<td colspan="9">Request:<br>' . print_r( maybe_unserialize( $job->request ), true ) . '</td>';
				echo '</tr>';

				echo '<tr>';
				echo '<td colspan="9">Response:<br>' . print_r( maybe_unserialize( $job->response_data ), true ) . '</td>';
				echo '</tr>';
			}


		}

		echo '</tbody>';
		echo '</table>';

		echo '</div> <!-- /.wrap -->';

	}


	public static function page_mass_translation() {

		echo '<div class="wrap">';
		echo '<h1>' . __( 'Mass translation', 'trp' ) . '</h1>';

		$total_posts = 0;
		$total_jobs = 0;

		if( isset( $_POST['post_ids'] ) && ! empty( $_POST['post_ids'] ) ) {

			$TRP = new TRP;

			// All languages available for translation
			$translation_languages = array_keys( TRP_Helper::get_translation_languages() );

			$source_lang = TRP_Helper::get_original_language_codes( $post_id )[0];

			$post_ids = explode( ',', $_POST['post_ids'] );

			$total_posts = count( $post_ids );

			foreach( $post_ids as $post_id ) {

				// Skip if post id is not a number
				if( ! is_numeric( $post_id ) )
					continue;

				/* Check which translations are already there */

				// Subtract existing languages from the $translation_languages array to get the target_languages
				$translations = new TRP_RepcoPostTranslations( $post_id );
				$ex_langs = $translations->get_existing_languages();

				// Target languages: languages that are neither the source language nor the existing languages
				$target_langs = array_values( array_diff( $translation_languages, $ex_langs, array( $source_lang ) ) );

				// If target languages remained, create the jobs for the post
				if( ! empty( $target_langs ) ) {

					/* Create the job */

					$data['post_id'] = $post_id;
					$data['service_id'] = 'translate';
					$data['context'] = 'post_translation';
					$data['parameters']['language'] = $source_lang;
					$data['parameters']['languages'] = $target_langs;

					if( is_array( $TRP->create_job( $data ) ) )
						$total_jobs++;

				}

			}

			echo '<p>' . $total_jobs . '/' . $total_posts . ' posts sent to the translation queue.</p>';
			echo '<p><a href="' . admin_url( 'admin.php?page=transposer' ) . '">Go to job queue</a></p>';

		}

		echo '<p class="description">Insert Post IDs to translate, separated by comma. E.g. 1234,5678</p>';
		echo '<p>Will only create translations in missing languages and not overwrite existing ones.</p>';
		echo '<form action="' . admin_url( 'admin.php?page=transposer-mass-translation' ) . '" method="post">';
		echo '<textarea cols="60" rows="10" name="post_ids" id="post_ids" class="trp-mass-translate"></textarea><br>';
		echo '<input type="submit" value="' . esc_attr__( 'Translate', 'trp' ) . '" onclick="javascript:this.form.submit(); this.disabled=true;" />';
		echo '</form>';
		echo '</div>';
	}


	public static function page_statistics() {

		echo '<div class="wrap">';
		echo '<h1>' . __( 'Statistics', 'trp' ) . '</h1>';
		echo '<p>' . __( 'List and export published posts or transcripts that were translated using the Transposer plugin.', 'trp' ) . '</p>';

		global $wpdb;

		$service = isset( $_GET['service'] ) && in_array( $_GET['service'], array( 'translations', 'transcripts' ) ) ? $_GET['service'] : 'translations';
		$offset = isset( $_GET['offset'] ) && is_numeric( $_GET['offset'] ) ? $_GET['offset'] : 0;
		$limit = 20;

		if( $service == 'translations' ) {
			$results = $wpdb->get_results( "SELECT tr.post_id, p.post_title, COUNT(tr.id)+1 as num, p.post_date, p.post_modified FROM ". $wpdb->prefix ."trp_translations tr, " . $wpdb->posts . " p WHERE tr.post_id=p.ID AND p.post_status='publish' GROUP BY tr.post_id ORDER BY p.post_date DESC LIMIT " . $offset . "," . $limit );
			$total = $wpdb->get_col( "SELECT COUNT(tr.post_id) FROM ". $wpdb->prefix ."trp_translations tr, " . $wpdb->posts . " p WHERE tr.post_id=p.ID AND p.post_status='publish' GROUP BY tr.post_id" );
			$total = count( $total );
			$headline = __( 'Post translations', 'trp' );
		}

		if( $service == 'transcripts' ) {
			$results = $wpdb->get_results( "SELECT tr.post_id, p.post_title, COUNT(tr.id) as num, p.post_date, p.post_modified FROM ". $wpdb->prefix ."trp_transcripts tr, " . $wpdb->posts ." p WHERE tr.parent_id > 0 AND p.post_parent > 0 AND tr.post_id=p.ID GROUP BY tr.post_id ORDER BY p.post_date DESC LIMIT " . $offset . "," . $limit );
			$total  = $wpdb->get_col( "SELECT COUNT(tr.post_id) FROM ". $wpdb->prefix ."trp_transcripts tr, " . $wpdb->posts ." p WHERE tr.parent_id > 0 AND p.post_parent > 0 AND tr.post_id=p.ID GROUP BY tr.post_id" );
			$total = count( $total );
			$headline = __( 'Transcript translations', 'trp' );
		}

		echo '<nav class="nav-tab-wrapper">';
		echo '<a class="nav-tab '; echo empty( $service ) || $service == 'translations' ? 'nav-tab-active' : ''; echo '" href="?page=transposer-statistics&service=translations">' . __( 'Posts', 'trp' ) . '</a>';
		echo '<a class="nav-tab '; echo ( $service == 'transcripts' ) ? 'nav-tab-active' : ''; echo '" href="?page=transposer-statistics&service=transcripts">' . __( 'Transcripts', 'trp' ) . '</a>';
		echo '</nav>';


		/**
		 * List results
		 */

		echo '<h2>' . $headline . '</h2>';

		echo '<p>';
		printf( __( '%s items found.', 'trp' ), $total );
		echo '</p>';

		if( $total > 0 ) {

			$prev_offset = $offset - $limit;
			$prev_offset = $prev_offset < 0 ? 0 : $prev_offset;
			$next_offset = $offset + $limit;

			echo '<p>';

			if( $prev_offset > 0 )
				echo '<a href="?page=transposer-statistics&service=' . $service . '&offset=' . $prev_offset . '">Previous page</a> &middot; ';

			if( ( $total - $limit ) >= $next_offset )
				echo '<a href="?page=transposer-statistics&service=' . $service . '&offset=' . $next_offset . '">Next page</a>';

			echo '</p>';

			echo '<table class="widefat">';
			echo '<thead>';
			echo '<th>' . __( 'Title', 'trp' ) . '</th>';
			echo '<th>' . __( 'Date', 'trp' ) . '</th>';
			//echo '<th>' . __( 'Modified', 'trp' ) . '</th>';
			echo '<th>' . __( 'Languages', 'trp' ) . '</th>';
			echo '<th>' . __( 'Contributors', 'trp' ) . '</th>';
			echo '</thead>';
			echo '<tbody>';

			$contributor_taxos = get_option( 'transposer_contributor_taxonomies', array() );

			foreach( $results as $tr ) {

				$total_translations = $tr->num;

				if( $service == 'translations' ) {
					$PostTranslations = new TRP_RepcoPostTranslations( $tr->post_id );
					$total_translations = count( $PostTranslations->get_existing_languages() );
					$permalink = get_permalink( $tr->post_id );
				}

				if( $service == 'transcripts' ) {
					// Transcripts relate to an attachment
					// Get the proper title of the parent post
					$parent_post = get_post( $tr->post_id );
					$tr->post_title = $parent_post->post_title;
					$permalink = get_permalink( $parent_post->ID );
				}

				if( is_array( $contributor_taxos ) ) {
					$contributors = wp_get_object_terms( $tr->post_id, $contributor_taxos );
					$contributor_names = [];

					foreach( $contributors as $contributor ) {
						$contributor_names[] = ucwords( $contributor->name );
					}
				}

				echo '<tr>';
				echo '<td><a href="' . esc_url ( $permalink ) . '" target="_blank">' . $tr->post_title . '</a></td>';
				echo '<td>' . date( 'd.m.Y', strtotime( $tr->post_date ) ) . '</td>';
				//echo '<td>' . date( 'd.m.Y H:i:s', strtotime( $tr->post_modified ) ) . '</td>';
				echo '<td>' . $total_translations . '</td>';
				echo '<td>' . implode( ', ', $contributor_names ) . '</td>';
				echo '<td>';
				echo '</tr>';

			}

			echo '</tbody>';
			echo '</table>';

			echo '<p>' . __( 'The number of translations include translations by other plugins, but only counts one for each language. The original post is included in this number.', 'trp' ) . '</p>';
			echo '<p><a href="' . admin_url( 'admin-post.php?action=transposer-csv-export' ) . '" class="button" target="_blank">' . __( 'Export as CSV', 'trp' ) . '</a> (' . __( 'separated by semicolon', 'trp' ) . ')</p>';

		}

		echo '</div>';

	}


	/**
	 * Download translation and transcript statistics as a CSV file
	 * Delimiter is semicolon (;)
	 */
	public static function export_csv() {

		if( ! current_user_can( 'manage_options' ) )
			return;

		header('Content-Type: application/csv');
		header('Content-Disposition: attachment; filename=transposer_statistics.csv');
		header('Pragma: no-cache');

		global $wpdb;

		$translations = $wpdb->get_results( "SELECT tr.post_id, p.post_title, COUNT(tr.id)+1 as num, p.post_date FROM ". $wpdb->prefix ."trp_translations tr, " . $wpdb->posts . " p WHERE tr.post_id=p.ID AND p.post_status='publish' GROUP BY tr.post_id ORDER BY p.post_date DESC" );
		$transcripts = $wpdb->get_results( "SELECT tr.post_id, COUNT(tr.id) as num, p.post_date FROM ". $wpdb->prefix ."trp_transcripts tr, cba_posts p WHERE parent_id > 0 AND p.post_parent > 0 AND tr.post_id=p.ID GROUP BY tr.post_id ORDER BY p.post_date DESC" );

		$content = "Translations\r\n\r\n";
		$content .= "Original title;Languages;Published;Contributors;Permalink\r\n";

		$contributor_taxos = get_option( 'transposer_contributor_taxonomies' );

		$ltoreplace = array( ';', '"' );
		$lreplaceto = array( '', '' );


		foreach( $translations as $tr ) {

			if( is_array( $contributor_taxos ) ) {
				$contributors = wp_get_object_terms( $tr->post_id, $contributor_taxos );
				$contributor_names = [];

				foreach( $contributors as $contributor ) {
					$contributor_names[] = ucwords( $contributor->name );
				}
			}

			$content .= str_replace( $ltoreplace, $lreplaceto, $tr->post_title ) . ';' . $tr->num . ';' . date( 'd.m.Y', strtotime( $tr->post_date ) ) . ';' . implode( ', ', $contributor_names ) . ';' . get_permalink( $tr->post_id ) . "\r\n";
		}

		$content .= "\r\n\r\n";
		$content .= "Transcripts\r\n\r\n";
		$content .= "Original title;Languages;Published;Permalink\r\n";

		foreach( $transcripts as $tr ) {

			// Transcripts relate to an attachment
			// Get the proper title of the parent post
			$parent_post = get_post( get_post_parent( $tr->post_id ) );

			$content .= str_replace( $ltoreplace, $lreplaceto, $parent_post->post_title ) . ';' . $tr->num . ';' . date( 'd.m.Y', strtotime( $tr->post_date ) ) . ';' . get_permalink( $parent_post->ID ) . "\r\n";
		}

		echo $content;
		exit;

	}


	/**
	 * Adds a column header to the edit posts screen
	 */
	public static function posts_column_header( $cols ) {

		// If the option is empty, we don't display the column at all
		$post_types = get_option( 'transposer_default_ui_post_types' );

		if( ! is_array( $post_types ) || empty( $post_types ) )
			return $cols;

		// Add the transcript column header
		$cols["transposer"] = 'Transposer';

		return $cols;

	}


	/**
	 * Adds the row content for the edit posts table column
	 */
	public static function posts_column_content( $column_name, $post_id ) {

		if( $column_name != 'transposer' )
			return;

		if( ! in_array( get_post_type( $post_id ), get_option( 'transposer_default_ui_post_types', array( 'post' ) ) ) )
			return;

		$TRP = new TRP;

		// In case of WPML and if it's a post translation, we ask for the original post ID instead of the given one
		// Since we always translate from the original post, also the relation is
		if( TRP_WPML::is_wpml() ) {
			$post_id = TRP_WPML::get_original( $post_id );
		}

		echo '<div class="trp-wrap">';
		$TRP->display_job_status( $post_id );
		echo '</div>';

		?><script>jQuery(document).ready( function() { displayTRPJobStatus(<?php echo $post_id; ?>); });</script><?php

/*

		// The the post's main language code for pre-selecting source language
		$post_languages = get_the_terms( $post_id, 'language' );

		if( $post_languages )
			$main_language_code = get_term_meta( $post_languages[0]->term_id, 'language_code', true );
		else
			$main_language_code = 'de'; // TODO: From where to retrieve the default? Options?

		$jobs = $TRP->get_jobs_by_post( $post_id, 'translate' );
		$status = empty( $jobs ) ? 'finished' : 'inprogress';

		if( $column_name == 'transposer' ) {
			echo '<div id="trp-service-selector-container-' . esc_attr( $post_id ) . '" language="' . esc_attr( $main_language_code ) . '" status="' . $status . '">';
			$TRP->display_job_status( $post_id );
			echo '</div>';

			echo '<div class="trp-last-msg"></div>';

			?><script>jQuery(document).ready( function() { displayTRPJobStatus(<?php echo $post_id; ?>); });</script><?php
		}
*/
	}


	/**
	 * Adds a column header to the edit media screen
	 */
	public static function media_column_header( $cols ) {

		// If the option is empty, we don't display the column at all
		$post_types = get_option( 'transposer_default_ui_post_types' );

		if( ! is_array( $post_types ) || empty( $post_types ) )
			return $cols;

		// Add the transcript column header
		$cols["transposer"] = 'Transposer';

		return $cols;

	}


	/**
	 * Adds the row content for the edit media table column
	 */
	public static function media_column_content( $column_name, $post_id ) {

		if( $column_name != 'transposer' )
			return;

		$TRP = new TRP;

		$post = get_post( $post_id );

		// If the post is otherwise not an attachment, we won't display a selector
		if( $post->post_type != 'attachment' )
			return;

		// Exclude non audio-visual media (images, documents, etc.)
		$mime_type = $post->post_mime_type;
		if( ! stristr( $mime_type, 'audio' ) && ! stristr( $mime_type, 'video' ) )
			return;

		echo '<div class="trp-wrap">';
		echo $TRP->get_region_editor_icon( $post_id ); // Add the region editor icon for opening the region and cut editor

		$TRP->display_job_status( $post_id ); // Add the Transposer Selector Button and existing transcripts & translations

		echo '</div>';

		?><script>jQuery(document).ready( function() { displayTRPJobStatus(<?php echo $post_id; ?>); });</script><?php

	}



	/**
	 * AJAX handler for triggering a Transposer job
	 *
	 * TODO: Permission check
	 *
	 * Returns JSON: the Job ID on success or 0 on failure
	 */
	public static function ajax_create_job() {

		if( ! check_ajax_referer( 'trp_create_job' ) )
			self::json_response( __( 'Session timed out. Please reload the page to proceed.', 'trp' ), 408 );

		if( ! isset( $_POST['post_id'] ) || ! is_numeric( $_POST['post_id'] ) || $_POST['post_id'] < 1 )
			self::json_response( 'Invalid post ID', 400 ); // Don't translate error messages we use for debugging

		if( ! isset( $_POST['service_id'] ) || empty( $_POST['service_id'] ) )
			self::json_response( 'Invalid service ID', 400 ); // Don't translate error messages we use for debugging

		if( ! isset( $_POST['context'] ) || empty( $_POST['context'] ) )
			self::json_response( 'Invalid context', 400 ); // Don't translate error messages we use for debugging

		$postdata = array();
		$postdata['post_id'] = $_POST['post_id'];
		$postdata['service_id'] = $_POST['service_id'];
		$postdata['context'] = $_POST['context'];

		if( isset( $_POST['input'] ) && ! empty( $_POST['input'] ) )
			$postdata['input'] = $_POST['input'];

		if( isset( $_POST['transcript_id'] ) && is_numeric( $_POST['transcript_id'] ) && $_POST['transcript_id'] > 0 )
			$postdata['transcript_id'] = $_POST['transcript_id'];

		if( isset( $_POST['parameters'] ) && is_array( $_POST['parameters'] ) ) {
			foreach( $_POST['parameters'] as $key => $parameter ) {
				$postdata['parameters'][$key] = $parameter;
			}
		}

		$TRP = new TRP;

		// TODO: Make a common model for the JSON output
		if( ! $job_id = $TRP->create_job( $postdata ) )
			self::json_response( __( 'Transcript already exists', 'trp' ), 200 );

		self::json_response( __( 'Job created', 'trp' ), 201 );

	}


	/**
	 * AJAX handler for retrying a Transposer job: setting it to 'queued' again
	 *
	 * TODO: Permission check
	 *
	 * TODO:? Returns JSON: the Job ID on success or 0 on failure
	 */
	public static function ajax_retry_job() {

		if( ! check_ajax_referer( 'trp_retry_job' ) )
			self::json_response( __( 'Session timed out. Please reload the page to proceed.', 'trp' ), 403 );

		if( ! isset( $_POST['job_id'] ) || ! is_numeric( $_POST['job_id'] ) || $_POST['job_id'] < 1 )
			self::json_response( 'Invalid job ID', 400 );

		$TRP = new TRP;

		wp_send_json( $TRP->retry_job( $_POST['job_id'] ), 200 );

	}


	/**
	 * Returns the original post for another translation
	 * Only applies for WPML since Transposer doesn't represent translations as posts.
	 * For Transposer the given post will always be the original
	 */
	public static function ajax_get_original_post() {

		if( ! isset( $_GET['post_id'] ) && ! is_numeric( $_GET['post_id'] ) )
			self::json_response( 'Invalid post ID', 400 );

		$post_id = $_GET['post_id'];

		if( TRP_WPML::is_wpml() ) {
			$post_id = TRP_WPML::get_original( $post_id );
		}

		wp_send_json( get_post( $post_id, 200 ) );

	}


	public static function ajax_get_transcript_list() {
		global $wpdb;
		$transcripts = $wpdb->get_results( $wpdb->prepare( "SELECT id, model, duration, type, parent_id, date_created, date_modified, user_id FROM " . $wpdb->prefix ."trp_transcripts WHERE post_id = %d", (int) $post_id ) );
		wp_send_json( $transcripts, 200 );

	}

	/**
	 * AJAX handler for retrieving all jobs of a post
	 *
	 * TODO: Permission check
	 * TODO: Check if service is available
	 *
	 * Returns JSON: the Job ID on success or 0 on failure
	 */
	public static function ajax_get_jobs_by_post() {

		if( ! check_ajax_referer( 'trp_get_jobs' ) )
			self::json_response( __( 'Session timed out. Please reload the page to proceed.', 'trp' ), 403 );

		if( ! isset( $_GET['post_id'] ) || ! is_numeric( $_GET['post_id'] ) || $_GET['post_id'] < 1 )
			self::json_response( 'Invalid post ID', 400 ); // Don't translate error messages we use for debugging

		$data = array();
		$data['post_id'] = $_GET['post_id'];
		$data['service_id'] = ''; // If not set it will load all jobs

		if( isset( $_GET['service_id'] ) && ! empty( $_GET['service_id'] ) )
			$data['service_id'] = $_GET['service_id'];

		$TRP = new TRP;

		// Return the found jobs
		wp_send_json( $TRP->get_jobs_by_post( $data['post_id'], $data['service_id'] ), 200 );

	}


	/**
	 * AJAX handler for retrieving the translations of a post
	 *
	 * TODO: Permission check
	 */
	public static function ajax_get_translations() {

		if( ! check_ajax_referer( 'trp_get_translations' ) )
			self::json_response( __( 'Session timed out. Please reload the page to proceed.', 'trp' ), 403 );

		if( ! isset( $_GET['post_id'] ) || ! is_numeric( $_GET['post_id'] ) || $_GET['post_id'] < 1 )
			self::json_response( 'Invalid post ID', 403 ); // Don't translate error messages we use for debugging

		$TRP = new TRP;

		// Return the found jobs
		wp_send_json( $TRP->get_translations( $_GET['post_id'] ), 200 );

	}


	/**
	 * AJAX handler for retrieving the translations of a post
	 *
	 * TODO: Permission check
	 */
	public static function ajax_get_translation_by_id() {

		if( ! check_ajax_referer( 'trp_get_translation' ) )
			self::json_response( __( 'Session timed out. Please reload the page to proceed.', 'trp' ), 403 );

		if( ! isset( $_GET['translation_id'] ) || ! is_numeric( $_GET['translation_id'] ) || $_GET['translation_id'] < 1 )
			self::json_response( 'Invalid translation ID', 400 ); // Don't translate error messages we use for debugging

		$TRP = new TRP;

		// Return the found jobs
		wp_send_json( $TRP->get_translation( $_GET['translation_id'] ), 200 );

	}



	/**
	 * AJAX handler for retrieving the HTML markup for the translations icons of a post
	 */
	public static function ajax_get_translations_icons() {

		header( 'Content-type: text/html' );

		if( ! check_ajax_referer( 'trp_get_translations' ) )
			die( __( 'Session timed out. Please reload the page to proceed.', 'trp' ) );

		if( ! isset( $_GET['post_id'] ) || ! is_numeric( $_GET['post_id'] ) || $_GET['post_id'] < 1 )
			die( 'Invalid post ID' ); // Don't translate error messages we use for debugging

		$TRP = new TRP;

		// Return the found jobs
		die( $TRP->get_translations_icons( $_GET['post_id'] ) );

	}


	/**
	 * AJAX handler for deleting a transcript
	 *
	 * @param
	 * @return
	 */
	public static function ajax_delete_transcript() {

		//if( ! check_ajax_referer( 'trp_edit_transcript' ) )
		//	self::json_response( __( 'Session timed out. Please reload the page to proceed.', 'trp' ), 403 );

		if( ! isset( $_POST['transcript_id'] ) || ! is_numeric( $_POST['transcript_id'] ) )
			self::json_response( 'Invalid transcript ID', 400 ); // Don't translate error messages we use for debugging

		$transcript_id = $_POST['transcript_id'];

		$TRP = new TRP;

		// Permission check
		// Only admins or the transcript's author may delete a transcript
		$transcript = $TRP->get_transcript( $transcript_id );

		if( ! $TRP->user_can_edit_transcript( $transcript ) )
			self::json_response( __( 'Permission denied', 'trp' ), 403 );

		// Delete the WebVTT file if existing
		TRP_WebVTT::delete_webvtt_filepath( $transcript->post_id, $transcript->language );

		// Delete from database
		global $wpdb;
		$wpdb->query( $wpdb->prepare( "DELETE FROM " . $wpdb->prefix . "trp_transcripts WHERE id = %d", (int) $transcript_id ) );

		self::json_response( __( 'Transcript deleted', 'trp' ), 200 );

	}


	/**
	 * Returns a transcript as JSON by its ID
	 *
	 * TODO: Add nonce
	 *
	 * Call by:
	 * {
	 *    'action': 'cba_get_transcript_by_id',
	 *    'transcript_id': transcript_id
	 * }
	 */
	public static function ajax_get_transcript_by_id() {

		iF( ! isset( $_GET['transcript_id'] ) || ! is_numeric( $_GET['transcript_id'] ) )
			self::json_response( 'Invalid transcript ID', 400 ); // Don't translate error messages we use for debugging

		$TRP = new TRP;

		wp_send_json( $TRP->get_transcript_by_id( $_GET['transcript_id'] ), 200 );

	}


	/**
	 * Saves a transcript by its ID
	 */
	public static function ajax_save_transcript() {

		// Permission check
		//if( ! check_ajax_referer( 'trp_edit_transcript' ) )
		//	self::json_response( __( 'Session timed out. Please reload the page to proceed.', 'trp' ), 403 );

		// TODO: process $_POST['post_id']
		// If a transcript is saved but doesn't exist yet, (check for "post_id" and "language"), insert it instead of update it

		if( ! isset( $_POST['transcript_id'] ) || ! is_numeric( $_POST['transcript_id'] ) )
			self::json_response( 'Invalid transcript ID', 400 ); // Don't translate error messages we use for debugging

		if( ! isset( $_POST['language'] ) || ! in_array( $_POST['language'], array_keys( TRP_Helper::get_all_languages() ) ) )
			self::json_response( 'Invalid language', 400 ); // Don't translate error messages we use for debugging

		if( ! isset( $_POST['transcript'] ) || empty( $_POST['transcript'] ) )
			self::json_response( 'Invalid transcript', 400 ); // Don't translate error messages we use for debugging

		if( ! isset( $_POST['segments'] ) && ! empty( $segments ) )
			self::json_response( 'Invalid segments', 400 ); // Don't translate error messages we use for debugging

		$transcript_id = $_POST['transcript_id'];

		// Transcript is a URL-encoded string
		$transcript_text = json_decode( html_entity_decode( stripslashes( urldecode( $_POST['transcript'] ) ) ) );

		// Segments are JSON.stringified
		$segments = json_decode( stripslashes( html_entity_decode( stripslashes( $_POST['segments'] ) ) ) );

		$TRP = new TRP;

		// Only admins or the transcript's author may save a transcript
		$transcript = $TRP->get_transcript( $transcript_id );

		if( ! $TRP->user_can_edit_transcript( $transcript ) )
			self::json_response( __( 'Permission denied', 'trp' ), 403 );

		// $transcript->type = 'manual'; // TODO: When calling via AJAX it means a human started the request by clicking a button.
		// BUT: It doesn't necessarily mean, that is was reviewed. Better make a checkbox for that to be sure
		$transcript->language = $_POST['language'];
		$transcript->transcript = $transcript_text;
		$transcript->segments = $segments;

		if( isset( $_POST['author'] ) )
			$transcript->author = urldecode( $_POST['author'] );

		if( isset( $_POST['license'] ) )
			$transcript->license = urldecode( $_POST['license'] );

		// Get rid of keys we can't save
		if( isset( $transcript->language_string ) )
			unset( $transcript->language_string );

		if( isset( $transcript->subtitles ) )
			unset( $transcript->subtitles );

		// Save transcript in the database
		$TRP->save_transcript( $transcript );

		// When saved via AJAX, it means it was saved via the editor
		// In this case we also update the WebVTT file
		$webvtt_content = TRP_WebVTT::generate_webvtt( $transcript->segments );
		TRP_WebVTT::save_webvtt_to_file( $transcript->post_id, $transcript->language, $webvtt_content );

		self::json_response( __( 'Transcript saved.', 'trp' ), 200 );

	}


	/**
	 * Import new transcripts parsed from WebVTT (.vtt) files
	 */
	public static function ajax_import_webvtt() {

		$total = 0;
		$success = 0;

		// Permission check
		//if( ! check_ajax_referer( 'trp_edit_transcript' ) )
		//	self::json_response( __( 'Session timed out. Please reload the page to proceed.', 'trp' ), 403 );

		// Is there a post ID?
		if( ! isset( $_POST['post_id'] ) || ! is_numeric( $_POST['post_id'] ) )
			self::json_response( 'Invalid post ID', 400 ); // Don't translate error messages we use for debugging

		$post_id = $_POST['post_id'];

		// Check if the post is an attachment
		if( get_post_type( $post_id ) != 'attachment' )
			self::json_response( __( 'Attachment is not a media file', 'trp' ), 415 );

		// Does the user have the permissions to edit the given post?
		//if( ! current_user_can( 'edit_post', $post_id ) )
		//	self::json_response( __( 'Permission denied', 'trp' ), 403 );

		$transcripts = $_POST['transcripts'];
		$transcripts = json_decode( html_entity_decode( stripslashes( urldecode( $transcripts ) ) ) );

		// Are there transcripts to import?
		if( ! isset( $_POST['transcripts'] ) || empty ( $transcripts ) || ( ! is_object( $transcripts ) && ! is_array( $transcripts ) ) )
			self::json_response( __( 'No transcripts given', 'trp' ), 400 );

		$total = count( $transcripts );

		$parent_transcript = new StdClass;
		$parent_id = 0;

		// Check if there's a parent
		// If so, extract it and remove it from the original object
		foreach( $transcripts as $key => $tr ) {
			if( $tr->is_parent == 1 ) {
				$parent_transcript = $tr;
				unset( $transcripts[$key] );
				break; // There can only be one parent
			}
		}

		$TRP = new TRP;

		// Save the parent transcript and get its ID ($parent_id)
		if( ! empty( $parent_transcript ) ) {

			// Prepare the object for saving
			$parent_transcript = self::prepare_import_transcript( $parent_transcript, $post_id );

			// Save transcript in the database and get the parent_id
			if( ! $parent_id = $TRP->save_transcript( $parent_transcript ) )
				self::json_response( __( "Couldn't save parent transcript", 'trp' ), 400 );

			// Create the local WebVTT file
			$webvtt_content = TRP_WebVTT::generate_webvtt( $parent_transcript->segments );
			TRP_WebVTT::save_webvtt_to_file( $post_id, $parent_transcript->language, $webvtt_content );

			$success++;

			// Before inserting the others, make sure to rewrite existing transcripts with the new parent
			$transcripts_list = $TRP->get_transcripts_list( $post_id );
			$update_parent_ids = array(); // Existing IDs that need an update for the parent

			// Collect every transcript that has a parent
			foreach( $transcripts_list as $tr ) {
				if( $tr->parent_id > 0 )
					$update_parent_ids[] = $tr->id;
			}

			// Do the db update
			if( count( $update_parent_ids ) > 0 ) {
				global $wpdb;
				$wpdb->query( $wpdb->prepare( "UPDATE " . $wpdb->prefix . "trp_transcripts SET parent_id = %d WHERE id IN(" . implode( ',', $update_parent_ids ) . ")", (int) $parent_id ) );
			}

		}

		// Save all the other transcripts with the given parent
		// Check if everything is there, otherwise skip the file and try the next one
		foreach( $transcripts as $tr ) {

			$transcript = self::prepare_import_transcript( $tr, $post_id );
			$transcript->parent_id = $parent_id; // Will either be 0 or relate to the parent that was inserted before

			// Save transcript in the database
			if( ! $TRP->save_transcript( $transcript ) ) {
				continue;
			} else {
				// Create the local WebVTT file
				// If it can't be created
				$webvtt_content = TRP_WebVTT::generate_webvtt( $transcript->segments );
				TRP_WebVTT::save_webvtt_to_file( $post_id, $transcript->language, $webvtt_content );
			}

			$success++;

		}

		self::json_response( sprintf( __( '%s transcripts imported.', 'trp' ), $success ), 201 );

	}


	/**
	 * Checks if the given import object has all the needed data
	 * Puts everything
	 *
	 * @param    object          An import object containing keys 'language', 'transcript', 'segments'
	 * @param    int             A post ID of type 'attachment'
	 * @return   object|false    The transcript object for inserting or false if anything is missing
	 */
	public static function prepare_import_transcript( $transcript, $post_id = 0 )  {

		if( ! is_numeric( $post_id ) || $post_id < 1 )
			return false;

		// Is there a language?
		if( ! isset( $transcript->language ) || strlen( $transcript->language ) != 2 )
			return false;

		// Is the transcript as plain text there?
		if( ! isset( $transcript->transcript ) || empty( $transcript->transcript ) )
			return false;

		// Are there segments?
		if( ! isset( $transcript->segments ) || empty( $transcript->segments ) || ( ! is_object( $transcript->segments ) && ! is_array( $transcript->segments ) ) )
			return false;

		// Put together a clean return object that can be used for inserting
		$return = new StdClass;
		$return->post_id = $post_id;
		$return->type = 'manual'; // When importing we label it 'manual'
		$return->language = $transcript->language;
		$return->transcript = $transcript->transcript;
		$return->segments = $transcript->segments;

		return $return;

	}


	/**
	 * Saves a translation by its ID
	 */
	public static function ajax_save_translation() {

		// Permission check
		if( ! check_ajax_referer( 'trp_get_translation' ) )
			self::json_response( __( 'Session timed out. Please reload the page to proceed.', 'trp' ), 403 );

		if( ! isset( $_POST['translation_id'] ) || ! is_numeric( $_POST['translation_id'] ) )
			self::json_response( 'Invalid translation ID', 400 ); // Don't translate error messages we use for debugging

		$TRP = new TRP;

		// Only admins or the transcript's author may edit a translation
		$translation = $TRP->get_translation( $_POST['translation_id'] );

		$translation->type = 'manual';
		$translation->post_title = $_POST['post_title'];
		$translation->subtitle = $_POST['subtitle'];
		$translation->post_excerpt = $_POST['post_excerpt'];
		$translation->post_content = $_POST['post_content'];

		$TRP->save_translation( $translation );

		self::json_response( __( 'Translation saved', 'trp' ), 200 );

	}


	/**
	 * Saves a transcript by its ID
	 */
	public static function ajax_editor_save_regions() {

		// Permission check
		//if( ! check_ajax_referer( 'trp_edit_regions' ) )
		//	self::json_response( __( 'Session timed out. Please reload the page to proceed.', 'trp' ), 403 );

		if( ! isset( $_POST['post_id'] ) || ! is_numeric( $_POST['post_id'] ) )
			self::json_response( 'Invalid post ID', 400 ); // Don't translate error messages we use for debugging

		$post_id = $_POST['post_id'];

		// Permission check
		//if( ! current_user_can( 'manage_options' ) && ! current_user_can( 'edit_post', $post_id ) )
		//	self::json_response( __( 'Permission denied', 'trp' ), 403 );

		// Check if post exists
		if( ! get_post( $post_id ) )
			self::json_response( __( 'Post not found', 'trp' ), 404 );

		// Update post_title if not empty (has to have a value)
		$title = ! isset( $_POST['title'] ) || empty( $_POST['title'] ) ? '' : $_POST['title'];
		if( ! empty( $title ) ) {

			$title = html_entity_decode( stripslashes( urldecode( $title ) ) );

			$attachment = array(
				'ID' => $post_id,
				'post_title' => $title
			);

			wp_update_post( $attachment );
		}

		// Save license as post meta if not empty (has to have a value)
		$license = ! isset( $_POST['license'] ) || empty( $_POST['license'] ) ? '' : $_POST['license'];
		if( ! empty( $license ) ) {
			$license = html_entity_decode( stripslashes( urldecode( $license ) ) );
			update_post_meta( $post_id, 'license', $license );
		}


		/* Regions */

		// Only save them if set
		// value still can be empty
		if( isset( $_POST['regions'] ) ) {

			$new_regions = empty( $_POST['regions'] ) ? array() : $_POST['regions'];

			// Regions are JSON.stringified
			if( ! empty( $new_regions ) ) {
				$new_regions = json_decode( stripslashes( html_entity_decode( stripslashes( $new_regions ) ) ) );
			}

			// Save the given regions (also if empty)
			$regions = new TRP_Regions( $post_id );
			$regions->set_regions( $new_regions );

			// Cut the file, remove copyrighted parts and create a new file without them
			if( isset( $_POST['remove_copyright'] ) && ( $_POST['remove_copyright'] == 'true' || $_POST['remove_copyright'] == '1' ) ) {

				if( ! $regions->cut() )
					self::json_response( __( "Error cutting file", "trp" ), 400 );
				else
					self::json_response( __( "File successfully cut", "trp" ), 200 );

			}

		}

		self::json_response( __( 'Updates saved', 'trp' ), 200 );

	}


	/**
	 * DEPRECATED
	 *
	 * Gets the JSON for a posts's WebVTT file for a given language
	 * TODO: REMOVE
	 */
	public static function ajax_get_webvtt_from_post() {

		if( ! check_ajax_referer( 'trp_get_webvtt_from_post' ) )
			self::json_response( __( 'Session timed out. Please reload the page to proceed.', 'trp' ), 403 );

		if( ! isset( $_GET['post_id'] ) || ! is_numeric( $_GET['post_id'] ) )
			self::json_response( 'Invalid transcript ID', 400 ); // Don't translate error messages we use for debugging

		if( ! isset( $_GET['language'] ) || strlen( $_GET['language'] ) != 2 )
			self::json_response( __( 'Invalid language', 'trp' ), 400 );

		$post_id = $_GET['post_id'];
		$language = $_GET['language'];

		// Only admins or the transcript's author may delete a transcript
		//$transcript = $TRP->get_transcript( $transcript_id );
		//if( ! current_user_can( 'manage_options' ) && $transcript->user_id != get_current_user_id() )
		//	wp_send_json( array( 'message' => __( 'Permission denied.', 'trp' ) ), 403 );

		$TRP = new TRP;

		// Try to get the content from the webvtt file
		// If false, try to create it and retrieve it again
		if( ! $webvtt = TRP_WebVTT::get_webvtt_from_file( $post_id, $language ) ) {
			$transcripts = $TRP->get_transcripts( $post_id, $language, false );

			if( count( $transcripts ) < 1 )
				self::json_response( __( 'No transcript found', 'trp' ), 404 );

			foreach( $transcripts as $transcript ) {
				if( TRP_WebVTT::save_webvtt_to_file( $post_id, $language, TRP_WebVTT::generate_webvtt( $transcript->segments ) ) )
					$webvtt = TRP_WebVTT::get_webvtt_from_file( $post_id, $language );
				else
					self::json_response( __( 'WebVTT subtitles not retrievable', 'trp' ), 404 );

				break;
			}

		}

		wp_send_json( $webvtt, 200 );

	}


	/**
	 * AJAX handler for getting the job status
	 * Returns the status as JSON
	 *
	 * Can be called by POST request and action 'ajax_get_job_status'
	 * Is called by function display_job_status()
	 */
	public static function ajax_get_job_status() {

		if( ! check_ajax_referer( 'trp_get_job_status' ) )
			wp_send_json( array( 'message' => __( 'Session timed out. Please reload the page to proceed.', 'trp' ) ), 403 );

		// There wasn't a proper job ID submitted
		// Without it we can't go any further
		if( ! isset( $_POST['job_id'] ) || ! is_numeric( $_POST['job_id'] ) || $_POST['job_id'] < 1 )
			wp_send_json( array( 'message' => 'Invalid job ID.' ), 403 ); // Don't translate error messages we use for debugging

		$job_id = $_POST['job_id'];

		$TRP = new TRP;

		// Retrieve the job status from the Transposer API
		$status = $TRP->get_job_status( $job_id );

		wp_send_json( $status, 200 );

	}


	/**
	 * Returns HTML Markup for directly outputing the status
	 */
	public static function ajax_display_job_status() {

		header("Content-type: text/html");

		if( ! check_ajax_referer( 'trp_display_job_status' ) )
			die( __( 'Session timed out. Please reload the page to proceed.', 'trp' ) );

		// There wasn't a proper post ID submitted
		if( ! isset( $_GET['post_id'] ) || ! is_numeric( $_GET['post_id'] ) || $_GET['post_id'] < 1 )
			die( 'Invalid post ID' ); // Don't translate error messages we use for debugging

		$TRP = new TRP;

		die( $TRP->display_job_status( $_GET['post_id'] ) );

	}


	/**
	 * Returns the JSON needed to load the transcript editor
	 */
	public static function ajax_editor_load_regions() {

		//if( ! check_ajax_referer( 'trp_get_attachment' ) )
		//	$this->json_response( __( 'Session timed out. Please reload the page to proceed.', 'trp' ), 403 );

		if( ! isset( $_GET['post_id'] ) || ! is_numeric( $_GET['post_id'] ) )
			self::json_response( 'Invalid post ID', 400 ); // Don't translate error messages we use for debugging

		// The transcript ID to edit
		$attachment_id = $_GET['post_id'];

		// Test if media file exists
		if( ! $attachment = get_post( $attachment_id ) )
			self::json_response( 'Media file not found', 404 ); // Don't translate error messages we use for debugging

		// Is it an audio we can cut?
		if( ! in_array( $attachment->post_mime_type, array( 'audio/mpeg', 'audio/mp3', 'audio/ogg' ) ) )
			self::json_response( 'Mime type not supported', 400 ); // Don't translate error messages we use for debugging

		// Permission check
		//if( ! current_user_can( 'manage_options' ) && ! current_user_can( 'edit_post', $attachment_id ) )
		//	$this->json_response( __( 'Permission denied', 'trp' ), 403 );

		$media = array(); // The output array

		// Add the media file's data to the output
		$media['file'] = new StdClass;
		$media['file']->id = $attachment->ID;
		$media['file']->title = $attachment->post_title;
		$media['file']->excerpt = $attachment->post_excerpt;
		$media['file']->content = $attachment->post_content;
		$media['file']->user_id = $attachment->post_author;
		$media['file']->date_created = $attachment->post_date;
		$media['file']->date_modified = $attachment->post_modified;

		// URL to the original media file
		$media['file']->url = wp_get_attachment_url( $attachment->ID );

		// Relative filepath starting from /wp-content/uploads/
		$attached_file = get_post_meta( $attachment->ID, '_wp_attached_file', true );
		$pathinfo = pathinfo( $attached_file );

		// If there's a downsampled preview version, take that one since it's faster
		$filename_preview = $pathinfo['dirname'] . '/' . $pathinfo['filename'] . '_preview.' . $pathinfo['extension'];

		if( @file_exists( wp_upload_dir()['basedir'] . '/' . $filename_preview ) )
			$media['file']->url = wp_upload_dir()['baseurl'] . '/' . $filename_preview;

		// URL to the cut file: not existing by default
		$media['file']->cut_url = '';

		// Filename for the cut version
		$filename_cut = $pathinfo['dirname'] . '/' . $pathinfo['filename'] . '_cut.' . $pathinfo['extension'];

		// If there's already a cut file, include it
		if( @file_exists( wp_upload_dir()['basedir'] . '/' . $filename_cut ) )
			$media['file']->cut_url = wp_upload_dir()['baseurl'] . '/' . $filename_cut;

		// Add attachment's metadata
		$metadata = wp_get_attachment_metadata( $attachment->ID );
		$media['file']->fileformat = $metadata['fileformat'];
		$media['file']->bitrate = $metadata['bitrate'];
		$media['file']->mime_type = $metadata['mime_type'];
		$media['file']->length = $metadata['length'];
		$media['file']->length_formatted = $metadata['length_formatted'];
		$media['file']->license = get_post_meta( $attachment->ID, 'license', true );

		$regions = new TRP_Regions( $attachment->ID );
		$media['regions'] = $regions->regions;

		$media['nonce'] = wp_create_nonce( 'trp_edit_regions' );

		wp_send_json( $media, 200 );

	}


	/**
	 * Returns the JSON needed to load the transcript editor
	 */
	public static function ajax_editor_load_transcript() {

		//if( ! check_ajax_referer( 'trp_get_transcript' ) )
		//	$this->json_response( __( 'Session timed out. Please reload the page to proceed.', 'trp' ), 403 );

		if( ! isset( $_GET['transcript_id'] ) || ! is_numeric( $_GET['transcript_id'] ) )
			self::json_response( 'Invalid transcript ID', 400 ); // Don't translate error messages we use for debugging

		// The transcript ID to edit
		$transcript_id = $_GET['transcript_id'];

		$TRP = new TRP;

		// If not existing, return an error
		if( ! $transcript = $TRP->get_transcript( $transcript_id ) )
			self::json_response( __( 'Transcript not found', 'trp' ), 404 );

		// Permission check
		//if( ! current_user_can( 'manage_options' ) && $transcript->user_id != get_current_user_id() )
		//	$this->json_response( __( 'Permission denied', 'trp' ), 403 );

		// Test if media file exists
		if( ! $attachment = get_post( $transcript->post_id ) )
			self::json_response( __( 'Media file not found', 'trp' ), 404 );

		$transcripts = array(); // The output array

		// Add the media file's data to the output
		$transcripts['file'] = new StdClass;
		$transcripts['file']->id = $attachment->ID;
		$transcripts['file']->title = $attachment->post_title;
		$transcripts['file']->excerpt = $attachment->post_excerpt;
		$transcripts['file']->content = $attachment->post_content;
		$transcripts['file']->user_id = $attachment->post_author;
		$transcripts['file']->date_created = $attachment->post_date;
		$transcripts['file']->date_modified = $attachment->post_modified;

		// URL to the original media file
		$transcripts['file']->url = wp_get_attachment_url( $attachment->ID );

		// URL to the cut file: not existing by default
		$transcripts['file']->cut_url = '';

		$pathinfo = pathinfo( $transcripts['file']->url );
		$filename_cut = $pathinfo['dirname'] . '/' . $pathinfo['filename'] . '_cut.' . $pathinfo['extension'];

		// If there's already a cut file, include it
		if( @file_exists( wp_upload_dir()['basedir'] . '/' . $filename_cut ) )
			$transcripts['file']->cut_url = wp_upload_dir()['baseurl'] . '/' . $filename_cut;

		// Add attachment's metadata
		$metadata = wp_get_attachment_metadata( $attachment->ID );
		$transcripts['file']->fileformat = $metadata['fileformat'];
		$transcripts['file']->bitrate = $metadata['bitrate'];
		$transcripts['file']->mime_type = $metadata['mime_type'];
		$transcripts['file']->length = $metadata['length'];
		$transcripts['file']->length_formatted = $metadata['length_formatted'];
		$transcripts['file']->license = get_post_meta( $attachment->ID, 'license', true );

		// Add the requested transcript object
		unset( $transcript->transcript ); // Remove full text
		unset( $transcript->duration ); // Remove duration
		$transcripts['transcript'] = $transcript;

		// Add the list of existing transcripts
		$transcripts['transcripts'] = $TRP->get_transcripts( $transcript->post_id );

		// Remove the plain text, we don't need it here, just slows everything down
		foreach( $transcripts['transcripts'] as $key => $tr ) {
			if( isset( $transcripts['transcripts'][$key]->transcript ) )
			 	unset( $transcripts['transcripts'][$key]->transcript );
		}

		$transcripts['nonce'] = wp_create_nonce( 'trp_edit_transcript' );

		wp_send_json( $transcripts, 200 );

	}


	public static function ajax_get_service_selector() {

		header( 'Content-type: text/html' );

		if( ! check_ajax_referer( 'trp_create_job' ) )
			die( __( 'Session timed out. Please reload the page to proceed.', 'trp' ) );

		if( ! isset( $_GET['post_id'] ) || ! is_numeric( $_GET['post_id'] ) )
			die( 'Invalid post ID.' ); // Don't translate error messages we use for debugging

		$post_id = $_GET['post_id'];
		$post_type = get_post_type( $post_id );

		$transcription_languages = TRP_Helper::get_transcription_languages();

		$TRP = new TRP;

		$transcripts = $TRP->get_transcripts_list( $post_id );
		$create_transcript_class = empty( $transcripts ) ? '' : 'ui-state-disabled';


		/**
		 * Try to get the original language code
		 */
		$main_language_code = '';

		// Try WPML
		if( TRP_WPML::is_wpml() ) {
			$main_language_code = TRP_WPML::get_original_language_code( $post_id );
		}

		// Try Eurozine
		if( empty( $main_language_code ) && TRP_Eurozine::is_eurozine() ) {
			$main_language_code = get_post_meta( $post_id, 'origin_lang', true ); // The language code is in a postmeta
		}

		// Try to get it from the language taxonomy
		if( empty( $main_language_code ) && taxonomy_exists( 'language' ) ) {
			$post_languages = get_the_terms( $post_id, 'language' );

			if( $post_languages )
				$main_language_code = get_term_meta( $post_languages[0]->term_id, 'language_code', true );
		}

		// If it's still empty, try to get it from the original transcript
		if( empty( $main_language_code ) ) {

			// Get the orignal transcript's language
			foreach( $transcripts as $transcript ) {
				if( $transcript->parent_id == 0 ) {
					$original_transcript_language_code = $transcript->language; // The code like 'en'
					$original_transcript_language = $transcription_languages[$transcript->language]; // The string like 'English'
					break;
				}
			}

		}

		// If it's still empty just set the fallback as default
		if( empty( $main_language_code ) )
			$main_language_code = get_option( 'transposer_fallback_language' );

		if( empty( $original_transcript_language_code ) )
			$original_transcript_language_code = $main_language_code;

		// Put the found (most likely) language on top of the language list
		$original_language[$main_language_code] = $transcription_languages[$main_language_code];
		unset( $transcription_languages[$main_language_code] ); // ...and remove it from the list
		$transcription_languages = array_merge( $original_language, $transcription_languages );

		?>

		<ul class="trp-menu ui-menu" post_id="<?php esc_attr_e( $post_id ); ?>" nonce="<?php echo wp_create_nonce( 'trp_create_job' ); ?>">

		<?php
		/**
		 * Service selector options for audio/video files
		 */
		if( $post_type == 'attachment' ) : ?>

			<li class="trp-edit-regions" post_id="<?php echo $post_id; ?>" nonce="<?php echo wp_create_nonce( 'trp_edit_regions' ); ?>"><div><?php esc_html_e( 'Edit chapters & cut...', 'trp' ); ?></div></li>

			<?php if( empty( $transcripts ) ) : ?>
				<?php if( defined( 'IS_CBA' ) && IS_CBA ) : ?>
					<!-- for cba this process is separated for now, so nothing to display here -->
				<?php else: ?>
					<li class="trp-create-transcript" language="<?php esc_attr_e( $code ); ?>"><div><?php _e( 'Generate transcript', 'trp' ); ?></div>
						<ul>
						<?php foreach( $transcription_languages as $code => $lang ) { ?>
							<li class="trp-create-transcript" language="<?php esc_attr_e( $code ); ?>"><div><?php esc_html_e( $lang ); ?></div></li>
						<?php } ?>
						</ul>
					</li>

				<?php endif; ?>
			<?php else : ?>
					<!-- if there are transcripts present, we currently don't give the option to regenerate it -->
			<?php endif; ?>

			<?php if( empty( $transcripts ) ) : ?>
				<li class="ui-state-disabled"><div><?php _e( 'Auto-translate transcript...', 'trp' ); ?></div></li>
			<?php else : ?>
				<li class="trp-create-transcript-translations" language="<?php esc_attr_e( $original_transcript_language_code ); ?>"><div><?php _e( 'Auto-translate transcript...', 'trp' ); ?></div></li>
			<?php endif; ?>
				<!--<li class="trp-detect-music"><div><?php _e( 'Detect music/speech', 'trp' ); ?></div></li>-->

		<?php endif; ?>


		<?php
		/**
		 * Service selector options for posts
		 */
		// TODO: Add a filter to make it cba independent
		if( in_array( $post_type, get_option( 'transposer_default_ui_post_types', array( 'post' ) ) ) ) : ?>
			<li class="trp-create-translations" language="<?php esc_attr_e( $main_language_code ); ?>"><div><?php _e( 'Auto-translate...', 'trp' ); ?></div></li>

			<?php

			// TODO: Add a function to create blank translations later
			if( current_user_can( 'trp_manage_translations' ) || current_user_can( 'trp_manage_transposer' ) || current_user_can( 'manage_options' ) ) : ?>
				<!--<li class="trp-create-translation"><div><?php _e( 'Create translation', 'trp' ); ?></div></li>-->
			<?php endif; ?>

		<?php endif; ?>

		</ul>

		<script>
			jQuery( function() {
				jQuery( ".trp-menu" ).menu();
			} );
		</script>

		<?php

		die();

	}


	/**
	 * Sends the JSON response object and dies
    * HTTP status codes: https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
	 *
	 * @param     string     The message to display
	 * @param     int        HTTP status code
	 * @return    void
	 */
	public static function json_response( $msg = '', $status_code = 200 ) {

		$success = array(
			200 => true,  // OK
			201 => true,  // Created
			202 => true,  // Accepted
			400 => false, // Bad Request
			401 => false, // Unauthorized
			402 => false, // Payment Required
			403 => false, // Forbidden
			404 => false, // Not found
			408 => false, // Request Timeout
			409 => false, // Conflict
			415 => false  // Unsupported Media Type
		);

		$response = array( 'success' => $success[$status_code], 'message' => $msg );

		wp_send_json( $response, $status_code );

	}


	/**
	 * On WPML instances we need to update the original post's modification date as well
	 * Repco will otherwise not ingest the update since a translation is part of the original post in the Repco API endpoint
	 *
	 * @param    int    The Post ID to update
	 */
	public static function update_original_post_modified( $post_id ) {

		if( ! TRP_WPML::is_wpml() )
			return;

		$original_post_id = TRP_WPML::get_original( $post_id );

		TRP::update_modification_date( $original_post_id );

	}


	/**
	 * Registers taxonomy 'language' or extends existing taxonomy for post types 'post' and 'attachment'
	 * Adds the language terms to the taxonomy if not existing and adds language codes as term meta
	 *
	 * TODO: Merge that with WPMLs language table
	 */
	public function register_language_taxonomy() {

		// Register taxonomy 'language' or extend
		if( taxonomy_exists( 'language' ) ) {

			// Make the already registered taxonomy available for posts and attachments
			register_taxonomy_for_object_type( 'language', 'post' );
			register_taxonomy_for_object_type( 'language', 'attachment' );

		} else {

			// Add new taxonomy, make it hierarchical (like categories)
			$labels = array(
				'name'              => _x( 'Languages', 'taxonomy general name', 'trp' ),
				'singular_name'     => _x( 'Language', 'taxonomy singular name', 'trp' ),
				'search_items'      => __( 'Search for languages', 'trp' ),
				'all_items'         => __( 'All languages', 'trp' ),
				'parent_item'       => __( 'Parent language:', 'trp' ),
				'parent_item_colon' => __( 'Parent languages:', 'trp' ),
				'edit_item'         => __( 'Edit language', 'trp' ),
				'update_item'       => __( 'Update language', 'trp' ),
				'add_new_item'      => __( 'Add new language', 'trp' ),
				'new_item_name'     => __( 'New language', 'trp' ),
				'menu_name'         => __( 'Language', 'trp' ),
			);

			$args = array(
				'hierarchical'      => true,
				'labels'            => $labels,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'language' ),
			);

			register_taxonomy( 'language', array( 'post', 'attachment' ), $args );

		}

		// Add the taxonomy and add languages if it hasn't been done already
		if( get_option( 'trp_added_language_terms' ) != 1 ) {

			$languages = TRP_Helper::get_all_languages();

			foreach( $languages as $code => $lang ) {

				// If the language doesn't exist, add it
				$term = get_term_by( 'slug', sanitize_title( $lang ), 'language' );

				if( ! $term || is_wp_error( $term ) ) {

					// Insert the term
					$term = wp_insert_term( $lang, 'language' );

					// Add the language code (e.g. 'en') as term meta
					add_term_meta( $term['term_id'], 'language_code', $code );
				} else {

					// If the language exists but not the meta, make sure to still save the language code
					if( empty( get_term_meta( $term->term_id, 'language_code', true ) ) )
						add_term_meta( $term->term_id, 'language_code', $code );

				}

			}

			// Save that we did the job in order not to repeat it
			update_option( 'trp_added_language_terms', 1 );

		}

	}


	public static function admin_notice_cron_disabled() {
	?>
		<div class="notice notice-warning is-dismissible">
			<p><?php _e( "The Transposer job queue is currently disabled. New jobs will be saved but not processed until it is switched on again.", 'trp' ); ?></p>
		</div>
	<?php
	}


	/**
	 * Clear the cron from the schedule after deactivating the plugin
	 */
	public static function cron_deactivate() {
		wp_clear_scheduled_hook( 'trp_cron' );
	}

	/**
	 * Function to initialize admin page for transposer editors
	 */
	public static function page_editor() {
		echo '<style> #wpadminbar { display: none; } </style>';
		echo '<div id="transposer-app" style="background-color: #1d1b20; bottom: 0; left: 0; overflow: hidden; position: fixed; right: 0; top: 0; z-index: 999;"></div>';
	}
}

?>