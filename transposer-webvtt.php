<?php

class TRP_WebVTT {

	public function __construct() {

	}

	/**
	 * Get an attachment's URL of its WebVTT subtitles file
	 * Will create the file if not existing, otherwise just returns its URL
	 *
	 * @param     int       A post ID of type 'attachment'
	 * @param     int       2 character language code
	 * @param     int       A post ID of type 'attachment'
	 * @return    string    The .vtt file's URL
	 */
	public static function get_webvtt_url( $attachment_id, $language = '' ) {

		$url = '';

		if( empty( $language ) )
			$language = get_option( 'transposer_fallback_language', 'en' );

		// Check if the file exists
		$webvtt_filepath = self::get_webvtt_filepath( $attachment_id, $language );

		// Create the WebVTT file if it doesn't exist
		// Avoid creating files for a certain cba instance
		if( ! @file_exists( $webvtt_filepath ) && ( ! defined( 'CBA_IS_ARBEIT' ) || ! CBA_IS_ARBEIT ) ) {

			// Get the subtitles
			// TODO: Remove redundancy by denesting get_transcript() off the parent class and implement it in an OOP-decent way
			global $wpdb;

			$segments = $wpdb->get_var( $wpdb->prepare( "SELECT segments FROM " . $wpdb->prefix . "trp_transcripts WHERE post_id = %d AND language = %s", (int) $attachment_id, $language ) );

			if( empty( $segments ) )
				return $url;

			if( ! TRP_Helper::write_to_file( $webvtt_filepath, self::generate_webvtt( json_decode( $segments ) ) ) )
				return $url;

		}

		// Replace the basedir (e.g. '/var/www/wp-content/uploads/') with the baseurl (e.g. 'https://cba.media/wp-content/uploads/')
		// TD Note: $wp_upload_dir misses the language subdomain, so using this we have CORS errors on subdomains
		$wp_upload_dir = wp_get_upload_dir();
		$url = str_replace( $wp_upload_dir['basedir'], get_bloginfo('url')."/wp-content/uploads", $webvtt_filepath );

		return $url;

	}


	/**
	 * Generates an absolute server filepath of a media file's/attachment's WebVTT file for a given language
	 *
	 * @param     int       An array of objects returned by Whisper (can be sentences or segments of it)
	 * @param     string    A string formatted as WebVTT including timecodes or false if empty
	 * @return    string    The absolute filepath to the .vtt file
	 */
	public static function get_webvtt_filepath( $attachment_id, $language = '' ) {

		// Relative path to the original file in /wp-content/uploads/, e.g.' /2023/01/01/myfile.mp3'
		$_wp_attached_file = get_post_meta( $attachment_id, '_wp_attached_file', true );

		// If the attachment doesn't exist
		if( empty( $_wp_attached_file ) )
			return '';

		if( empty( $language ) )
			$language = get_option( 'transposer_fallback_language', 'en' );

		$file = wp_get_upload_dir()['basedir'] . '/' . $_wp_attached_file;

		$pathinfo = pathinfo( $file );
		$basename = $pathinfo['basename']; // Base filename without directory ('file.mp3')
		$filename = $pathinfo['filename']; // Base filename without directory and extension ('file')
		$dirname = $pathinfo['dirname']; // Directory path ('/var/www/wp-content/uploads/xyz')

		// The generated file will follow the convention: {model}_{filename}.json
		return $dirname . '/' . $filename . '_' . $language . '.vtt';
	}


	/**
	 * Deletes a transcript's .vtt file
	 *
	 * @param     int       Post ID of type 'attachment'
	 * @param     string    Two character language code
	 * @return    boolean   Whether the delete worked
	 */
	public static function delete_webvtt_filepath( $attachment_id, $language = '' ) {

		if( strlen( $language ) != 2 )
			return false;

		$webvtt_filepath = self::get_webvtt_filepath( $attachment_id, $language );

		if( @file_exists( $webvtt_filepath ) ) {
			if( ! @unlink( $webvtt_filepath ) )
				return false;
		}

		return true;

	}


	/**
	 * Generates a WebVTT formatted string from the segments given by Whisper
	 *
	 * Splits sentences by comma and transforms them into segments with corresponding timecodes to reduce the length of sentences displayed as subtitles in the player
	 * Calculates the start and end times for a segment by the average duration of a letter based on the sentence's duration
	 *
	 * TODO: Parse and validate on the fly
	 *
	 * Validation: https://w3c.github.io/webvtt.js/parser.html
	 *
	 * @param     array             An array of objects returned by Whisper (can be sentences or segments of it)
	 * @return    string|boolean    A string formatted as WebVTT including timecodes or false if empty
	 */
	public static function generate_webvtt( $segments ) {

		if( ! isset( $segments ) || empty( $segments ) )
			return false;

		$segment_collector = $segments;

/*

		$segment_collector = array();

		foreach( $segments as $segment ) {
			$segment_collector[] = $segment;
		}


		// Cycle each sentence
		foreach( $segments as $sentence ) {

			//echo "<b>" . $sentence->text . "<b>:<br><br>";

			// Is there a comma in the sentence?
			if( stristr( $sentence->text, ',' ) != false ) {

				// Number of letters in the sentence
				$num_sentence_letters = strlen( $sentence->text );

				// Duration of the sentence
				$dur_sentence = $sentence->end - $sentence->start;
				//echo "sentence takes " . $dur_sentence . " seconds.<br>";

				// Duration per letter
				$dur_per_letter = $dur_sentence / $num_sentence_letters;
				//echo "duration per letter: " . $dur_per_letter . " seconds.<br>";

				// Split sentence by comma
				$sentence_array = explode( ',', $sentence->text );

				// Count the segments
				$num_segments = count( $sentence_array );

				// Segment duration and control duration
				$dur_segment = 0;
				$ctrl_dur_sentence = 0;

				$i = 1;

				// Cycle each segment
				foreach( $sentence_array as $segment ) {

					// If it's not the last segment in the sentence, add the comma again, since it gets lost during explode()
					if( $i < $num_segments )
						$segment .= ',';

					// Segment's number of letters
					$num_segment_letters = strlen( $segment );

					//echo "<b>" . $segment . "</b><br>";
					//echo "segment has " . $num_segment_letters ." letters.<br>";

					// Duration of current segment
					$dur_segment = $num_segment_letters * $dur_per_letter;
					//echo "--> segment takes " . $dur_segment . " seconds<br>";

					// Add to the control duration
					$ctrl_dur_sentence += $dur_segment;

					if( $i == 1 ) {
						// First segment
						$start = $sentence->start; // Take the start time of the sentence
						$end = $start + $dur_segment; // The end is start + the segment's duration
					} else {
						$start = $end; // Take the last end as start
						$end += $dur_segment; // Add the duration of the segment to get the segment's end
					}

					$new_segment = new StdClass;
					$new_segment->start = $start;
					$new_segment->end = $end;
					$new_segment->text = $segment;

					// For the last segment always take the sentence's end to avoid overlaps
					if( $i == $num_segments )
						$new_segment->end = $sentence->end;

					// Collect the segments in an array
					$segment_collector[] = $new_segment;

					$i++;

				} // Each segment

				//echo "sentence control duration: " . $ctrl_dur_sentence . "<hr>";

			} else {
				// If no comma in sentence: just take it as it is
				$segment_collector[] = $sentence;
			}

		} // Each sentence
*/
		// Put together the WebVTT file content
		$content = "WEBVTT\n\n"; // WebVTT Header

		foreach( $segment_collector as $seg ) {
			// Add and format timecodes for the segment
			$content .= self::format_webvtt_timecode( $seg->start ) . ' --> ' . self::format_webvtt_timecode( $seg->end ) . "\n";
			$content .= trim( $seg->text ) ."\n\n";
		}

		// TODO: Check WebVTT by sending it through a parser
		return $content;

	}


	/**
	 * Returns the content of a post's WebVTT (.vtt) file for a given language
	 *
	 * @param     int       The Post ID (post type 'attachment')
	 * @param     string    Two-character language code
	 * @return    string    The file's content as plain text
	 */
	public static function get_webvtt_from_file( $post_id, $language ) {

		// Check if the file exists
		$webvtt_filepath = self::get_webvtt_filepath( $post_id, $language );

		if( ! @file_exists( $webvtt_filepath ) )
			return false;

		// Retrieve transcript object from file
		if( ! $transcript = @file_get_contents( $webvtt_filepath ) )
			return false;

		return $transcript;

	}


	/**
	 * Save (create, udpate) a WebVTT file for a given post and language
	 *
	 * @param     int       Post ID of post_type 'attachment'
	 * @param     string    2 character language code
	 * @param     string    The plain text WebVTT content to save
	 * @return    boolean
	 */
	public static function save_webvtt_to_file( $post_id, $language, $webvtt_content = '' ) {

		if( empty( $webvtt_content ) ) {
			return "WebVTT content is empty. Won't overwrite existing file.";
		}

		// Check if the file exists
		$webvtt_filepath = self::get_webvtt_filepath( $post_id, $language );

		if( ! TRP_Helper::write_to_file( $webvtt_filepath, stripslashes( $webvtt_content ) ) )
			return false;

		return true;

	}


	/**
	 * Formats seconds in float to hh:mm:ss.ms
	 *
	 * @param     float     Float number of seconds
	 * @return    string    The formatted string
	 */
	private static function format_webvtt_timecode( $seconds = 0 ) {

		// Calculate hours, minutes, seconds, and milliseconds
		$hours = (int) ( (int) $seconds / 3600 );
		$minutes = (int) ( ( (int) $seconds % 3600 ) / 60 );
		$sec = (int) ( (int) $seconds % 60 );
		$milliseconds = (int) round( fmod( (int) $seconds, 1 ) * 1000 );

		// Format the result as hh:mm:ss.ms
		return sprintf( "%02d:%02d:%02d.%03d", $hours, $minutes, $sec, $milliseconds );

	}

}

?>